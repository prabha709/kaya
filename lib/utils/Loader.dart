import 'package:flutter/material.dart';
class AppLoader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topCenter,
      child: SizedBox(
          child: Image.asset("assets/screen-loader-2.gif", ))
    );
  }
}
