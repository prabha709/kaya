import 'package:flutter/material.dart';

class GenerateOption extends StatelessWidget {
  const GenerateOption({
    Key key,
    @required this.defaultBoxColor,
    @required this.defaultTextColor,
    @required this.optionText,

  }) : super(key: key);

  final Color defaultBoxColor;
  final Color defaultTextColor;
  final String optionText;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){},
      child: Container(
        child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 15,
                width: 15,
                color: defaultBoxColor,
              ),
              SizedBox(width: 15),
              Text(
                optionText,
                style: TextStyle(color: defaultTextColor),
                textAlign: TextAlign.left,
              ),
            ]),
      ),
    );
  }
}