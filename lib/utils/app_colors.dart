import 'package:flutter/material.dart';

class AppColors {
  static const Color primaryColor = Colors.white;
  static const List<Color> primaryGradientColor = [
    Color.fromRGBO(129, 251, 184, 1),
    Color.fromRGBO(40, 199, 111, 1)
  ];
  static const List<Color> secondaryGradientColor = [
    Color.fromRGBO(247, 97, 161, 1),
    Color.fromRGBO(115, 103, 240, 1)
  ];
  static const List<Color> backgroundGradientColor = [
    Color(0xff),
    Color.fromRGBO(115, 103, 240, 1)
  ];

  static const Color colorWhite = Colors.white;
  static const Color successColor = Colors.green;
  static const Color failureColor = Colors.red;
  static const Color colorAccent = Colors.black;
  static const Color greyColor = Color(0xffF0F0F0);
  static const Color containerBlack = Colors.black;
  static const Color baseColor = Color(0xffa3c9b3);
}
