import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
class DatabaseHelper{

  static final _dbname = "myDatabase.db";
  static final _dbversion =1;
  static final _tableName= "MyTable"; 


  static final columnCatId= "_catid";
  static final columnName= "_Name";
  static final columnPrice= "_price";
  static final columnqty = "_qty";
  static final columnSession = "_session";
  static final columnId= "_id";
  
   
  
  DatabaseHelper._privateContructor();
  static final DatabaseHelper instance =DatabaseHelper._privateContructor();

static Database  _database;
Future<Database> get database async{
  if(_database!=null) {

  return _database;
  }else{
  _database= await _initiateDatabase();

return _database;
  }
}
_initiateDatabase() async{
Directory directory = await getApplicationDocumentsDirectory();
String path = join(directory.path,_dbname );
return await openDatabase(path,version:_dbversion,onCreate: _onCreate);

}

 _onCreate(Database db, int version) async{
await db.execute(
  
  ''' 
  CREATE TABLE  $_tableName(
   
    $columnName TEXT NOT NULL,
   

   
  )
''');

}  
Future<int> insert(Map<String,dynamic> row) async{
  Database db = await instance.database;
return db.insert(_tableName, row);
}
Future<List<Map<String, dynamic>>> queryAll() async{
  Database db = await instance.database;
return db.query(_tableName);
}
Future<List<Map<String, dynamic>>> customquery(columnname) async{
  Database db = await instance.database;
return db.query(_tableName,where: "$columnName =? ",whereArgs: [columnname]);
}
Future<List<Map<String, dynamic>>> customSessionQuery(sessionData) async{
  Database db = await instance.database;
return db.query(_tableName,where: "$columnSession =?",whereArgs: [sessionData]);
}
Future<int> update (Map<String, dynamic> row) async{
  Database db = await instance.database;
  int id=row[columnId];
  String sessionNo = row[columnSession];
  
return await db.update(_tableName,row,where: "$columnId =? and $columnSession=?", whereArgs: [id,sessionNo]);
}
Future<int> delete(String productName) async{
  Database db = await instance.database;
return db.delete(_tableName,where: "$columnName =?", whereArgs: [productName]);
}
Future<int> customDelete(int productId, String sessionId) async{
  Database db = await instance.database;
return db.delete(_tableName,where: "$columnId =? and $columnSession=?", whereArgs: [productId,sessionId]);
}

}