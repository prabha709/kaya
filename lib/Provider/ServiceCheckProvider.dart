import 'dart:io';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:kaya_skincare/Api%20Request/ServiceCheckApi.dart';
import 'package:kaya_skincare/Model/ServiceCheckModel.dart';

class ServiceCheckProvider extends ChangeNotifier {
  ServiceCheckModel serviceCheckModel = new ServiceCheckModel();

  Future<bool> getDetails(id) async {
    print(id);
    try {
      var response = await ServiceCheckApi(patientId: id).serviceCheck();

      if (response.statusCode == 200) {
        var jsonObject = json.decode(response.body);
        serviceCheckModel = ServiceCheckModel.fromJson(jsonObject);
        notifyListeners();
        return true;
      } else {
        return false;
      }
    } on SocketException catch (error) {
      Fluttertoast.showToast(
          msg: error.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
      return false;
    }
  }
}
