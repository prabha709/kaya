import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';
import 'package:kaya_skincare/Api%20Request/Form1Api.dart';
import 'package:kaya_skincare/Model/form2_model.dart';
import 'package:http/http.dart' as http;
import 'package:kaya_skincare/Model/form2_response_model.dart';
import 'package:kaya_skincare/utils/BaseUrl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:kaya_skincare/Model/saved_question_form1_response.dart';

class Form2Provider extends ChangeNotifier {
  Form2Model form2model;
  Form2Response form2Response;
  int lengthOfQuestionsForm2;
  var form2Submitted;
  SavedAnswersForm1Response savedAnswersFormResponse;
  bool fromstate = false;

  Future<bool> getDetails(int patientId) async {
    try {
      var response = await FormApi().patientForm2Data(patientId);

      if (response.statusCode == 200) {
        var jsonObject = json.decode(response.body);
        Form2Model data = Form2Model.fromJson(jsonObject);
        form2model = data;
        lengthOfQuestionsForm2 = form2model.resp.length ?? 1;
        notifyListeners();
        return true;
      } else {
        Fluttertoast.showToast(
            msg: "Kindly submit the Form1 Details",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
        return false;
      }
    } on SocketException catch (error) {
      Fluttertoast.showToast(
          msg: error.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
      return false;
    }
  }

  Future<bool> getAnswers(int patientId) async {
    try {
      var response = await FormApi().checkForm2Answer(patientId);

      if (response.statusCode == 200) {
        var jsonObject = json.decode(response.body);

        form2Response = Form2Response.fromJson(jsonObject);
        notifyListeners();
        return true;
      } else {
        return false;
      }
    } on SocketException catch (error) {
      Fluttertoast.showToast(
          msg: error.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
      return false;
    }
  }

  submitFormDetails(
      {@required int patientId,
      @required Map postData,
      @required String formId}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String data1 = formId.replaceAll("[", "");
    String data2 = data1.replaceAll("]", "");
    String data3 = data2.replaceAll(", ", "/");
    String token = prefs.getString("token");
    String url = BaseUrl.url + "patient_form2/$data3/$patientId";
    print(url);
    String encodedPostData = json.encode(postData);
    Response response = await http.post("$url",
        body: encodedPostData,
        headers: {
          "Authorization": "token $token",
          "Content-Type": "application/json"
        });
    if (response.statusCode == 200) {
      notifyListeners();
      Fluttertoast.showToast(
          msg:
              "Congratulations! Your 'Know Your Skin Questionnaire' is complete.",
          backgroundColor: Colors.green);

      return true;
      // var jsonObject = json.decode(response.body);
      // form2Response = Form2Response.fromJson(jsonObject);

    } else {
      Fluttertoast.showToast(
          msg: "Internal Server Error", backgroundColor: Colors.red);
      return false;
    }
  }

  checkFormPage(String patientId) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    form2Submitted = preferences.getString('form2SavedRadios$patientId');

    if (form2Submitted == null) return;
    form2Submitted = jsonDecode(form2Submitted);
    savedAnswersFormResponse =
        SavedAnswersForm1Response.fromJson(form2Submitted);

    notifyListeners();
  }

  void setLoading(bool value) {
    fromstate = value;
    notifyListeners();
  }

  bool get isLoading => fromstate;
}
