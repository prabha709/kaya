import 'dart:io';
import 'dart:convert';
import 'package:flutter/material.dart';

import 'package:fluttertoast/fluttertoast.dart';
import 'package:kaya_skincare/Api%20Request/doctor_service_api.dart';
import 'package:kaya_skincare/Model/DoctorServiceModel.dart';

class DoctorSericeProvider extends ChangeNotifier {
  DoctorServiceModel doctorServiceModel = new DoctorServiceModel();
  List<DoctorServices> filterData = [];
  int selectedCount = 0;

  Future<bool> getDetails() async {
    try {
      var response = await DoctorServiceApi().doctorServiceApi();
      var jsonObject = json.decode(response.body);
      if (response.statusCode == 200) {
        DoctorServiceModel data = DoctorServiceModel.fromJson(jsonObject);
        doctorServiceModel = data;

        notifyListeners();
        return true;
      } else {
        Fluttertoast.showToast(
            msg: jsonObject.toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
        return false;
      }
    } on SocketException catch (error) {
      Fluttertoast.showToast(
          msg: error.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }
}
