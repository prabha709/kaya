import 'dart:io';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:kaya_skincare/Api%20Request/PatientDataApi.dart';
import 'package:kaya_skincare/Model/PatientDetailsModel.dart';
import 'package:fluttertoast/fluttertoast.dart';

class PatientDetailsProvider extends ChangeNotifier {
  ServiceCheckModel patientDetailsModel = new ServiceCheckModel();

  Future<bool> getDetails(id) async {
    try {
      var response = await PatientDataApi(patientId: id).patientListData();
      var jsonObject = json.decode(utf8.decode(response.bodyBytes));
      if (response.statusCode == 200) {
        ServiceCheckModel data = ServiceCheckModel.fromJson(jsonObject);
        patientDetailsModel = data;
        notifyListeners();
        return true;
      } else {
        Fluttertoast.showToast(
            msg: jsonObject.toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
        return false;
      }
    } on SocketException catch (error) {
      Fluttertoast.showToast(
          msg: error.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
      return false;
    }
  }
}
