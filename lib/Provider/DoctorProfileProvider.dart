import 'dart:io';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:kaya_skincare/Api%20Request/DoctotProfileAPi.dart';

import 'package:kaya_skincare/Model/DoctorProfileModel.dart';
import 'package:fluttertoast/fluttertoast.dart';

class DoctorProfileProvider extends ChangeNotifier {
  DoctorProfileModel doctorProfileModel = new DoctorProfileModel();
  String firstCharFirstName;
  String fname;
  List doctorServices;

  Future<bool> getDetails() async {
    try {
      var response = await DoctorProfileApi().doctorProfile();
      var jsonObject = json.decode(response.body);
      if (response.statusCode == 200) {
        DoctorProfileModel data = DoctorProfileModel.fromJson(jsonObject);
        doctorProfileModel = data;
        fname = doctorProfileModel.resp.fname;
        firstCharFirstName = doctorProfileModel.resp.fname[0];

        notifyListeners();
        return true;
      } else {
        Fluttertoast.showToast(
            msg: jsonObject.toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
        return false;
      }
    } on SocketException catch (error) {
      Fluttertoast.showToast(
          msg: error.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }
}
