import 'dart:io';
import 'dart:convert';
import 'package:flutter/material.dart';

import 'package:fluttertoast/fluttertoast.dart';
import 'package:kaya_skincare/Api%20Request/ImagesApi.dart';
import 'package:kaya_skincare/Model/PatientImgesModel.dart';

class PatientImageProvider extends ChangeNotifier {
  PatientImageModel patientImageModel = new PatientImageModel();
  List selectedData = ["0"];
  getcount(preserveddata) async {
    // Map data ={
    //   "image":preserveddata
    // };
    selectedData.add(preserveddata);
    print(selectedData);
    notifyListeners();
  }

  removeCount(preserveddata) async {
    selectedData.remove(preserveddata);
    notifyListeners();
  }

  Future<bool> getDetails(id) async {
    try {
      var response = await ImagesApi(patientId: id).patientImage();

      if (response.statusCode == 200) {
        var jsonObject = json.decode(response.body);
        PatientImageModel data = PatientImageModel.fromJson(jsonObject);
        patientImageModel = data;
        notifyListeners();
        return true;
      } else {
        return false;
      }
    } on SocketException catch (error) {
      Fluttertoast.showToast(
          msg: error.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
      return false;
    }
  }

  Future<bool> deleteDeails(imageid, patientId) async {
    try {
      var response = await ImagesApi(imageId: imageid).deleteImage();

      if (response.statusCode == 204) {
        Fluttertoast.showToast(
            msg: "Image Deleted SuccessFully",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.green,
            textColor: Colors.white,
            fontSize: 16.0);
        getDetails(patientId);
        notifyListeners();
        return true;
      } else {
        Fluttertoast.showToast(
            msg: "Internal Server Error",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
        return false;
      }
    } on SocketException catch (error) {
      Fluttertoast.showToast(
          msg: error.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
      return false;
    }
  }
}
