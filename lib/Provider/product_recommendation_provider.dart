import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:kaya_skincare/Api%20Request/product_recommendation_api.dart';
import 'package:kaya_skincare/Model/product_recommendation_model_response.dart';
import 'package:kaya_skincare/Model/product_recommendation_post_response.dart';
import 'package:kaya_skincare/utils/app_colors.dart';

class ProductRecommendationProvider extends ChangeNotifier {
  ProductRecommendationModel productRecommendationModel;

  ProductRecommendationPostResponse productRecommendationPostResponse;
  int postId;

  Future getProductRecommendations(int patientId) async {
    try {
      var response =
          await ProductRecommendationApi().getProductRecommendations(patientId);
      if (response.statusCode == 200) {
        var jsonObject = json.decode(response.body);
        productRecommendationModel =
            ProductRecommendationModel.fromJson(jsonObject);

        notifyListeners();
        return true;
      } else {
        return false;
      }
    } on SocketException catch (error) {
      Fluttertoast.showToast(
          msg: "Socket Exception Time out error",
          backgroundColor: AppColors.failureColor);
      return false;
    }
  }

  postProductRecommendations(String patientId, Map data) async {
    var response = await ProductRecommendationApi()
        .postProductRecommendations(patientId, data);
    if (response.statusCode == 200) {
      var jsonObject = json.decode(response.body);
      productRecommendationPostResponse =
          ProductRecommendationPostResponse.fromJson(jsonObject);
      postId = productRecommendationPostResponse.id;
    }
  }
}
