import 'dart:io';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:kaya_skincare/Api%20Request/PatientListApi.dart';
import 'package:kaya_skincare/Model/PatientListModel.dart';
import 'package:fluttertoast/fluttertoast.dart';

class PatientListProvider extends ChangeNotifier {
  PatientListModel patientListModel = new PatientListModel();
  List<String> patientList = [];
  List<Resp> patientdata = new List();
  List<Resp> searchData = [];

  PatientListProvider() {
    patientListModel.resp = patientdata;
  }

  Future<bool> getDetails() async {
    try {
      var response = await PatientListApi().patientListData();
      var jsonObject = json.decode(response.body);
      if (response.statusCode == 200) {
        PatientListModel data = PatientListModel.fromJson(jsonObject);
        patientListModel = data;

        notifyListeners();
        return true;
      } else {
        Fluttertoast.showToast(
            msg: jsonObject.toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
        return false;
      }
    } on SocketException catch (error) {
      Fluttertoast.showToast(
          msg: error.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
      notifyListeners();
    }
  }

  Future<bool> deleteDetails(id) async {
    try {
      var response = await PatientListApi(patientId: id).patientListDelete();

      if (response.statusCode == 200) {
        Fluttertoast.showToast(
            msg: "Data Deleted Sucessfully",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.green,
            textColor: Colors.white,
            fontSize: 16.0);
        await getDetails();
        notifyListeners();
        return true;
      } else {
        Fluttertoast.showToast(
            msg: "Something Went Wrong",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
        return false;
      }
    } on SocketException catch (error) {
      Fluttertoast.showToast(
          msg: error.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }
}
