import 'dart:io';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:kaya_skincare/Api%20Request/BeforeAfterApi.dart';


import 'package:fluttertoast/fluttertoast.dart';
import 'package:kaya_skincare/Model/BeforeAfterModel.dart';



class BeforeAfterProvider extends ChangeNotifier{
BeforeAfterModel beforeAfterModel = new BeforeAfterModel();
 



 
  Future<bool> getDetails(id)async{
    
     try{
       var response = await BeforeAfterApi(patientId: id).beforeAfterImage();
       
       if(response.statusCode== 200){
      var jsonObject = json.decode(response.body);
         BeforeAfterModel data = BeforeAfterModel.fromJson(jsonObject);
         beforeAfterModel=data;
         notifyListeners();
        return true;
       }else if(response.statusCode == 204){
         return false;
       }
       
       else{
        var jsonObject = json.decode(response.body);
      Fluttertoast.showToast(
        msg: jsonObject.toString(),
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0
    );
         return false;

       }

     }on SocketException catch(error){
       Fluttertoast.showToast(
        msg: error.toString(),
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0
    );
    return false;
    
     }
       
       
       

  }

  Future<bool> deleleImage(id)async{
    
     try{
       var response = await BeforeAfterApi(imageId: id).deleteImage();
       
       if(response.statusCode== 200){
     
  Fluttertoast.showToast(
        msg: "Image Deleted",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.green,
        textColor: Colors.white,
        fontSize: 16.0
    );
         notifyListeners();
        return true;
       }
       
       else{
        
      Fluttertoast.showToast(
        msg: "Internal Server Error",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0
    );
         return false;

       }

     }on SocketException catch(error){
       Fluttertoast.showToast(
        msg: error.toString(),
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0
    );
    return false;
    
     }
       
       
       

  }
}