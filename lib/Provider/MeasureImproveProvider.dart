import 'dart:io';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:kaya_skincare/Api%20Request/MeasureImprovementsApi.dart';
import 'package:kaya_skincare/Model/MeasureImproModel.dart';


class MeasureImprovementsProvider extends ChangeNotifier{
  MeasureImproveModel measureImproveModel = new MeasureImproveModel();



 
  Future<bool> getDetails(id)async{
    
     try{
       var response = await MeasureImprovementsApi(patientId:id).patientData();
      //  var jsonObject = json.decode(response.body);
       if(response.statusCode == 200){
      var jsonObject = json.decode(response.body);
         MeasureImproveModel data = MeasureImproveModel.fromJson(jsonObject);
         measureImproveModel=data;
         notifyListeners();
        return true;
       }else {
          return false;
        
         
       }
 

     }on SocketException catch(error){
       Fluttertoast.showToast(
        msg: error.toString(),
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0
    );
    return false;
    
     }
       
       
       

  }
   Future<bool> deleteImage(id)async{
    
     try{
       var response = await MeasureImprovementsApi(imageId:id).deleteImage();
      //  var jsonObject = json.decode(response.body);
       if(response.statusCode == 200){
      
        Fluttertoast.showToast(
        msg: "Deleted Successfuly",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.green,
        textColor: Colors.white,
        fontSize: 16.0
    );
         notifyListeners();
        return true;
       }else {
         Fluttertoast.showToast(
        msg:"Internal Server Error",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0
    );
          return false;
        
         
       }
 

     }on SocketException catch(error){
       Fluttertoast.showToast(
        msg: error.toString(),
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0
    );
    return false;
    
     }
       
       
       

  }

  
}