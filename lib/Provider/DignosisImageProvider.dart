import 'dart:io';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:kaya_skincare/Api%20Request/DiagnosisApi.dart';
import 'package:kaya_skincare/Model/DiagnosisImageModel.dart';
import 'package:fluttertoast/fluttertoast.dart';


class DignosisImageProvider extends ChangeNotifier{
  DiagnosisImageModel diagnosisImageModel = new DiagnosisImageModel();



 
  Future<bool> getDetails(id)async{
    
     try{
       var response = await DiagonasisApi(patientId:id).patientDiagnosisImage();
      //  var jsonObject = json.decode(response.body);
       if(response.statusCode == 200){
      var jsonObject = json.decode(response.body);
         DiagnosisImageModel data = DiagnosisImageModel.fromJson(jsonObject);
         diagnosisImageModel=data;
         notifyListeners();
        return true;
       }else {
          return false;
        
         
       }
 

     }on SocketException catch(error){
       Fluttertoast.showToast(
        msg: error.toString(),
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0
    );
    return false;
    
     }
       
       
       

  }
    Future<bool> deleteDetails(imageId, patientId)async{
    
     try{
       var response = await DiagonasisApi(dImage:imageId).deleteImage();
      //  var jsonObject = json.decode(response.body);
       if(response.statusCode == 200){
      Fluttertoast.showToast(
        msg: "Image Deleted SucessFully",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.green,
        textColor: Colors.white,
        fontSize: 16.0
    );
    getDetails(patientId);
    notifyListeners();
    return true;
       }else {
          return false;
        
         
       }
 

     }on SocketException catch(error){
       Fluttertoast.showToast(
        msg: error.toString(),
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0
    );
    return false;
    
     }
       
       
       

  }

  
}