import 'dart:io';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:kaya_skincare/Api%20Request/Form1Api.dart';
import 'package:kaya_skincare/Model/Form1AnswerModel.dart';
import 'package:kaya_skincare/Model/Form1Model.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:kaya_skincare/Model/form_detail_response.dart';
import 'package:kaya_skincare/Model/saved_question_form1_response.dart';
import 'package:kaya_skincare/utils/BaseUrl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class Form1Provider extends ChangeNotifier {
  var formSubmitted;
  SavedAnswersForm1Response savedAnswersForm1Response;
  List <String> form1selectedAnswers = [];



  generateForm1SelectedAnswerList(int lengthOfQuestions){
    form1selectedAnswers = List<String>.generate(lengthOfQuestions, (i) => "");
  }

  Form1Model form1model;
  int lengthOfQuestionsForm1;
  Form1AnswerModel form1answerModel;
  List finaldata =[];

  FormDetailResponse formDetailResponse;

  Future<bool> getDetails() async {
    try {
      var response = await FormApi().patientForm1Data();
    var jsonObject = json.decode(response.body);
      if (response.statusCode == 200) {
        // finaldata.add(jsonObject["resp"]);
        // print(jsonObject["resp"]);
        Form1Model data = Form1Model.fromJson(jsonObject);
        form1model = data;
        lengthOfQuestionsForm1 = form1model.resp.length ?? 1;
        notifyListeners();
        return true;
      } else {
        Fluttertoast.showToast(
            msg: jsonObject.toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
        return false;
      }
    } on SocketException catch (error) {
      Fluttertoast.showToast(
          msg: error.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
      return false;
    }
  }

  Future<bool> getAnswers(id) async {
    try {
      var response = await FormApi().checkForm1Answer(id);

      if (response.statusCode == 200) {
        var jsonObject = json.decode(utf8.decode(response.bodyBytes));
        Form1AnswerModel data = Form1AnswerModel.fromJson(jsonObject);
        form1answerModel = data;
        notifyListeners();
        return true;
      } else {
        return false;
      }
    } on SocketException catch (error) {
      Fluttertoast.showToast(
          msg: error.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
      return false;
    }
  }

  submitFormDetails(
      {@required String patientId, @required Map postData}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");
    print("Patient Id = " + patientId.toString());
    String url = BaseUrl.url + "patient_form1/$patientId";
    String encodedPostData = json.encode(postData);
    print(encodedPostData);
    Response response = await http.post("$url",
        body: encodedPostData,
        headers: {
          "Authorization": "token $token",
          "Content-Type": "application/json"
        });
    if (response.statusCode == 200) {

      notifyListeners();
      return true;
    } else {
      return false;
    }
  }

//   checkFormPage(String patientId) async{
//     SharedPreferences preferences = await SharedPreferences.getInstance() ;
//     formSubmitted = preferences.getString('form1SavedRadios$patientId')  ;

//     if(formSubmitted == null)
//       return ;
//     formSubmitted = jsonDecode(formSubmitted);
//     savedAnswersForm1Response = SavedAnswersForm1Response.fromJson(formSubmitted);
//     notifyListeners() ;
//   }
}
