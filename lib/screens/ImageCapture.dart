// import 'package:flutter/material.dart';
// import 'package:camera/camera.dart';
// import 'package:fluttertoast/fluttertoast.dart';
// import 'package:kaya_skincare/utils/BaseUrl.dart';
// import "package:http/http.dart" as http;
// import 'package:kaya_skincare/utils/app_colors.dart';
// import 'package:shared_preferences/shared_preferences.dart';

// class ImageCapture extends StatefulWidget {
//   final int patientId;

//   ImageCapture({this.patientId});
//   @override
//   _ImageCaptureState createState() => _ImageCaptureState();
// }

// class _ImageCaptureState extends State<ImageCapture> {
//   final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
//   CameraController controller;
//   List<CameraDescription> cameras;
//   bool openCamera1 = false;

//   bool openCamera2 = false;
//   bool loading = false;
//   bool openCamera3 = false;
//   XFile imageFile1;
//   XFile imageFile2;
//   XFile imageFile3;
//   List imageData = [];

//   void showInSnackBar(String message) {
//     // ignore: deprecated_member_use
//     _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(message)));
//   }

//   void _showCameraException(CameraException e) {
//     print("${e.code} ${e.description}");
//     showInSnackBar('Error: ${e.code}\n${e.description}');
//   }

//   openTheCamera() async {
//     cameras = await availableCameras();
//     controller = CameraController(cameras[0], ResolutionPreset.medium);
//     controller.initialize().then((_) {
//       if (!mounted) {
//         return;
//       }
//       setState(() {});
//     });
//   }

//   Future<XFile> takePicture() async {
//     if (!controller.value.isInitialized) {
//       // showInSnackBar('Error: select a camera first.');
//       return null;
//     }

//     if (controller.value.isTakingPicture) {
//       // A capture is already pending, do nothing.
//       return null;
//     }

//     try {
//       XFile file = await controller.takePicture();
//       return file;
//     } on CameraException catch (e) {
//       print(e);
//       // _showCameraException(e);
//       return null;
//     }
//   }

//   void onTakePictureButtonPressed() {
//     takePicture().then((XFile file) {
//       if (mounted) {
//         if (openCamera1 == true) {
//           setState(() {
//             imageFile1 = file;
//             openCamera1 = false;
//           });

//           imageData.add(imageFile1.path);
//         } else if (openCamera2 == true) {
//           setState(() {
//             imageFile2 = file;
//             openCamera2 = false;
//           });

//           imageData.add(imageFile2.path);
//         } else if (openCamera3 == true) {
//           setState(() {
//             imageFile3 = file;
//             openCamera3 = false;
//           });

//           imageData.add(imageFile3.path);
//         }
//         // File data = file.toFile();

//         // if (file != null) showInSnackBar('Picture saved to ${file.path}');
//       }
//     });
//   }

//   _uploadImage() async {
//     String url = BaseUrl.url + "patient_image/${widget.patientId}";
//     SharedPreferences prefs = await SharedPreferences.getInstance();

//     String token = prefs.getString("token");
//     Map<String, String> data = {"Authorization": "token $token"};
//     var request = http.MultipartRequest("POST", Uri.parse("$url"));
//     request.headers.addAll(data);
//     request.files.add(await http.MultipartFile.fromPath(
//       "image",
//       imageFile1.path,
//     ));
//     request.files.add(await http.MultipartFile.fromPath(
//       "image",
//       imageFile2.path,
//     ));
//     request.files.add(await http.MultipartFile.fromPath(
//       "image",
//       imageFile3.path,
//     ));
//     var res = await request.send();
//     final respStr = res.stream.bytesToString();
//     print(res.statusCode);
//     // var userData = (respStr as Map<String, dynamic>)['data'];
//     print(respStr);
//     if (res.statusCode == 201) {
//       setState(() {
//         imageFile1 = null;
//         imageFile2 = null;
//         imageFile3 = null;
//         loading = false;
//       });
//       Fluttertoast.showToast(
//           msg: "Image Uploaded Sucessfully..",
//           toastLength: Toast.LENGTH_SHORT,
//           gravity: ToastGravity.BOTTOM,
//           timeInSecForIosWeb: 1,
//           backgroundColor: Colors.green,
//           textColor: Colors.black,
//           fontSize: 16.0);
//     } else {
//       setState(() {
//         imageFile1 = null;
//         imageFile2 = null;
//         imageFile3 = null;
//         loading = false;
//       });
//       Fluttertoast.showToast(
//           msg: "Internal Server Error",
//           toastLength: Toast.LENGTH_SHORT,
//           gravity: ToastGravity.BOTTOM,
//           timeInSecForIosWeb: 1,
//           backgroundColor: Colors.red,
//           textColor: Colors.white,
//           fontSize: 16.0);
//     }
//     // setState(() {
//     //   result= respStr;
//     // });
//     return respStr;
//   }

//   Widget _captureControlRowWidget() {
//     return Row(
//       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//       mainAxisSize: MainAxisSize.max,
//       children: <Widget>[
//         IconButton(
//           icon: const Icon(Icons.camera_alt),
//           color: Colors.white,
//           onPressed: controller != null &&
//                   controller.value.isInitialized &&
//                   !controller.value.isRecordingVideo
//               ? onTakePictureButtonPressed
//               : null,
//         ),
//       ],
//     );
//   }

//   @override
//   void initState() {
//     openTheCamera();

//     super.initState();
//   }

//   void dispose() {
//     // controller?.dispose();
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     var size = MediaQuery.of(context).size;

//     /*24 is for notification bar on Android*/
//     final double itemHeight = (size.height - kToolbarHeight - 5);
//     final double itemWidth = size.width / 2;
//     Size screenSize = MediaQuery.of(context).size;
//     return Scaffold(
//       appBar: AppBar(
//         flexibleSpace: Container(
//           decoration: BoxDecoration(
//             image: DecorationImage(
//               image: AssetImage("assets/bckg_img.png"),
//               fit: BoxFit.cover,
//             ),
//           ),
//         ),
//         centerTitle: true,
//         backgroundColor: Colors.white,
//         elevation: 0.0,
//         automaticallyImplyLeading: false,
//         title: Container(
//           decoration: BoxDecoration(
//             color: Color(0xffF0F0F0),
//             borderRadius: BorderRadius.all(Radius.circular(35)),
//           ),
//           width: screenSize.width / 2.5,
//           height: screenSize.height / 10,
//           child: Center(
//               child: Text(
//             "Image Capture",
//             style: TextStyle(color: Colors.black, fontSize: 25),
//           )),
//         ),
//       ),
//       body: GridView.count(
//         crossAxisCount: 2,
//         childAspectRatio: itemWidth / itemHeight,
//         children: [
//           // Text("Left Side Face Image",style: TextStyle(fontSize: 18.0,fontWeight: FontWeight.bold)),
//           InkWell(
//             onTap: () {
//               setState(() {
//                 openCamera1 = true;
//               });
//             },
//             child: Card(
//               elevation: 2.0,
//               child: Container(
//                 height: screenSize.height / 2.6,
//                 width: screenSize.width / 4,
//                 child: openCamera1 == false
//                     ? Center(
//                         child: imageFile1 == null
//                             ? Column(
//                                 mainAxisAlignment: MainAxisAlignment.center,
//                                 children: [
//                                   Icon(Icons.camera),
//                                   Text("Left Side Face")
//                                 ],
//                               )
//                             : RotatedBox(
//                                 quarterTurns:
//                                     MediaQuery.of(context).orientation ==
//                                             Orientation.landscape
//                                         ? 2
//                                         : 1,
//                                 child: Image.asset(imageFile1.path)))
//                     : RotatedBox(
//                         quarterTurns: MediaQuery.of(context).orientation ==
//                                 Orientation.landscape
//                             ? 2
//                             : 1,
//                         child: Stack(children: [
//                           AspectRatio(
//                               aspectRatio: controller.value.aspectRatio,
//                               child: CameraPreview(controller)),
//                           _captureControlRowWidget()
//                         ]),
//                       ),
//               ),
//             ),
//           ),
//           // Text("Left Side Face Image",style: TextStyle(fontSize: 18.0,fontWeight: FontWeight.bold)),
//           InkWell(
//             onTap: () {
//               setState(() {
//                 openCamera2 = true;
//               });
//             },
//             child: Card(
//               elevation: 2.0,
//               child: Container(
//                 height: screenSize.height / 2.6,
//                 width: screenSize.width / 4,
//                 child: openCamera2 == false
//                     ? Center(
//                         child: imageFile2 == null
//                             ? Column(
//                                 mainAxisAlignment: MainAxisAlignment.center,
//                                 children: [
//                                   Icon(Icons.camera),
//                                   Text("Right Side Face")
//                                 ],
//                               )
//                             : RotatedBox(
//                                 quarterTurns:
//                                     MediaQuery.of(context).orientation ==
//                                             Orientation.landscape
//                                         ? 2
//                                         : 1,
//                                 child: Image.asset(imageFile2.path)))
//                     : RotatedBox(
//                         quarterTurns: MediaQuery.of(context).orientation ==
//                                 Orientation.landscape
//                             ? 2
//                             : 1,
//                         child: Stack(children: [
//                           AspectRatio(
//                               aspectRatio: controller.value.aspectRatio,
//                               child: CameraPreview(controller)),
//                           _captureControlRowWidget()
//                         ]),
//                       ),
//               ),
//             ),
//           ),
//           // Text("Left Side Face Image",style: TextStyle(fontSize: 18.0,fontWeight: FontWeight.bold)),
//           InkWell(
//             onTap: () {
//               setState(() {
//                 openCamera3 = true;
//               });
//             },
//             child: Card(
//               elevation: 2.0,
//               child: Container(
//                 height: screenSize.height / 2.6,
//                 width: screenSize.width / 4,
//                 child: openCamera3 == false
//                     ? Center(
//                         child: imageFile3 == null
//                             ? Column(
//                                 mainAxisAlignment: MainAxisAlignment.center,
//                                 children: [
//                                   Icon(Icons.camera),
//                                   Text("Straight Face")
//                                 ],
//                               )
//                             : RotatedBox(
//                                 quarterTurns:
//                                     MediaQuery.of(context).orientation ==
//                                             Orientation.landscape
//                                         ? 2
//                                         : 1,
//                                 child: Image.asset(imageFile3.path)))
//                     : RotatedBox(
//                         quarterTurns: MediaQuery.of(context).orientation ==
//                                 Orientation.landscape
//                             ? 2
//                             : 1,
//                         child: Stack(children: [
//                           AspectRatio(
//                               aspectRatio: controller.value.aspectRatio,
//                               child: CameraPreview(controller)),
//                           _captureControlRowWidget()
//                         ]),
//                       ),
//               ),
//             ),
//           ),
//         ],
//       ),
//       floatingActionButton:
//           imageFile1 != null && imageFile2 != null && imageFile3 != null
//               ? FloatingActionButton(
//                   onPressed: () {
//                     _uploadImage();
//                   },
//                   child: loading == false
//                       ? Icon(Icons.upload_file)
//                       : Center(
//                           child: CircularProgressIndicator(
//                             backgroundColor: AppColors.colorAccent,
//                           ),
//                         ),
//                 )
//               : Container(),
//     );
//   }
// }
