import "package:flutter/material.dart";
import 'package:kaya_skincare/Provider/PatientDetailsProvider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:provider/provider.dart';
import '../utils/app_colors.dart';

class Form1Static extends StatefulWidget {
  final int patientId;
  Form1Static(this.patientId);

  @override
  _Form1StaticState createState() => _Form1StaticState();
}

class _Form1StaticState extends State<Form1Static> {
  List<String> questions = [];
  var q1;
  var q2;
  var q3;
  var q4;
  var q5;
  var q6;
  var q7;
  var q8;
  var q9;
  var q10;
  var q11;
  var q12;
  String test;
  String test2;
  String test3;
  String test4;
  String test5;
  String test6;
  String test7;
  String test8;
  String test9;
  String test10;
  String test11;
  String test12;
  String text;
  String text2;
  String text3;
  String text4;
  String text5;
  String text6;
  String text7;
  String text8;
  String text9;
  String text10;
  String text11;
  String text12;
  bool allvaluefetch = false;
  getStringValuesSF() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      test = prefs.getString(widget.patientId.toString() + "f01");
      test2 = prefs.getString(widget.patientId.toString() + "f02");
      test3 = prefs.getString(widget.patientId.toString() + "f03");
      test4 = prefs.getString(widget.patientId.toString() + "f04");
      test5 = prefs.getString(widget.patientId.toString() + "f05");
      test6 = prefs.getString(widget.patientId.toString() + "f06");
      test7 = prefs.getString(widget.patientId.toString() + "f07");
      test8 = prefs.getString(widget.patientId.toString() + "f08");
      test9 = prefs.getString(widget.patientId.toString() + "f09");
      test10 = prefs.getString(widget.patientId.toString() + "f10");
      test11 = prefs.getString(widget.patientId.toString() + "f11");
      test12 = prefs.getString(widget.patientId.toString() + "f12");
      allvaluefetch = true;
    });
  }

  PatientDetailsProvider patientDetailsProvider;
  @override
  void initState() {
    getStringValuesSF();
super.initState();
  }

  @override
  Widget build(BuildContext context) {
    patientDetailsProvider =
        Provider.of<PatientDetailsProvider>(context, listen: false);
    return !allvaluefetch
        ? CircularProgressIndicator()
        : Padding(
            padding: const EdgeInsets.all(18.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                patientDetailsProvider.patientDetailsModel.gender != "Male"
                    ? Card(
                        elevation: 5.0,
                        child: Column(
                          children: [
                            Align(
                                alignment: Alignment.topLeft,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 18.0, top: 18.0),
                                  child: Text("Are you pregnant ?",
                                      style: TextStyle(
                                        fontSize: 18,
                                      )),
                                )),
                            RadioListTile(
                              activeColor: AppColors.containerBlack,
                              title: Text("Yes"),
                              value: "0",
                              groupValue: test == null ? q1 : test,
                              onChanged: (val) async {
                                SharedPreferences prefs =
                                    await SharedPreferences.getInstance();
                                setState(() {
                                  q1 = val;
                                  test = val;
                                });
                                prefs.setString(
                                    widget.patientId.toString() + "f01", val);
                                prefs.setString(
                                    widget.patientId.toString() + "text1",
                                    "yes");
                              },
                            ),
                            RadioListTile(
                              activeColor: AppColors.containerBlack,
                              title: Text("No"),
                              value: "1",
                              groupValue: test == null ? q1 : test,
                              onChanged: (val) async {
                                SharedPreferences prefs =
                                    await SharedPreferences.getInstance();
                                setState(() {
                                  q1 = val;
                                  test = val;
                                });
                                prefs.setString(
                                    widget.patientId.toString() + "f01", val);
                                prefs.setString(
                                    widget.patientId.toString() + "text1",
                                    "no");
                              },
                            ),
                          ],
                        ),
                      )
                    : Container(),
                SizedBox(
                  height: 15.0,
                ),
                patientDetailsProvider.patientDetailsModel.gender != "Male"
                    ? Card(
                        elevation: 5.0,
                        child: Column(
                          children: [
                            Align(
                                alignment: Alignment.topLeft,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 18.0, top: 18.0),
                                  child: Text("Are you breast feeding ?",
                                      style: TextStyle(
                                        fontSize: 18,
                                      )),
                                )),
                            RadioListTile(
                              activeColor: AppColors.containerBlack,
                              title: Text("Yes"),
                              value: "0",
                              groupValue: test == null ? q2 : test2,
                              onChanged: (val) async {
                                SharedPreferences prefs =
                                    await SharedPreferences.getInstance();
                                setState(() {
                                  q2 = val;
                                  test2 = val;
                                });
                                prefs.setString(
                                    widget.patientId.toString() + "f02", val);
                                prefs.setString(
                                    widget.patientId.toString() + "text2",
                                    "yes");
                              },
                            ),
                            RadioListTile(
                              activeColor: AppColors.containerBlack,
                              title: Text("No"),
                              value: "1",
                              groupValue: test == null ? q2 : test2,
                              onChanged: (val) async {
                                SharedPreferences prefs =
                                    await SharedPreferences.getInstance();
                                setState(() {
                                  q2 = val;
                                  test2 = val;
                                });
                                prefs.setString(
                                    widget.patientId.toString() + "f02", val);
                                prefs.setString(
                                    widget.patientId.toString() + "text2",
                                    "no");
                              },
                            ),
                          ],
                        ),
                      )
                    : Container(),
                SizedBox(
                  height: 15.0,
                ),
                Card(
                  elevation: 5.0,
                  child: Column(
                    children: [
                      Align(
                          alignment: Alignment.topLeft,
                          child: Padding(
                            padding:
                                const EdgeInsets.only(left: 18.0, top: 18.0),
                            child: Text(
                                "How much water do you generally drink every day?",
                                style: TextStyle(
                                  fontSize: 18,
                                )),
                          )),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text("1-2 litres"),
                        value: "0",
                        groupValue: test == null ? q3 : test3,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q3 = val;
                            test3 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f03", val);
                          prefs.setString(widget.patientId.toString() + "text3",
                              "1-2 litres");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text("2-3 litres"),
                        value: "1",
                        groupValue: test == null ? q3 : test3,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q3 = val;
                            test3 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f03", val);
                          prefs.setString(widget.patientId.toString() + "text3",
                              "2-3 litres");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text("3-4 litres"),
                        value: "2",
                        groupValue: test == null ? q3 : test3,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q3 = val;
                            test3 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f03", val);
                          prefs.setString(widget.patientId.toString() + "text3",
                              "3-4 litres");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text("Not Sure"),
                        value: "3",
                        groupValue: test == null ? q3 : test3,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q3 = val;
                            test3 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f03", val);
                          prefs.setString(widget.patientId.toString() + "text3",
                              "Not Sure");
                        },
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 15.0,
                ),
                Card(
                  elevation: 5.0,
                  child: Column(
                    children: [
                      Align(
                          alignment: Alignment.topLeft,
                          child: Padding(
                            padding:
                                const EdgeInsets.only(left: 18.0, top: 18.0),
                            child: Text("Do you smoke cigarettes?",
                                style: TextStyle(
                                  fontSize: 18,
                                )),
                          )),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text("Frequently"),
                        value: "0",
                        groupValue: test == null ? q4 : test4,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q4 = val;
                            test4 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f04", val);
                          prefs.setString(widget.patientId.toString() + "text4",
                              "Frequently");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text("Occasionally"),
                        value: "1",
                        groupValue: test == null ? q4 : test4,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q4 = val;
                            test4 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f04", val);
                          prefs.setString(widget.patientId.toString() + "text4",
                              "Occasionally");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text("Unwilling to disclose"),
                        value: "2",
                        groupValue: test == null ? q4 : test4,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q4 = val;
                            test4 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f04", val);
                          prefs.setString(widget.patientId.toString() + "text4",
                              "Unwilling to disclose");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text("I dont smoke"),
                        value: "3",
                        groupValue: test == null ? q4 : test4,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q4 = val;
                            test4 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f04", val);
                          prefs.setString(widget.patientId.toString() + "text4",
                              "I dont smoke");
                        },
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 15.0,
                ),
                Card(
                  elevation: 5.0,
                  child: Column(
                    children: [
                      Align(
                          alignment: Alignment.topLeft,
                          child: Padding(
                            padding:
                                const EdgeInsets.only(top: 18.0, left: 18.0),
                            child: Text("How often do you drink alcohol?",
                                style: TextStyle(
                                  fontSize: 18,
                                )),
                          )),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text("Frequently"),
                        value: "0",
                        groupValue: test == null ? q5 : test5,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q5 = val;
                            test5 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f05", val);
                          prefs.setString(widget.patientId.toString() + "text5",
                              "Frequently");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text("Occasionally"),
                        value: "1",
                        groupValue: test == null ? q5 : test5,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q5 = val;
                            test5 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f05", val);
                          prefs.setString(widget.patientId.toString() + "text5",
                              "Occasionally");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text("Unwilling to disclose"),
                        value: "2",
                        groupValue: test == null ? q5 : test5,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q5 = val;
                            test5 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f05", val);
                          prefs.setString(widget.patientId.toString() + "text5",
                              "Unwilling to disclose");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text("I dont Drink"),
                        value: "3",
                        groupValue: test == null ? q5 : test5,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q5 = val;
                            test5 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f05", val);
                          prefs.setString(widget.patientId.toString() + "text5",
                              "I dont Drink");
                        },
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 15.0,
                ),
                Card(
                  elevation: 5.0,
                  child: Column(
                    children: [
                      Align(
                          alignment: Alignment.topLeft,
                          child: Padding(
                            padding:
                                const EdgeInsets.only(top: 18.0, left: 18.0),
                            child: Text(
                                "How does the skin on the forehead & nose bridge usually feel?",
                                style: TextStyle(
                                  fontSize: 18,
                                )),
                          )),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text("Oily"),
                        value: "0",
                        groupValue: test == null ? q6 : test6,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q6 = val;
                            test6 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f06", val);
                          prefs.setString(
                              widget.patientId.toString() + "text6", "Oily");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text("Normal"),
                        value: "1",
                        groupValue: test == null ? q6 : test6,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q6 = val;
                            test6 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f06", val);
                          prefs.setString(
                              widget.patientId.toString() + "text6", "Normal");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text("Changes as per environment"),
                        value: "2",
                        groupValue: test == null ? q6 : test6,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q6 = val;
                            test6 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f06", val);
                          prefs.setString(widget.patientId.toString() + "text6",
                              "Changes as per environment");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text("Hot and flushed"),
                        value: "3",
                        groupValue: test == null ? q6 : test6,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q6 = val;
                            test6 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f06", val);
                          prefs.setString(widget.patientId.toString() + "text6",
                              "Hot and flushed");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text("Dry"),
                        value: "4",
                        groupValue: test == null ? q6 : test6,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q6 = val;
                            test6 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f06", val);
                          prefs.setString(
                              widget.patientId.toString() + "text6", "Dry");
                        },
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 15.0,
                ),
                Card(
                  child: Column(
                    children: [
                      Align(
                          alignment: Alignment.topLeft,
                          child: Padding(
                            padding:
                                const EdgeInsets.only(top: 18.0, left: 18.0),
                            child: Text(
                                "How does the skin on the sides of your nose feel?",
                                style: TextStyle(
                                  fontSize: 18,
                                )),
                          )),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text("Oily"),
                        value: "0",
                        groupValue: test == null ? q7 : test7,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q7 = val;
                            test7 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f07", val);
                          prefs.setString(
                              widget.patientId.toString() + "text7", "Oily");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text("Normal"),
                        value: "1",
                        groupValue: test == null ? q7 : test7,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q7 = val;
                            test7 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f07", val);
                          prefs.setString(
                              widget.patientId.toString() + "text7", "Normal");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text("Changes as per environment"),
                        value: "2",
                        groupValue: test == null ? q7 : test7,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q7 = val;
                            test7 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f07", val);
                          prefs.setString(widget.patientId.toString() + "text7",
                              "Changes as per environment");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text("Hot and flushed"),
                        value: "3",
                        groupValue: test == null ? q7 : test7,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q7 = val;
                            test7 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f07", val);
                          prefs.setString(widget.patientId.toString() + "text7",
                              "Hot and flushed");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text("Dry"),
                        value: "4",
                        groupValue: test == null ? q7 : test7,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q7 = val;
                            test7 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f07", val);
                          prefs.setString(
                              widget.patientId.toString() + "text7", "Dry");
                        },
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 15.0,
                ),
                Card(
                  elevation: 5.0,
                  child: Column(
                    children: [
                      Align(
                          alignment: Alignment.topLeft,
                          child: Padding(
                            padding:
                                const EdgeInsets.only(top: 18.0, left: 18.0),
                            child:
                                Text("How does the skin on your cheeks feel?",
                                    style: TextStyle(
                                      fontSize: 18,
                                    )),
                          )),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text("Oily"),
                        value: "0",
                        groupValue: test == null ? q8 : test8,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q8 = val;
                            test8 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f08", val);
                          prefs.setString(
                              widget.patientId.toString() + "text8", "Oily");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text("Normal"),
                        value: "1",
                        groupValue: test == null ? q8 : test8,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q8 = val;
                            test8 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f08", val);
                          prefs.setString(
                              widget.patientId.toString() + "text8", "Normal");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text("Changes as per environment"),
                        value: "2",
                        groupValue: test == null ? q8 : test8,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q8 = val;
                            test8 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f08", val);
                          prefs.setString(widget.patientId.toString() + "text8",
                              "Changes as per environment");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text("Hot and flushed"),
                        value: "3",
                        groupValue: test == null ? q8 : test8,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q8 = val;
                            test8 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f08", val);
                          prefs.setString(widget.patientId.toString() + "text8",
                              "Hot and flushed");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text("Dry"),
                        value: "4",
                        groupValue: test == null ? q8 : test8,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q8 = val;
                            test8 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f08", val);
                          prefs.setString(
                              widget.patientId.toString() + "text8", "Dry");
                        },
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 15.0,
                ),
                Card(
                  elevation: 5.0,
                  child: Column(
                    children: [
                      Align(
                          alignment: Alignment.topLeft,
                          child: Padding(
                            padding:
                                const EdgeInsets.only(top: 18.0, left: 18.0),
                            child: Text(
                                "Which of the following statements would you agree with?",
                                style: TextStyle(
                                  fontSize: 18,
                                )),
                          )),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text(
                            "My skin looks extremely shiny and feels sticky"),
                        value: "0",
                        groupValue: test == null ? q9 : test9,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q9 = val;
                            test9 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f09", val);
                          prefs.setString(widget.patientId.toString() + "text9",
                              "My skin looks extremely shiny and feels sticky");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title:
                            Text("My Skin Is Neither Very Oily Nor Very Dry"),
                        value: "1",
                        groupValue: test == null ? q9 : test9,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q9 = val;
                            test9 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f09", val);
                          prefs.setString(widget.patientId.toString() + "text9",
                              "My skin Is neither very oily nor very dry");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text(
                            "I Have An Oily Forehead And Nose Bridge (T Zone)"),
                        value: "2",
                        groupValue: test == null ? q9 : test9,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q9 = val;
                            test9 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f09", val);
                          prefs.setString(widget.patientId.toString() + "text9",
                              "I have an oily forehead and nose bridge (T Zone)");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text(
                            "My skin feels hot,red and flushed with change of temperature/sunexposure"),
                        value: "3",
                        groupValue: test == null ? q9 : test9,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q9 = val;
                            test9 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f09", val);
                          prefs.setString(widget.patientId.toString() + "text9",
                              "My skin feels hot, red and flushed with change of temperature/sun exposure");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text("None Of The Above."),
                        value: "4",
                        groupValue: test == null ? q9 : test9,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q9 = val;
                            test9 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f09", val);
                          prefs.setString(widget.patientId.toString() + "text9",
                              "None Of The Above.");
                        },
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 15.0,
                ),
                Card(
                  elevation: 5.0,
                  child: Column(
                    children: [
                      Align(
                          alignment: Alignment.topLeft,
                          child: Padding(
                            padding:
                                const EdgeInsets.only(top: 18.0, left: 18.0),
                            child: Text(
                                "Please select the option that best fits your skin current skin behavior after washing your face?",
                                style: TextStyle(
                                  fontSize: 18,
                                )),
                          )),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text(
                            "Feels fresh and rejuvenated, shine on cheeks, forehead and nose"),
                        value: "0",
                        groupValue: test == null ? q11 : test11,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q11 = val;
                            test11 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f11", val);
                          prefs.setString(
                              widget.patientId.toString() + "text11",
                              "Feels fresh and rejuvenated, shine on cheeks, forehead and nose");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text(
                            "My skin feels dry immediately after washing but normalizes in sometime"),
                        value: "1",
                        groupValue: test == null ? q11 : test11,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q11 = val;
                            test11 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f11", val);
                          prefs.setString(
                              widget.patientId.toString() + "text11",
                              "My skin feels dry immediately after washing but normalizes in sometime");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text(
                            "My cheeks feel a little dry but nose and forehead(T-Zone) feel fresh and shiny"),
                        value: "2",
                        groupValue: test == null ? q11 : test11,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q11 = val;
                            test11 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f11", val);
                          prefs.setString(
                              widget.patientId.toString() + "text11",
                              "My cheeks feel a little dry but nose and forehead(T-Zone) feel fresh and shiny");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text(
                            "My skin feels red and itchy immediately after washing"),
                        value: "3",
                        groupValue: test == null ? q11 : test11,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q11 = val;
                            test11 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f11", val);
                          prefs.setString(
                              widget.patientId.toString() + "text11",
                              "My skin feels red and itchy immediately after washing");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title:
                            Text("My skin feels stretched, dry and lifeless"),
                        value: "4",
                        groupValue: test == null ? q11 : test11,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q11 = val;
                            test11 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f11", val);
                          prefs.setString(
                              widget.patientId.toString() + "text11",
                              "My skin feels stretched, dry and lifeless");
                        },
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 15.0,
                ),
                Card(
                  elevation: 5.0,
                  child: Column(
                    children: [
                      Align(
                          alignment: Alignment.topLeft,
                          child: Padding(
                            padding:
                                const EdgeInsets.only(left: 18.0, top: 18.0),
                            child: Text(
                                "How does your skin respond to sun exposure/hot humid climate?",
                                style: TextStyle(
                                  fontSize: 18,
                                )),
                          )),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text("My skin feels sticky and looks dark"),
                        value: "0",
                        groupValue: test == null ? q10 : test10,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q10 = val;
                            test10 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f10", val);
                          prefs.setString(
                              widget.patientId.toString() + "text10",
                              "My skin feels sticky and looks dark");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text("My skin feels the same"),
                        value: "1",
                        groupValue: test == null ? q10 : test10,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q10 = val;
                            test10 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f10", val);
                          prefs.setString(
                              widget.patientId.toString() + "text10",
                              "My skin feels the same");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text(
                            "The skin on my nose and forehead tend to become very sticky and shiny"),
                        value: "2",
                        groupValue: test == null ? q10 : test10,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q10 = val;
                            test10 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f10", val);
                          prefs.setString(
                              widget.patientId.toString() + "text10",
                              "The skin on my nose and forehead tend to become very sticky and shiny");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text("My face feels hot ,flushed and red"),
                        value: "3",
                        groupValue: test == null ? q10 : test10,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q10 = val;
                            test10 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f10", val);
                          prefs.setString(
                              widget.patientId.toString() + "text10",
                              "My face feels hot ,flushed and red");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text("My face looks lifeless, dull and patchy"),
                        value: "4",
                        groupValue: test == null ? q10 : test10,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q10 = val;
                            test10 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f10", val);
                          prefs.setString(
                              widget.patientId.toString() + "text10",
                              "My face looks lifeless, dull and patchy");
                        },
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 15.0,
                ),
                Card(
                  child: Column(
                    children: [
                      Align(
                          alignment: Alignment.topLeft,
                          child: Padding(
                            padding:
                                const EdgeInsets.only(top: 18.0, left: 18.0),
                            child: Text(
                                "Please select the option that best describes your skin behavior to moisturisers/cosmetics?",
                                style: TextStyle(
                                  fontSize: 18,
                                )),
                          )),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text(
                            "I cannot use anything on my face, I tend to get pimples immediately"),
                        value: "0",
                        groupValue: test == null ? q12 : test12,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q12 = val;
                            test12 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f12", val);
                          prefs.setString(
                              widget.patientId.toString() + "text12",
                              "I cannot use anything on my face, I tend to get pimples immediately");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text(
                            "I don’t usually need a moisturiser and am comfortable with cosmetics"),
                        value: "1",
                        groupValue: test == null ? q12 : test12,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q12 = val;
                            test12 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f12", val);
                          prefs.setString(
                              widget.patientId.toString() + "text12",
                              "I don’t usually need a moisturizer and am comfortable with cosmetics");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text(
                            "I usually only apply moisturizer on my cheeks and do not get the right kind of cosmetics. The skin on my T-Zone differs from full face"),
                        value: "2",
                        groupValue: test == null ? q12 : test12,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q12 = val;
                            test12 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f12", val);
                          prefs.setString(
                              widget.patientId.toString() + "text12",
                              "I usually only apply moisturizer on my cheeks and don’t get the right kind of cosmetics. The skin on my T-Zone differs from full face");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text(
                            "My skin tends to breakout into rashes,so I use only particular products"),
                        value: "3",
                        groupValue: test == null ? q12 : test12,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q12 = val;
                            test12 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f12", val);
                          prefs.setString(
                              widget.patientId.toString() + "text12",
                              "My skin tends to breakout into rashes, so I use only particular products");
                        },
                      ),
                      RadioListTile(
                        activeColor: AppColors.containerBlack,
                        title: Text(
                            "My skin feels stretched, lifeless, patchy .Need to apply moisturiser atleast 1-2 times a day"),
                        value: "4",
                        groupValue: test == null ? q12 : test12,
                        onChanged: (val) async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            q12 = val;
                            test12 = val;
                          });
                          prefs.setString(
                              widget.patientId.toString() + "f12", val);
                          prefs.setString(
                              widget.patientId.toString() + "text12",
                              "My skin feels stretched, lifeless, patchy .Need to apply moisturizer at least 1-2 times a day");
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
  }
}
