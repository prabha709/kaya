import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:kaya_skincare/Provider/doctor_service_provider.dart';
import 'package:kaya_skincare/screens/Selectedrecommendations.dart';
import 'package:kaya_skincare/utils/Loader.dart';
import 'package:kaya_skincare/utils/app_colors.dart';
import 'package:multi_select_item/multi_select_item.dart';
import 'package:provider/provider.dart';

List<String> savedData = List<String>();
List<String> savedCount = List<String>();
Map selectedDataList;

class DoctorService extends StatefulWidget {
  final int patientId;
  final List selectedProducts;
  final List selectedServices;

  DoctorService({this.patientId, this.selectedProducts, this.selectedServices});

  @override
  _DoctorServiceState createState() => _DoctorServiceState();
}

class _DoctorServiceState extends State<DoctorService>
    with AutomaticKeepAliveClientMixin {
  List doctorServiceDatas;
  DoctorSericeProvider doctorSericeProvider;
  Future fetchData;
  bool loadProducts = false;
  List filterData = [];

  @override
  void initState() {
    doctorSericeProvider = context.read<DoctorSericeProvider>();
    doctorSericeProvider.filterData.clear();
    fetchData = doctorSericeProvider.getDetails();
    doctorSericeProvider.selectedCount = 0;
    savedData.clear();
    savedCount.clear();
    super.initState();
  }

  onSearchTextChanged(String text) async {
    doctorSericeProvider.filterData.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }

    doctorSericeProvider.doctorServiceModel.doctorServices
        .forEach((userDetail) {
      if (userDetail.rowLabels.toLowerCase().contains(text.toLowerCase()))
        doctorSericeProvider.filterData.add(userDetail);
    });
    setState(() {});
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    Future<bool> _loadMore() async {
      print("onLoadMore");
      await Future.delayed(Duration(seconds: 0, milliseconds: 2000));

      return true;
    }

    Size size = MediaQuery.of(context).size;
    doctorSericeProvider =
        Provider.of<DoctorSericeProvider>(context, listen: false);

    return loadProducts
        ? SelectedRecommendation(
            patientId: widget.patientId,
            selectedServices: widget.selectedServices,
            selectedProducts: widget.selectedProducts,
            selectedDservices: savedData,
            selectedCount: savedCount,
          )
        : Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: AppBar(
              flexibleSpace: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/bckg_img.png"),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              centerTitle: true,
              backgroundColor: Colors.white,
              elevation: 0.0,
              automaticallyImplyLeading: false,
              title: Container(
                decoration: BoxDecoration(
                  color: Color(0xffF0F0F0),
                  borderRadius: BorderRadius.all(Radius.circular(35)),
                ),
                width: size.width / 2.5,
                height: size.height / 12,
                child: Center(
                    child: Text(
                  "Select Doctor Services",
                  style: TextStyle(color: Colors.black, fontSize: 25),
                )),
              ),
              toolbarHeight: 160.0,
            ),
            body: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(18.0),
                    child: Card(
                        elevation: 5.0,
                        child: ListTile(
                          title: Container(
                            child: TextFormField(
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  prefixIcon: Icon(Icons.search),
                                  hintStyle: TextStyle(
                                    fontSize: 20.0,
                                    color: Colors.black,
                                  ),
                                  hintText: "Search"),
                              // onChanged: onSearchTextChanged,
                              onFieldSubmitted: onSearchTextChanged,
                            ),
                          ),
                        )),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height / 1.5,
                    // color: Colors.green,
                    child: FutureBuilder(
                        future: fetchData,
                        builder: (context, snapshot) {
                          if (snapshot.connectionState ==
                              ConnectionState.done) {
                            if (!snapshot.hasData) {
                              return Text("No Data");
                            } else {
                              return doctorSericeProvider.filterData.length == 0
                                  ? ListView.builder(
                                      addAutomaticKeepAlives: true,
                                      physics: ScrollPhysics(),
                                      shrinkWrap: true,
                                      itemCount: doctorSericeProvider
                                          .doctorServiceModel
                                          .doctorServices
                                          .length,
                                      itemBuilder: (context, index) {
                                        return Padding(
                                          padding: const EdgeInsets.only(
                                              left: 18.0,
                                              right: 18.0,
                                              top: 8.0),
                                          child: CheckboxData(
                                            index: index,
                                            patientId: widget.patientId,
                                            productName: doctorSericeProvider
                                                .doctorServiceModel
                                                .doctorServices[index]
                                                .rowLabels,
                                          ),
                                        );
                                      })
                                  : ListView.builder(
                                      addAutomaticKeepAlives: true,
                                      physics: ScrollPhysics(),
                                      shrinkWrap: true,
                                      itemCount: doctorSericeProvider
                                          .filterData.length,
                                      itemBuilder: (context, index) {
                                        return Padding(
                                          padding: const EdgeInsets.only(
                                              left: 18.0,
                                              right: 18.0,
                                              top: 8.0),
                                          child: Card(
                                              elevation: 5.0,
                                              child: CheckboxData(
                                                index: index,
                                                patientId: widget.patientId,
                                                productName:
                                                    doctorSericeProvider
                                                        .filterData[index]
                                                        .rowLabels,
                                              )),
                                        );
                                      });
                            }
                          } else {
                            return AppLoader();
                          }
                        }),
                  ),
                ],
              ),
            ),
            floatingActionButton: Container(
              height: 50,
              width: 135,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: AppColors.containerBlack,
                ),
                child: Text(
                  "Next",
                  style: TextStyle(color: AppColors.colorWhite, fontSize: 18.0),
                ),
                onPressed: () {
                  setState(() {
                    loadProducts = true;
                  });
                },
              ),
            ),
          );
  }
}

class CheckboxData extends StatefulWidget {
  final String productName;

  final int patientId;

  final int index;

  CheckboxData({
    this.index,
    this.productName,
    this.patientId,
  });

  @override
  _CheckboxDataState createState() => _CheckboxDataState();
}

class _CheckboxDataState extends State<CheckboxData>
    with AutomaticKeepAliveClientMixin {
  DoctorSericeProvider doctorSericeProvider;
  @override
  void initState() {
    doctorSericeProvider = context.read<DoctorSericeProvider>();
    super.initState();
  }

  MultiSelectController controller = new MultiSelectController();

  Future alertData() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Select Session'),
            content: MyAlertBox(),
            actions: <Widget>[
              TextButton(
                child: Text('NO'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              TextButton(
                child: Text('YES'),
                onPressed: () async {
                  setState(() {
                    controller.toggle(widget.index);
                  });

                  setState(() {
                    if (controller.isSelected(widget.index) == true) {
                      setState(() {
                        savedData.add(widget.productName);
                        savedCount.add(widget.productName +
                            " (${doctorSericeProvider.selectedCount})");
                      });

                      // print("data $savedData");
                    } else {
                      if (controller.isSelected(widget.index) == false)
                        setState(() {
                          int index = savedData.indexOf(widget.productName);
                          savedData.remove(widget.productName);
                          savedCount.removeAt(index);
                        });

                      print("count $savedCount");
                    }

                    //  loaddservice = true;
                  });
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return MultiSelectItem(
      isSelecting: controller.isSelecting,
      onSelected: () async {
        if (savedData.contains(widget.productName)) {
          controller.deselect(widget.index);
          setState(() {
            int index = savedData.indexOf(widget.productName);
            savedData.remove(widget.productName);
            savedCount.removeAt(index);
          });
        } else {
          await alertData();
        }
      },
      child: Card(
        color: savedData.contains(widget.productName)
            ? Colors.black
            : Colors.white,
        // controller.isSelected(widget.index) ? Colors.black : Colors.white,
        elevation: 3.0,
        child: ListTile(
          title: Text(
            widget.productName,
            style: TextStyle(
                color: savedData.contains(widget.productName)
                    ? Colors.white
                    : Colors.black),
          ),
        ),
      ),
    );
    // print(savedData1);
  }
}

class MyAlertBox extends StatefulWidget {
  @override
  _MyAlertBoxState createState() => _MyAlertBoxState();
}

class _MyAlertBoxState extends State<MyAlertBox> {
  DoctorSericeProvider doctorSericeProvider;
  @override
  void initState() {
    doctorSericeProvider = context.read<DoctorSericeProvider>();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Row(
          children: [
            Text("1"),
            Radio(
                value: 1,
                groupValue: doctorSericeProvider.selectedCount,
                onChanged: (value) {
                  setState(() {
                    doctorSericeProvider.selectedCount = value;
                  });
                }),
          ],
        ),
        Row(
          children: [
            Text("2"),
            Radio(
                value: 2,
                groupValue: doctorSericeProvider.selectedCount,
                onChanged: (value) {
                  setState(() {
                    doctorSericeProvider.selectedCount = value;
                  });
                }),
          ],
        ),
        Row(
          children: [
            Text("3"),
            Radio(
                value: 3,
                groupValue: doctorSericeProvider.selectedCount,
                onChanged: (value) {
                  setState(() {
                    doctorSericeProvider.selectedCount = value;
                  });
                }),
          ],
        ),
        Row(
          children: [
            Text("4"),
            Radio(
                value: 4,
                groupValue: doctorSericeProvider.selectedCount,
                onChanged: (value) {
                  setState(() {
                    doctorSericeProvider.selectedCount = value;
                  });
                }),
          ],
        ),
        Row(
          children: [
            Text("6"),
            Radio(
                value: 6,
                groupValue: doctorSericeProvider.selectedCount,
                onChanged: (value) {
                  setState(() {
                    doctorSericeProvider.selectedCount = value;
                  });
                }),
          ],
        ),
      ],
    );
  }
}
