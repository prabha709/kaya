import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:kaya_skincare/utils/BaseUrl.dart';
import "package:http/http.dart" as http;
import 'package:kaya_skincare/utils/app_colors.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ImageUpload extends StatefulWidget {
  final int patientId;

  ImageUpload({this.patientId});
  @override
  _ImageUploadState createState() => _ImageUploadState();
}

class _ImageUploadState extends State<ImageUpload> {
  File tmpFile1;
  File tempFile2;
  File tempFile3;
  bool loading = false;

  List imageData = [];

  selectImage1() async {
    var file = await FilePicker.getFile(
      type: FileType.image,
    );
    if (file != null) {
      setState(() {
        tmpFile1 = file;
      });
    }
  }

  selectImage2() async {
    var file = await FilePicker.getFile(
      type: FileType.image,
    );
    if (file != null) {
      setState(() {
        tempFile2 = file;
      });
    }
  }

  selectImage3() async {
    var file = await FilePicker.getFile(
      type: FileType.image,
    );
    if (file != null) {
      setState(() {
        tempFile3 = file;
      });
    }
  }

  _uploadImage() async {
    String url = BaseUrl.url + "patient_image/${widget.patientId}";
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String token = prefs.getString("token");
    Map<String, String> data = {"Authorization": "token $token"};
    var request = http.MultipartRequest("POST", Uri.parse("$url"));
    request.headers.addAll(data);
    request.files.add(await http.MultipartFile.fromPath(
      "image",
      tmpFile1.path,
    ));
    request.files.add(await http.MultipartFile.fromPath(
      "image",
      tempFile2.path,
    ));
    request.files.add(await http.MultipartFile.fromPath(
      "image",
      tempFile3.path,
    ));
    var res = await request.send();
    final respStr = res.stream.bytesToString();
    print(res.statusCode);
    if (res.statusCode == 201) {
      setState(() {
        tmpFile1 = null;
        tempFile2 = null;
        tempFile3 = null;
        loading = false;
      });
      Fluttertoast.showToast(
          msg: "Image Uploaded Sucessfully..",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.green,
          textColor: Colors.black,
          fontSize: 16.0);
    } else {
      setState(() {
        tmpFile1 = null;
        tempFile2 = null;
        tempFile3 = null;
        loading = false;
      });
      Fluttertoast.showToast(
          msg: "Internal Server Error",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
    // var userData = (respStr as Map<String, dynamic>)['data'];
    print(respStr);

    return respStr;
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    /*24 is for notification bar on Android*/
    final double itemHeight = (size.height - kToolbarHeight - 5);
    final double itemWidth = size.width / 2;
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(
          toolbarHeight: 160.0,
          flexibleSpace: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/bckg_img.png"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          elevation: 0.0,
          automaticallyImplyLeading: false,
          title: Container(
            decoration: BoxDecoration(
              color: Color(0xffF0F0F0),
              borderRadius: BorderRadius.all(Radius.circular(35)),
            ),
            width: screenSize.width / 2.5,
            height: screenSize.height / 10,
            child: Center(
                child: Text(
              "Image Upload",
              style: TextStyle(color: Colors.black, fontSize: 25),
            )),
          ),
        ),
        body: GridView.count(
          crossAxisCount: 2,
          childAspectRatio: itemWidth / itemHeight,
          children: [
            // Text("Left Side Face Image",style: TextStyle(fontSize: 18.0,fontWeight: FontWeight.bold)),
            InkWell(
              onTap: () {
                selectImage1();
              },
              child: Card(
                elevation: 2.0,
                child: Container(
                    height: screenSize.height / 2.6,
                    width: screenSize.width / 4,
                    child: tmpFile1 == null
                        ? Center(
                            child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(Icons.camera),
                              Text("Left Side Face")
                            ],
                          ))
                        : Container(
                            height: screenSize.height / 2.6,
                            width: screenSize.width / 4,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    fit: BoxFit.fill,
                                    image: FileImage(tmpFile1))),
                          )),
              ),
            ),
            // Text("Left Side Face Image",style: TextStyle(fontSize: 18.0,fontWeight: FontWeight.bold)),
            InkWell(
              onTap: () {
                setState(() {
                  selectImage2();
                });
              },
              child: Card(
                elevation: 2.0,
                child: Container(
                    height: screenSize.height / 2.6,
                    width: screenSize.width / 4,
                    child: tempFile2 == null
                        ? Center(
                            child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(Icons.camera),
                              Text("Right Side Face")
                            ],
                          ))
                        : Container(
                            height: screenSize.height / 2.6,
                            width: screenSize.width / 4,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    fit: BoxFit.fill,
                                    image: FileImage(tempFile2))),
                          )),
              ),
            ),

            InkWell(
              onTap: () {
                setState(() {
                  selectImage3();
                });
              },
              child: Card(
                elevation: 2.0,
                child: Container(
                    height: screenSize.height / 2.6,
                    width: screenSize.width / 4,
                    child: tempFile3 == null
                        ? Center(
                            child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(Icons.camera),
                              Text("Straight Face")
                            ],
                          ))
                        : Container(
                            height: screenSize.height / 2.6,
                            width: screenSize.width / 4,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    fit: BoxFit.fill,
                                    image: FileImage(tempFile3))),
                          )),
              ),
            ),
          ],
        ),
        floatingActionButton:
            tmpFile1 != null && tempFile2 != null && tempFile3 != null
                ? FloatingActionButton(
                    onPressed: () {
                      setState(() {
                        loading = true;
                      });
                      _uploadImage();
                    },
                    child: loading == false
                        ? Icon(Icons.upload_file)
                        : Center(
                            child: CircularProgressIndicator(
                              backgroundColor: AppColors.colorAccent,
                            ),
                          ),
                  )
                : Container());
  }
}
