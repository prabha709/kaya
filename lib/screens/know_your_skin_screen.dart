import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:kaya_skincare/Provider/Form1Provider.dart';
import 'package:kaya_skincare/Provider/PatientDetailsProvider.dart';
import 'package:kaya_skincare/Provider/form2_provider.dart';

import 'package:kaya_skincare/screens/Form1StaticQuestions.dart';
import 'package:kaya_skincare/screens/Form2Load.dart';
import 'package:kaya_skincare/screens/form_2_screen.dart';
import 'package:kaya_skincare/utils/Loader.dart';
import 'package:kaya_skincare/utils/app_colors.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'checkbox_for_form1.dart';

class KNowYouSkinScreen extends StatefulWidget {
  final int patientIndexId;
  final String editClicked = '';

  KNowYouSkinScreen({this.patientIndexId});

  @override
  _KNowYouSkinScreenState createState() => _KNowYouSkinScreenState();
}

class _KNowYouSkinScreenState extends State<KNowYouSkinScreen> {
  Form1Provider form1provider;
  Form2Provider form2provider;
  PatientDetailsProvider patientDetailsProvider;
  TextEditingController controller;
  
  bool checkBoxWithTextFieldValue = false;
  List<String> selectedQuestions;
  List<String> checkBoxData = [];

  List<String> selectedAnswers;
  List<String> selectedRadios;

  int lengthOfQuestions;
  int lenghtofOptionsforcheck;
  bool form1 = true;
  bool editSkinType = false;
void updateTheButton() {
  setState(() {
    form1 = false;
  });
}
  getDetails() async {
    patientDetailsProvider = context.read<PatientDetailsProvider>();

    await form1provider.getDetails();
    lengthOfQuestions = form1provider?.lengthOfQuestionsForm1 ?? 1;
    lenghtofOptionsforcheck = form1provider
        .form1model.checkbox[0].questionType.questionOptions.option.length;
    checkBoxData =
        List<String>.generate(lenghtofOptionsforcheck, (index) => "");
    selectedQuestions = List<String>.generate(lengthOfQuestions, (i) => "");
    selectedAnswers = List<String>.generate(lengthOfQuestions, (i) => "");
    selectedRadios = List<String>.generate(lengthOfQuestions, (i) => "");
    form1provider.generateForm1SelectedAnswerList(lengthOfQuestions);
  }

  List selected = [];

  @override
  void initState() {
    Future.delayed(const Duration(milliseconds: 1000), () {
      // getpquestins();
      getDetails();
    });
    form2provider = context.read<Form2Provider>();
    super.initState();
  }

  // getpquestins() async {
  //   int i;

  //   form1provider = context.read<Form1Provider>();
  //   for (i = 0; i < form1provider.form1model.resp.length; i++) {
  //     selected.add(form1provider.form1model.resp[i].questionType.question);
  //   }
  // }

  getCheckBoxSel() async {
    String check;
    String check2;
    String check3;
    String check4;
    SharedPreferences prefs = await SharedPreferences.getInstance();

    check = prefs.getString(widget.patientIndexId.toString() + "Check1");
    check2 = prefs.getString(widget.patientIndexId.toString() + "Check2");
    check3 = prefs.getString(widget.patientIndexId.toString() + "Check3");
    check4 = prefs.getString(widget.patientIndexId.toString() + "Check4");
    List<String> selectedRadioList = [check, check2, check3, check4];
    return selectedRadioList;
  }

  getRadioListFromSp() async {
    String test;
    String test2;
    String test3;
    String test4;
    String test5;
    String test6;
    String test7;
    String test8;
    String test9;
    String test10;
    String test11;
    String test12;

    SharedPreferences prefs = await SharedPreferences.getInstance();

    test = prefs.getString(widget.patientIndexId.toString() + "text1");
    test2 = prefs.getString(widget.patientIndexId.toString() + "text2");
    test3 = prefs.getString(widget.patientIndexId.toString() + "text3");
    test4 = prefs.getString(widget.patientIndexId.toString() + "text4");
    test5 = prefs.getString(widget.patientIndexId.toString() + "text5");
    test6 = prefs.getString(widget.patientIndexId.toString() + "text6");
    test7 = prefs.getString(widget.patientIndexId.toString() + "text7");
    test8 = prefs.getString(widget.patientIndexId.toString() + "text8");
    test9 = prefs.getString(widget.patientIndexId.toString() + "text9");
    test11 = prefs.getString(widget.patientIndexId.toString() + "text11");
    test10 = prefs.getString(widget.patientIndexId.toString() + "text10");
    test12 = prefs.getString(widget.patientIndexId.toString() + "text12");

    List<String> selectedRadioList = [
      test == null ? "None" : test,
      test2 == null ? "None" : test2,
      test3,
      test4,
      test5,
      test6,
      test7,
      test8,
      test9,
      test11,
      test10,
      test12
    ];
    var data = selectedRadioList.cast<String>().toList();
    return data;
  }




  @override
  Widget build(BuildContext context) {

  Future<bool> _onBackPressed() {
     form2provider = context.read<Form2Provider>();

    // void _update(bool checked) {
    //   setState(() => editEnabled = checked);
    // }

    return showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
            // title: Text('your SkinType'),
            content: Builder(
              builder: (context) {
                return Container(
                  height: MediaQuery.of(context).size.height / 2.3,
                  width: MediaQuery.of(context).size.width / 4,
                  child: SkinType(
                    patientId: widget.patientIndexId,
                    skinContext: context,
                    editSkin: updateTheButton,
                  ),
                );
              },
            ),
            // actions: <Widget>[
            //   editSkinType?Container(): TextButton(
            //           child: Text('Continue'),
            //           onPressed: () {
            //             setState(() {
            //               form1 = false;
            //             });
            //             Navigator.pop(context);
            //           },
            //         )
            //       ],
          );
        });
  }
    form1provider = Provider.of<Form1Provider>(context, listen: false);
    patientDetailsProvider = Provider.of<PatientDetailsProvider>(context);

    return form1 == true && checkBoxData != null
        ? Scaffold(
            backgroundColor: Colors.white,
            body: FutureBuilder(
              future: form1provider.getAnswers(widget.patientIndexId),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  if (snapshot.data == false) {
                    return SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 18.0),
                            child: Text(
                              "Know Your Skin Questionnaire",
                              style: TextStyle(fontSize: 25,),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 18, left: 18, right: 18),
                            child: Card(
                              elevation: 5.0,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 18.0, top: 18.0),
                                    child: Text(
                                      "Do you have any of these medical conditions ?",
                                      style: TextStyle(fontSize: 17),
                                    ),
                                  ),
                                  CheckBoxForm1(
                                    checkBoxData: checkBoxData,
                                    patientid: widget.patientIndexId.toString(),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Form1Static(widget.patientIndexId),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    right: 18, bottom: 15),
                                child: Container(
                                  height: 50,
                                  width: 250,
                                  child: ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      primary: AppColors.containerBlack,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              new BorderRadius.circular(30.0)),
                                    ),
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        Text(
                                          "Next",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 17,
                                              fontWeight: FontWeight.w500,
                                              fontFamily: "Montserrat"
                                              ),
                                        ),
                                        Icon(Icons.forward)
                                      ],
                                    ),
                                    onPressed: () async {
                                      int count = 0;
                                      int count2 = 0;
                                      List<String> radioData =
                                          await getRadioListFromSp();
                                      List<String> checkData =
                                          await getCheckBoxSel();
                                      checkData.forEach((element) {
                                        if (element == null) {
                                          count2++;
                                          return;
                                        }
                                      });
                                      print(checkData);
                                      print(radioData);
                                      radioData.forEach((element) {
                                        if (element == null) {
                                          count++;
                                          return;
                                        }
                                      });
                                      if (count > 0) {
                                        return Fluttertoast.showToast(
                                            msg:
                                                "kindly fill all the questions");
                                      } else {
                                        // return Fluttertoast.showToast(msg: "Data validated");

                                        List<String>
                                            checkboxDataWithoutEmptyString = [
                                          "none"
                                        ];

                                        Map postData = {
                                          "checkbox": [
                                            {
                                              "question":
                                                  "Do you have any Medical Condition?",
                                            },
                                            {
                                              "answer":
                                                  checkboxDataWithoutEmptyString
                                            },
                                          ],
                                          "question": [
                                            "Are you pregnant?",
                                            "Are you breast feeding?",
                                            "How much water do you generally drink every day?",
                                            "Do you smoke cigarettes?",
                                            "How often do you drink alcohol?",
                                            "How does the skin on the forehead and nose bridge usually feel?",
                                            "How does the skin on the sides of your nose feel?",
                                            "How does the skin on your cheeks feel?",
                                            "Which of the following statements would you agree with?",
                                            "Please select the option that best fits your skin current skin behavior after washing your face",
                                            "How does your skin respond to sun exposure/hot humid climate?",
                                            "Please select the option that best describes your skin behavior to Moisturizers/Cosmetics"
                                          ],
                                          "answer": radioData,
                                          "flag": true
                                        };
                                        String patientId =
                                            patientDetailsProvider
                                                .patientDetailsModel.id
                                                .toString();

                                        var result = await form1provider
                                            .submitFormDetails(
                                                patientId: patientId,
                                                postData: postData);

                                        if (result == true) {
                                          _onBackPressed();

                                          // setState(() {
                                          //   form1 = false;
                                          // });
                                        } else {
                                          Fluttertoast.showToast(
                                              msg: "Internal Server Error",
                                              toastLength: Toast.LENGTH_SHORT,
                                              gravity: ToastGravity.BOTTOM,
                                              timeInSecForIosWeb: 1,
                                              backgroundColor: Colors.red,
                                              textColor: Colors.white,
                                              fontSize: 16.0);
                                        }
                                      }
                                    },
                                  ),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    );
                  } else {
                    return SingleChildScrollView(
                      child: Column(
                        children: [
                          ListView.builder(
                            key: UniqueKey(),
                            physics: ScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: form1provider
                                .form1answerModel.resp.questionAnswers.length,
                            itemBuilder: (context, index) {
                              String data = form1provider.form1answerModel.resp
                                  .questionAnswers[index].answer;
                              String data2 = data.replaceAll("['", "");
                              String data3 = data2.replaceAll("']", "");
                              return Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Card(
                                  elevation: 5.0,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SingleChildScrollView(
                                        scrollDirection: Axis.horizontal,
                                        child: Row(
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 10.0, left: 20),
                                              child: Text(
                                                  form1provider
                                                      .form1answerModel
                                                      .resp
                                                      .questionAnswers[index]
                                                      .question,
                                                  style: TextStyle(
                                                      fontSize: 20,
                                                      fontWeight:
                                                          FontWeight.bold)),
                                            )
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 20.0, bottom: 10.0),
                                        child: Text("Answer: " + data3,
                                            style: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.green)),
                                      )
                                    ],
                                  ),
                                ),
                              );
                            },
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  height: 60,
                                  width: 120,
                                  child: MaterialButton(
                                    disabledColor: Colors.grey,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(30.0),
                                    ),
                                    textColor: Colors.white,
                                    onPressed: () {
                                      setState(() {
                                        form1 = false;
                                      });
                                    },
                                    color: AppColors.colorAccent,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          'Next',
                                          style: TextStyle(fontSize: 17),
                                        ),
                                        Icon(Icons.arrow_forward)
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    );
                  }
                } else {
                  return AppLoader();
                }
              },
            ),
          )
        : Form2Screen(
            patientindexId: widget.patientIndexId,
          );
  }
}
