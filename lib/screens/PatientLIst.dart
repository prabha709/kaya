import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:kaya_skincare/Provider/DoctorProfileProvider.dart';
import 'package:kaya_skincare/Provider/PatientListProvider.dart';
import 'package:kaya_skincare/screens/AddPatient.dart';
import 'package:kaya_skincare/screens/side_menu_bar.dart';
import 'package:kaya_skincare/utils/BaseUrl.dart';
import 'package:kaya_skincare/utils/Loader.dart';
import 'package:kaya_skincare/utils/app_colors.dart';
import 'package:provider/provider.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

class PatientList extends StatefulWidget {
  final List<CameraDescription> cameras;

  PatientList({
    this.cameras,
  });

  @override
  _PatientListState createState() => _PatientListState();
}

class _PatientListState extends State<PatientList> {
  PatientListProvider patientListProvider;
  List patientList = [];
  List patientPhones = [];
  bool clicked = false;
  bool showSearchBar = false;
  DoctorProfileProvider doctorProfileProvider;
  String shortMessage;
  Color closeButtonColor = Colors.white;
  TextEditingController searchController = TextEditingController();

  String drName;

  @override
  void initState() {
    doctorProfileProvider = context.read<DoctorProfileProvider>();
    patientListProvider = context.read<PatientListProvider>();
    patientListProvider.getDetails();
    doctorProfileProvider.getDetails();
    TimeOfDay now = TimeOfDay.now();

    shortMessage = (now.hour >= 19 || now.hour < 5)
        ? "Good Evening!"
        : ((now.hour >= 5 && now.hour < 12)
            ? "Good Morning!"
            : "Good Afternoon!");
    print(shortMessage);

    super.initState();
  }

  Future<bool> _onBackPressed(int index) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Are your sure you want to delete this client?'),
            content: Text(
                'Once deleted, the client data will not be accessible to you.'),
            actions: <Widget>[
              TextButton(
                child: Text('NO'),
                onPressed: () {
                  setState(() {});
                  Navigator.pop(context);
                },
              ),
              TextButton(
                child: Text('YES'),
                onPressed: () async {
                  await patientListProvider.deleteDetails(
                      patientListProvider.patientListModel.resp[index].id);
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }

  onSearchTextChanged(String text) async {
    patientListProvider.searchData.clear();
    if (text.isEmpty) {
      setState(() {
        closeButtonColor = Colors.white;
      });
      return;
    }
    else if(text.startsWith(RegExp(r'[0-9]'))){
  patientListProvider.patientListModel.resp.forEach((userDetail) {
     if (userDetail.phone.contains(text))
        patientListProvider.searchData.add(userDetail);
  });
   setState(() {
      closeButtonColor = Colors.black;
    });

    }
else{
  
    patientListProvider.patientListModel.resp.forEach((userDetail) {
      if (userDetail.patientName.toLowerCase().contains(text.toLowerCase()))
        patientListProvider.searchData.add(userDetail);
    });

    
    setState(() {
      closeButtonColor = Colors.black;
    });
  }
}


  clearSavedData() {
    patientListProvider.searchData.clear();
  }

  @override
  Widget build(BuildContext context) {
    patientListProvider = Provider.of<PatientListProvider>(context);
    doctorProfileProvider = Provider.of<DoctorProfileProvider>(context);
    Size size = MediaQuery.of(context).size;
    print(widget.cameras);
    drName = doctorProfileProvider.doctorProfileModel?.resp?.fname ?? "";
    print(doctorProfileProvider.doctorProfileModel?.resp?.fname ?? "");
    return Scaffold(
      appBar: AppBar(
        actions: [
          Visibility(
            visible: showSearchBar,
            child: Center(
              child: Container(
                width: size.width * 0.2,
                child: Row(
                  children: [
                    Expanded(
                      child: TextFormField(
                        controller: searchController,
                        onFieldSubmitted: (String text) {
                          setState(() {
                            showSearchBar = false;
                          });
                        },
                        textAlign: TextAlign.left,
                        decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          contentPadding: EdgeInsets.symmetric(horizontal: 35),
                          border: new OutlineInputBorder(
                            borderSide: BorderSide.none,
                            borderRadius: const BorderRadius.all(
                              const Radius.circular(30.0),
                            ),
                          ),
                          hintStyle: TextStyle(
                            fontSize: 20.0,
                            color: Colors.black,
                          ),
                          hintText: "Search",
                        ),
                        onChanged: onSearchTextChanged,
                      ),
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.close,
                        color: closeButtonColor,
                      ),
                      onPressed: () {
                        searchController.clear();
                        patientListProvider.searchData.clear();
                        FocusScopeNode currentFocus = FocusScope.of(context);
                        if (!currentFocus.hasPrimaryFocus) {
                          currentFocus.unfocus();
                        }
                        setState(() {
                          closeButtonColor = Colors.white;
                        });
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
          Visibility(
            visible: !showSearchBar,
            child: InkWell(
              onTap: () {
                setState(() {
                  showSearchBar = showSearchBar ? false : true;
                });
              },
              child: CircleAvatar(
                backgroundColor: Colors.white,
                radius: 35,
                foregroundColor: Colors.black,
                child: Icon(
                  Icons.search,
                  size: 35,
                ),
              ),
            ),
          ),
          SizedBox(width: 55)
        ],
        flexibleSpace: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/bckg_img.png"),
              fit: BoxFit.cover,
            ),
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0.0,
        automaticallyImplyLeading: false,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  InkWell(
                      child:
                          Image.asset("assets/profile_vector.png", scale: 1.5)),
                  SizedBox(width: 15),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        shortMessage,
                        style: TextStyle(
                            color: AppColors.containerBlack,
                            fontSize: 20,
                            fontFamily: "Montserrat",
                            fontWeight: FontWeight.w500),
                      ),
                      SizedBox(height: 10),
                      Text(
                        "Dr. " + drName,
                        style: TextStyle(
                            color: AppColors.containerBlack,
                            fontSize: 20,
                            fontFamily: "Montserrat",
                            fontWeight: FontWeight.w600),
                      )
                    ],
                  )
                ],
              ),
            ),
            Spacer(),
            Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                    color: Color(0xffF0F0F0),
                    borderRadius: BorderRadius.all(Radius.circular(35)),
                  ),
                  width: size.width / 2.5,
                  height: size.height / 12,
                  child: Center(
                      child: Text(
                    "Client List",
                    style: TextStyle(color: Colors.black, fontSize: 25,fontFamily: "Montserrat",fontWeight: FontWeight.w500),
                  )),
                ),
              ],
            ),
            Spacer(),
          ],
        ),
        toolbarHeight: 160.0,
      ),
      body: Container(
        height: size.height,
        width: size.width,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(BaseUrl.image), fit: BoxFit.fill)),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                  height: size.height,
                  child: patientListProvider.patientListModel == null
                      ? AppLoader()
                      : patientListProvider.searchData.length == 0
                          ? AnimationLimiter(
                              child: ListView.builder(
                                  physics: ScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: patientListProvider
                                      .patientListModel.resp.length,
                                  itemBuilder: (context, index) {
                                    // print(patientListProvider.searchData[index].patientName);
                                    return AnimationConfiguration.staggeredList(
                                        position: index,
                                        duration:
                                            const Duration(milliseconds: 700),
                                        child: SlideAnimation(
                                          verticalOffset: 50.0,
                                          child: FadeInAnimation(
                                            child: _listItem(index),
                                          ),
                                        ));
                                  }),
                            )
                          : ListView.builder(
                              itemCount: patientListProvider.searchData.length,
                              itemBuilder: (context, index) {
                                return Padding(
                                  padding: const EdgeInsets.only(
                                      top: 18.0, left: 38.0, right: 38.0),
                                  child: InkWell(
                                    onTap: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => SideMenuBar(
                                            cameras: widget.cameras,
                                            patientIndex: patientListProvider
                                                .searchData[index].id,
                                                patientName: patientListProvider.patientListModel.resp[index].patientName,
                patientAge: patientListProvider.patientListModel.resp[index].age.toString(), 
                                          ),
                                        ),
                                      ).then((value) {
                                        searchController.clear();
                                        setState(() {});
                                      });

                                      FocusScopeNode currentFocus =
                                          FocusScope.of(context);

                                      if (!currentFocus.hasPrimaryFocus) {
                                        currentFocus.unfocus();
                                      }
                                    },
                                    child: Card(
                                      margin: EdgeInsets.only(left: 18.0,right: 18.0,top:13.0),
                                     child: Container(
                                       height: 110.0,
                                       child: Center(
                                         child: ListTile(
                                           dense: true,
      visualDensity: VisualDensity( vertical: 4),
      leading: CircleAvatar(
                                                   
                                                    backgroundColor: Colors.grey,
                                                    child: Center(
                                                      child: Text(
                                                        patientListProvider
                                                            .searchData[index]
                                                            .patientName[0],
                                                       style: TextStyle(
                        fontWeight: FontWeight.w500, fontSize: 18.0,fontFamily: "Montserrat",color: Colors.black),
                                                      ),
                                                    ),
                                                  ),
                                                  title: Text(
                                                    patientListProvider
                                                            .searchData[index]
                                                            .patientName +
                                                        " | " +
                                                        patientListProvider
                                                            .searchData[index].phone
                                                            .toString(),
                                                  style: TextStyle(
                        fontWeight: FontWeight.w500, fontSize: 18.0,fontFamily: "Montserrat",),
                                                  ),
                                                  trailing: Text(
                                                    patientListProvider
                                                        .searchData[index].id
                                                        .toString(),
                                                   style: TextStyle(
                        fontWeight: FontWeight.w500, fontSize: 16.0,fontFamily: "Montserrat",),
                                                  ),

                                         ),
                                       ),
                                     ),
                                    ),
                                  ),
                                );
                              }
                              )
                              ),
            ],
          ),
        ),
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.all(30.0),
        child: Container(
          height: 60,
          width: 60,
          child: FloatingActionButton(
            child: Icon(
              Icons.add,
              color: Colors.white,
            ),
            backgroundColor: Colors.black,
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => AddPatient(),
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  _listItem(index) {
    Size size = MediaQuery.of(context).size;
    return Dismissible(
      key: UniqueKey(),
      onDismissed: (direction) async {
        _onBackPressed(index);
      },
      child: InkWell(
        onTap: () {
          // print(patientListProvider.patientListModel.resp[index].age);
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => SideMenuBar(
                cameras: widget.cameras,
                patientIndex:
                    patientListProvider.patientListModel.resp[index].id,
                patientName: patientListProvider.patientListModel.resp[index].patientName,
                patientAge: patientListProvider.patientListModel.resp[index].age.toString(),    
              ),
            ),
          );
        },
        child: Card(
         margin: EdgeInsets.only(left: 18.0,right: 18.0,top:13.0),
         child: Container(
           height: 110.0,
           child: Center(
             child: ListTile(
            
               leading: CircleAvatar(
                      
                      backgroundColor: Colors.grey,
                      child: Center(
                        child: Text(
                          patientListProvider
                              .patientListModel.resp[index].patientName[0],
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 25.0,
                              fontFamily: "Montserrat",
                              color: Colors.black),
                        ),
                      ),
                    ),
                    title: Text(
                        patientListProvider
                                .patientListModel.resp[index].patientName +
                            " | " +
                            patientListProvider.patientListModel.resp[index].phone
                                .toString(),
                        style: TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 18.0,fontFamily: "Montserrat",),
                      ),
                      trailing: Text(
                        patientListProvider.patientListModel.resp[index].id
                            .toString(),
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          fontFamily: "Montserrat",
                        ),
                      ),
             ),
           ),
         ),
        ),
      ),
    );
  }
}
