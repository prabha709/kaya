import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_downloader/image_downloader.dart';
import 'package:kaya_skincare/Provider/DignosisImageProvider.dart';
import 'package:kaya_skincare/Provider/PatientImageProvider.dart';
import 'package:kaya_skincare/accessCamera/OpenCamera.dart';
import 'package:kaya_skincare/utils/BaseUrl.dart';
import 'package:kaya_skincare/utils/Loader.dart';
import 'package:kaya_skincare/utils/app_colors.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tflite/tflite.dart';
import 'package:http/http.dart' as http;
import 'package:solid_bottom_sheet/solid_bottom_sheet.dart';

class DiagonaliseImage extends StatefulWidget {
  final int patientIndex;
  final bool appBarLoad;

  DiagonaliseImage({
    this.patientIndex,
    this.appBarLoad,
  });

  @override
  _DiagonaliseImageState createState() => _DiagonaliseImageState();
}

class _DiagonaliseImageState extends State<DiagonaliseImage>
    with SingleTickerProviderStateMixin {
  SolidController _controller = SolidController();
  String message;
  String imageData;
  String ssd = "SSD MobileNet";
  String yolo = "Tiny YOLOv2";
  bool selectImage = false;
  bool loading = false;
  PatientImageProvider patientImageProvider;
  Animation<double> _animation;
  AnimationController _animationController;
  bool showfab = true;
  loadModel() async {
    String res;
    res = await Tflite.loadModel(
        model: "assets/detect.tflite", labels: "assets/mask_labelmap.txt");

    print(res);
  }

  DignosisImageProvider dignosisImageProvider;
int _currentPage = 0;
PageController pageController;
  @override
  void initState() {
   
    pageController= PageController(initialPage:_currentPage );
    showfab = true;
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 260),
    );

    final curvedAnimation =
        CurvedAnimation(curve: Curves.easeInOut, parent: _animationController);
    _animation = Tween<double>(begin: 0, end: 1).animate(curvedAnimation);
    selectImage = false;
    super.initState();
  }

  Future<bool> confirmImage(int currentPage) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              'Diagnosis Notification',
              textAlign: TextAlign.center,
            ),
            content: Column(
              children: [
                Text('Are your sure you want to process this Image?'),
                SizedBox(
                  height: 10.0,
                ),
                Expanded(
                                  child: Container(
                  
                    child: FittedBox(
                        fit: BoxFit.fill,
                        child: Image.network(BaseUrl.imageurl + imageData)),
                  ),
                )
              ],
            ),
            actions: <Widget>[
              TextButton(
                child: Text('NO'),
                onPressed: () {
                  setState(() {});
                  Navigator.pop(context);
                },
              ),
              TextButton(
                child: Text('YES'),
                onPressed: () async {
                  _uploadImages(currentPage);
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }

  _uploadImages(int currentPage) async {
    String url = BaseUrl.url + "diagnose_image/${widget.patientIndex}";
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
    });
    String token = prefs.getString("token");
    Map<String, String> data = {"Authorization": "token $token"};

    var request = http.MultipartRequest("POST", Uri.parse("$url"));
    request.headers.addAll(data);
    Map<String, String> concerndata = {"d_image": imageData};

    request.fields.addAll(concerndata);
    // request.files.add(await http.MultipartFile.fromPath("d_image", imageData));

    var res = await request.send();
    final respStr = res.stream.bytesToString();
    print(res.statusCode);
    if (res.statusCode == 200) {
      setState(() {
        loading = false;
        currentPage=0;
      });
      pageController.animateToPage(
      currentPage,
      duration: Duration(milliseconds: 350),
      curve: Curves.easeIn,
    );
      Fluttertoast.showToast(
          msg: "Your Result is Ready.",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.green,
          textColor: Colors.black,
          fontSize: 16.0);

      return respStr;
    } else if (res.statusCode == 204) {
      setState(() {
        loading = false;
      });
      Fluttertoast.showToast(
          msg: "No Face Found in Image",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    } else {
      setState(() {
        loading = false;
      });
      Fluttertoast.showToast(
          msg: "Internal Server Error",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);

      return null;
    }
  }
  
  @override
  Widget build(BuildContext context) {

    patientImageProvider =
        Provider.of<PatientImageProvider>(context, listen: false);
     dignosisImageProvider =
        Provider.of<DignosisImageProvider>(context, listen: false);
    Size screenSize = MediaQuery.of(context).size;

    return Scaffold(
      appBar: widget.appBarLoad == null
          ? AppBar(
              
              centerTitle: true,
              backgroundColor: Colors.white,
              elevation: 0.0,
              automaticallyImplyLeading: false,
              title: Container(
                decoration: BoxDecoration(
                  color: Color(0xffF0F0F0),
                  borderRadius: BorderRadius.all(Radius.circular(35)),
                ),
                width: screenSize.width / 2.5,
                height: screenSize.height / 12,
                child: Center(
                    child: Text(
                  "Diagnosis",
                  style: TextStyle(color: Colors.black, fontSize: 25,fontFamily: "Montserrat",fontWeight: FontWeight.w600),
                )),
              ),
              actions: [
                Padding(
                  padding: const EdgeInsets.only(right: 18.0),
                  child: CircleAvatar(
                    backgroundColor: AppColors.containerBlack,
                    radius: 30.0,
                    child: Center(
                      child: IconButton(
                        icon: Icon(
                          Icons.camera,
                          size: 30.0,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          SystemChrome.setPreferredOrientations(
                              [DeviceOrientation.portraitUp]);
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => OpenCamers(
                                patientIndex: widget.patientIndex,
                                patchImage: false,
                              ),
                            ),
                          );

                          loadModel();
                        },
                      ),
                    ),
                  ),
                )
              ],
              toolbarHeight: 160.0,
            )
          : null,
      
      body:  PageView(
        controller: pageController,
            children: [
              FutureBuilder(
                future: dignosisImageProvider.getDetails(widget.patientIndex),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    if (snapshot.data == false) {
                      return Center(
                        child: Text("No Images Processed Yet!"),
                      );
                    } else {
                      return ListView.builder(
                        shrinkWrap: true,
                          itemCount: 1,
                          itemBuilder: (context, index) {
                            return Row(
                              children: [
                                Expanded(
                                 
                                                                  child: Container(
                                                                    height: MediaQuery.of(context).size.height-160.0,
                                    width: MediaQuery.of(context).size.width / 3,
                                     child: FittedBox(
                                       fit: BoxFit.fill,
                                                                            child: Image.network(
                                         BaseUrl.imageurl +
                                             dignosisImageProvider
                                                 .diagnosisImageModel
                                                 .resp
                                                 .last
                                                 .processImage,
                                       ),
                                     ),
                                   ),
                                ),
                                Expanded(
                                  
                                  child: Container(
                                    height: MediaQuery.of(context).size.height-160.0,
                                          width: MediaQuery.of(context).size.width / 3,
                                    child: Image.network(
                                      BaseUrl.imageurl +
                                          dignosisImageProvider
                                              .diagnosisImageModel
                                              .resp
                                              .last
                                              .dPlot,
                                    ),
                                  ),
                                )
                              ],
                            );
                          });
                    }
                  } else {
                    return AppLoader();
                  }
                },
              ),
loading== false?FutureBuilder(
                future: patientImageProvider.getDetails(widget.patientIndex),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    if (snapshot.data == false) {
                      return Center(
                        child: Text("No Images Processed Yet!"),
                      );
                    } else {
                      return SingleChildScrollView(
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                             Padding(
                               padding: const EdgeInsets.all(8.0),
                               child: Text(
                    "Previous Images",
                    style: TextStyle(
                          fontSize: 18.0,
                          fontWeight: FontWeight.w600,
                          fontFamily: "Montserrat"
                          ),
                          
                  ),
                             ),
                            Container(
                               height: screenSize.height /2,
                                child: ListView.builder(
                                  shrinkWrap: true,
                                            scrollDirection: Axis.horizontal,
                                            itemCount: patientImageProvider
                                                .patientImageModel.all.length,
                                            itemBuilder: (context, index) {
                                              return InkWell(
                                                onTap: () async {
                                                  setState(() {
                                                    imageData = patientImageProvider
                            .patientImageModel.all
                            .elementAt(index)
                            .image;
                                                  });
                                                  confirmImage(_currentPage);
                                                },
                                                child: Column(
                                                  children: [
                                                    Text(patientImageProvider
                            .patientImageModel.all
                            .elementAt(index)
                            .createdDate, style: TextStyle(
                                        fontSize: 16.0,
                          fontWeight: FontWeight.w500,
                          fontFamily: "Montserrat"),),
                                                    Expanded(
                                                                                                          child: Card(
                            elevation: 2.0,
                            child: Container(
                                height: screenSize.height /1.5,
                                width: screenSize.width / 4,
                                decoration: BoxDecoration(
                                    color: Colors.green
                                        .withOpacity(0.5),
                                    image: DecorationImage(
                                        fit: BoxFit.cover,
                                        image: NetworkImage(BaseUrl
                                                .imageurl +
                                            "${patientImageProvider.patientImageModel.all.elementAt(index).image}"))),
                              ),
                                                        ),
                                                    ),
                                                  ],
                                                ),
                                              );
                                            },
                                          ),
                              ),
                              SizedBox(height: 3),
                            Text(
                                  "Post treatment images",
                                  style: TextStyle(
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.w600,
                                      fontFamily: "Montserrat"
                                      ),
                                ),
                                 patientImageProvider.patientImageModel.postImages !=
                                      null
                                  ?Container(
                                        height: screenSize.height /2,
                                        child: ListView.builder(
                                          shrinkWrap: true,
                                          scrollDirection: Axis.horizontal,
                                          itemCount: patientImageProvider
                                              .patientImageModel.postImages.length,
                                          itemBuilder: (context, index) {
                                            return InkWell(
                                              onTap: () async {
                                                setState(() {
                                                  imageData = patientImageProvider
                                                      .patientImageModel.postImages
                                                      .elementAt(index)
                                                      .image;
                                                });
                                                confirmImage(
                                                  _currentPage
                                                );
                                              },
                                              child: Column(
                                                children: [
                                                  Text(patientImageProvider
                                                      .patientImageModel.postImages
                                                      .elementAt(index)
                                                      .createdDate, style: TextStyle(
                                        fontSize: 16.0,
                          fontWeight: FontWeight.w500,
                          fontFamily: "Montserrat"),),
                                                  Expanded(
                                                                                                      child: Card(
                                                        elevation: 2.0,
                                                        child: Container(
                                                          height:
                                                              screenSize.height / 2.6,
                                                          width: screenSize.width / 4,
                                                          decoration: BoxDecoration(
                                                              color: Colors.green
                                 .withOpacity(0.5),
                                                              image: DecorationImage(
                                 fit: BoxFit.cover,
                                 image: NetworkImage(
                                     BaseUrl.imageurl +
                                         "${patientImageProvider.patientImageModel.postImages.elementAt(index).image}"))),
                                                        ),
                                                      ),
                                                  ),
                                                ],
                                              ),
                                            );
                                          },
                                        ),
                                      ):Container(
                                     
                                      child: Center(
                                        child: Text("No Post Images Found"),
                                      ),
                                    )
                          ],
                        ),
                      );
                    }
                  } else {
                    return AppLoader();
                  }
                },
              ):AppLoader(),

            ],
      )
          
    );
  }
}
