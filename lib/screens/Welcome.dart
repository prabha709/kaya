import 'package:flutter/material.dart';
import 'package:kaya_skincare/Login/LoginScreen.dart';
import 'package:kaya_skincare/utils/BaseUrl.dart';

import 'package:camera/camera.dart';
import 'package:kaya_skincare/utils/raised_button.dart';

class WelcomePage extends StatefulWidget {
  final List<CameraDescription> cameras;

  WelcomePage({
    this.cameras,
  });
  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  login(context) {
    // Navigator.pop(context);

    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => LoginScreen(
                cameras: widget.cameras,
              )),
    );
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Colors.white54,
      resizeToAvoidBottomInset: false,
      body:Container(
        height: screenSize.height,
        width: screenSize.width,
             decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(BaseUrl.image), fit: BoxFit.fill)),
        child: Container(
          margin: EdgeInsets.only(top:18.0, right: 18.0,left: 18.0),
          child: Column(
            children: [
              Expanded(
                child: Container(
                 
                  color: Colors.white.withOpacity(0.2),
                  child: Row(
                    children: [
                        Image.asset(
                        "assets/Kaya_Clinic_logo 2.png",
                        
                      ),
                      Spacer(),
                      Image.asset("assets/kaya_login_image_1.png"),
                      SizedBox(width: 25),
                      Text("18+ yrs of \nDermatological Expertise",style:TextStyle(fontFamily: "Montserrat",fontSize: 14.0,fontWeight: FontWeight.w500),),
                      Spacer(),
                      Image.asset("assets/login_image_2.png"),
                      SizedBox(width: 25),
                      Text("Largest dermatologist\nclinic chain in india",style:TextStyle(fontFamily: "Montserrat",fontSize: 14.0,fontWeight: FontWeight.w500),),
                      Spacer(),
                      Image.asset("assets/login_image_3.png"),
                      SizedBox(width: 25),
                      Text("Customized solutions for\nIndian skin & hair",style:TextStyle(fontFamily: "Montserrat",fontSize: 14.0,fontWeight: FontWeight.w500),),
                      Spacer(),
                    ],
                  ),
                ),
              ),
              Row(
                children: [
                  Expanded(
                                    child: Container(
                      
                        
                        
                          child: Image.asset(
                            "assets/login_primary_image.png",
                           
                          ),
                        ),
                  ),
                      Expanded(
                                            child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: 75),
                            Text(
                              "Kaya Doctor App",
                              style: TextStyle(
                fontSize: 28, fontFamily: "Montserrat",fontWeight: FontWeight.w600),
                            ),
                            SizedBox(height: 25),
                            Text(
                              "We welcome our doctors to our new AI skin\nconsulting experience.",
                              style: TextStyle(
                fontSize: 18,fontFamily: "Montserrat", fontWeight: FontWeight.w500),
                            ),
                            SizedBox(height: 25),
                            Text(
                              "If you do not have or have forgotten your\ncredentials, or have any feedback for us,\nplease do contact the Admin Team.",
                              style: TextStyle(
                fontSize: 18, fontWeight: FontWeight.w500,fontFamily: "Montserrat",),
                            ),
                            SizedBox(height: 60),
                            MyRaisedButton(
                              onPressed: login,
                              title: "Sign in",
                              textColor: Colors.white,
                              buttonColor: Colors.black,
                            ),
                           
                            // Row(
                            //   mainAxisAlignment: MainAxisAlignment.end,
                            //   children: [
                            //     InkWell(
                            //         onTap: () {
                            //           Navigator.push(
                            //               context,
                            //               MaterialPageRoute(
                            //                   builder: (context) => SignUpScreen()));
                            //         },
                            //         child: Text(
                            //           "Create an account",
                            //           style: TextStyle(
                            //               fontSize: 20, fontWeight: FontWeight.w500),
                            //         )),
                            //   ],
                            // ),
                          ],
                        ),
                      ),
                ],
              ),
            ],
          ),
        ))
    );
  }
}
