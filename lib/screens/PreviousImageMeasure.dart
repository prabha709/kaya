import 'package:flutter/material.dart';
import 'package:kaya_skincare/Provider/PatientImageProvider.dart';
import 'package:kaya_skincare/localDb/Database_helper.dart';
import 'package:kaya_skincare/utils/BaseUrl.dart';
import 'package:kaya_skincare/utils/app_colors.dart';
import 'package:provider/provider.dart';

List selectedcount = List<String>();

class PastImages extends StatefulWidget {
  final String images;
  final String datetext;
  final int index;

  PastImages({this.datetext, this.images, this.index});
  @override
  _PastImagesState createState() => _PastImagesState();
}

class _PastImagesState extends State<PastImages> {
  bool isSelected = false;

  PatientImageProvider patientImageProvider;

  @override
  void initState() {
    patientImageProvider = context.read<PatientImageProvider>();

    patientImageProvider.selectedData.clear();

    super.initState();
  }
   

  @override
  Widget build(BuildContext context) {
   
    PatientImageProvider patientImageProvider =
        Provider.of<PatientImageProvider>(context, listen: false);
    var size = MediaQuery.of(context).size;

    return InkWell(
      onTap: () async {
        // patientImageProvider.selectedData.forEach((element) async{ 
        //   List<Map<String, dynamic>> queryall =await DatabaseHelper.instance.queryAll();
        //   print(queryall);
        // List<Map<String, dynamic>> customqueryRow =
        //       await DatabaseHelper.instance.customquery(widget.images );
        //       if(customqueryRow.length!=0){
        //          await DatabaseHelper.instance.delete(widget.images);
        //          print(customqueryRow);
        //       }else{
        //        DatabaseHelper.instance.insert({
              
        //       DatabaseHelper.columnName: widget.images,
        //        });
              
        //       }

        // });
        if (patientImageProvider.selectedData.contains(widget.images)) {
          setState(() {
            patientImageProvider.removeCount(widget.images);

            isSelected = false;
          });
        } else if (patientImageProvider.selectedData.length <= 1) {
          setState(() {
            patientImageProvider.getcount(widget.images);
            isSelected = true;
          });
          print(widget.images);
        } else {
          print("data");
        }
      },
      child: Column(
        children: [
          Text(widget.datetext.toString()),
          Container(
            color: isSelected == false ? Colors.white : AppColors.baseColor,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                elevation: 2.0,
                child: Stack(
                  children: [
                    Container(
                        height: size.height / 2.6,
                        width: size.width / 4,
                        decoration: BoxDecoration(
                            color: Colors.green.withOpacity(0.5),
                            image: DecorationImage(
                                fit: BoxFit.cover,
                                image: NetworkImage(
                                    BaseUrl.imageurl + "${widget.images}"))),
                      ),
                    isSelected == false 
                        ? Container()
                        : CircleAvatar(
                            radius: 30.0,
                            backgroundColor: Colors.black,
                            child: Consumer<PatientImageProvider>(
                              builder: (context, model, child) => (Text(
                                ((patientImageProvider.selectedData
                                            .indexOf(widget.images) +
                                        1)
                                    .toString()),
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20.0),
                              )),
                            ),
                          )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
