import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_downloader/image_downloader.dart';
import 'package:kaya_skincare/Provider/MeasureImproveProvider.dart';
import 'package:kaya_skincare/Provider/PatientImageProvider.dart';

import 'package:kaya_skincare/screens/PreviousImageMeasure.dart';
import 'package:kaya_skincare/screens/before_after_screen_2.dart';
import 'package:kaya_skincare/utils/BaseUrl.dart';
import 'package:kaya_skincare/utils/Loader.dart';

import 'package:provider/provider.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:solid_bottom_sheet/solid_bottom_sheet.dart';
import 'package:tflite/tflite.dart';

class MeasureImages extends StatefulWidget {
  final int patientId;

  MeasureImages({
    this.patientId,
  });
  @override
  _MeasureImagesState createState() => _MeasureImagesState();
}

class _MeasureImagesState extends State<MeasureImages>
    with SingleTickerProviderStateMixin {
  SolidController _controller = SolidController();
  bool loadimage = false;
  bool loading = false;
  Animation<double> _animation;
  String imageData1;
  String imageData2;
  AnimationController _animationController;
  MeasureImprovementsProvider measureImprovementsProvider;
  PatientImageProvider patientImageProvider;
  loadModel() async {
    String res;
    res = await Tflite.loadModel(
        model: "assets/detect.tflite", labels: "assets/mask_labelmap.txt");

    print(res);
  }

  bool showfab = true;
int _currentPage = 0;
PageController pageController;
  @override
  void initState() {
    loadimage = false;
pageController= PageController(initialPage:_currentPage );
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 260),
    );

    final curvedAnimation =
        CurvedAnimation(curve: Curves.easeInOut, parent: _animationController);
    _animation = Tween<double>(begin: 0, end: 1).animate(curvedAnimation);

    super.initState();
  }

  Future<bool> _compareButton(int currentPage) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Select any one Concern?'),
            content: ConcernType(),
            actions: <Widget>[
              TextButton(
                child: Text('NO'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              TextButton(
                child: Text('YES'),
                onPressed: () async {
                  
                  _uploadImages(selectedsession,currentPage);
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }

  _uploadImages(String selectedsession, int currentPage) async {
    String url =
        BaseUrl.url + "patient_measure_improvement/${widget.patientId}";
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
    });
    String token = prefs.getString("token");
    Map<String, String> data = {"Authorization": "token $token"};

    var request = http.MultipartRequest("POST", Uri.parse("$url"));
    request.headers.addAll(data);

    Map<String, String> concerndata = {
      "image": imageData1,
      "current_image": imageData2,
      "concern_type": selectedsession,
    };
    // request.files.add(await http.MultipartFile.fromPath("image", imageData1));
    // request.files
    //     .add(await http.MultipartFile.fromPath("current_image", imageData2));
    request.fields.addAll(concerndata);
    var res = await request.send();
    final respStr = res.stream.bytesToString();
    print(res.statusCode);
    if (res.statusCode == 201) {
      setState(() {
        loading = false;
 _currentPage=0;
      });
       pageController.animateToPage(
      _currentPage,
      duration: Duration(milliseconds: 350),
      curve: Curves.easeIn,
    );
      Fluttertoast.showToast(
          msg: "Your Result is Ready.",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.green,
          textColor: Colors.black,
          fontSize: 16.0);
      return respStr;
    } else if (res.statusCode == 204) {
      setState(() {
        loading = false;
      });
      Fluttertoast.showToast(
          msg: "No Face Found in Image",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    } else {
      setState(() {
        loading = false;
      });
      Fluttertoast.showToast(
          msg: "Internal Server Error",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);

      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    patientImageProvider =
        Provider.of<PatientImageProvider>(context, listen: false);
    measureImprovementsProvider =
        Provider.of<MeasureImprovementsProvider>(context, listen: false);
    Size screenSize = MediaQuery.of(context).size;
    double screenHeight = screenSize.height;
    double screenWidth = screenSize.width;

    return Scaffold(
      appBar: AppBar(
       
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0.0,
        automaticallyImplyLeading: false,
        title: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                color: Color(0xffF0F0F0),
                borderRadius: BorderRadius.all(Radius.circular(35)),
              ),
              width: screenSize.width / 2.5,
              height: screenSize.height / 12,
              child: Center(
                  child: Text(
                "Measure Improvements",
                style: TextStyle(color: Colors.black, fontSize: 25),
              )),
            ),
          ],
        ),
        toolbarHeight: 160.0,
      ),
      backgroundColor: Colors.white,
      body: PageView(
        controller: pageController,
            children: [
              FutureBuilder(
          future: measureImprovementsProvider.getDetails(widget.patientId),
          builder: (context, snapshot) {
            if(snapshot.connectionState == ConnectionState.done){
if (snapshot.data == false) {
                return Center(
                  child: Text("No images Compared Yet!"),
                );
            }else{
              return  ListView.builder(
                
                shrinkWrap: true,
                    itemCount: 1,
                    itemBuilder: (context, index) {
                      double skinScore = (measureImprovementsProvider
                              .measureImproveModel.resp.last.skinScore) /
                          10 *
                          100;
                          return Column(
                            children: [
                              Container(
                                child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Text(
                                        measureImprovementsProvider
                                            .measureImproveModel
                                            .resp
                                            .last
                                            .createdDate,
                                        style: TextStyle(fontSize: 18),
                                      ),
                                      Text(
                                          measureImprovementsProvider
                                              .measureImproveModel
                                              .resp
                                              .last
                                              .updatedDate,
                                          style: TextStyle(fontSize: 18)),
                                    ],
                                  ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                                                          child: Card(
                                        elevation: 5.0,
                                        child: Container(
                                          height: screenHeight -160,
                                          width: screenWidth / 3,
                                          child: FittedBox(
                                            fit: BoxFit.fill,
                                            child: Image.network(
                                                BaseUrl.imageurl +
                                                    measureImprovementsProvider
                                                        .measureImproveModel
                                                        .resp
                                                        .last
                                                        .image),
                                          ),
                                        ),
                                      ),
                                    ),
                                   
                                    Expanded(
                                                                          child: Card(
                                        elevation: 5.0,
                                        child: Container(
                                          height: screenHeight -160,
                                          width: screenWidth / 3,
                                          child: FittedBox(
                                            fit: BoxFit.fill,
                                            child: Image.network(
                                                BaseUrl.imageurl +
                                                    measureImprovementsProvider
                                                        .measureImproveModel
                                                        .resp
                                                        .last
                                                        .processImage),
                                          ),
                                          decoration: BoxDecoration(),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Text(
                                "Congratulations! Your Skin has Improved by",
                                style: TextStyle(
                                      fontSize: 18.0,
                        fontWeight: FontWeight.w600,
                        fontFamily: "Montserrat")),
                                    new CircularPercentIndicator(
                              radius: 80.0,
                              lineWidth: 5.0,
                              percent: measureImprovementsProvider
                                      .measureImproveModel.resp.last.skinScore /
                                  10,
                              center: Text(skinScore.round().toString() + "%"),
                              progressColor: Colors.green,
                            ),
                              ],
                            ),
                                  
                                )

                            ],
                          );
                    }
                    );
            }
            }else{
              return Center(child: AppLoader());
            }
            
          }
          ),


        loading== false? FutureBuilder(
           future: patientImageProvider.getDetails(widget.patientId),
           builder: (context, snapshot){
              if (snapshot.connectionState == ConnectionState.done) {
                    if (snapshot.data == false) {
                      return Center(
                        child: Text("No Images Processed Yet!"),
                      );
                    }else{
                      return SingleChildScrollView(
                      child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      "Previous Images",
                                      style: TextStyle(
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.w600,
                          fontFamily: "Montserrat"),
                                    ),
                                  ),
                                
                                  Container(
                                      height:
                                      MediaQuery.of(context).size.height / 2,
                                      child: ListView.builder(
                                        
                                        addAutomaticKeepAlives: true,
                                        scrollDirection: Axis.horizontal,
                                        shrinkWrap: true,
                                        itemCount: patientImageProvider
                                            .patientImageModel.all.length,
                                        itemBuilder: (context, index) {
                                          return PastImages(
                                            datetext: patientImageProvider
                                                .patientImageModel.all
                                                .elementAt(index)
                                                .createdDate,
                                            images: patientImageProvider
                                                .patientImageModel.all
                                                .elementAt(index)
                                                .image,
                                            index: index,
                                          );
                                        },
                                      ),
                                    ),
                                  patientImageProvider
                                              .patientImageModel.postImages !=
                                          null
                                     ? Text(
                                       "Post treatment images",
                                       style: TextStyle(
                                           fontSize: 18.0,
                                           fontWeight: FontWeight.w600,
                          fontFamily: "Montserrat"),
                                     )
                                      : Text(""),
                                
                                  patientImageProvider
                                              .patientImageModel.postImages !=
                                          null
                                      ? Container(
                                         height:
                                            MediaQuery.of(context).size.height /
                                                2,    
                                            child: ListView.builder(
                                              scrollDirection: Axis.horizontal,
                                              shrinkWrap: true,
                                              itemCount: patientImageProvider
                                                  .patientImageModel
                                                  .postImages
                                                  .length,
                                              itemBuilder: (context, index) {
                                                return PastImages(
                                                  datetext: patientImageProvider
                                                      .patientImageModel.postImages
                                                      .elementAt(index)
                                                      .createdDate,
                                                  images: patientImageProvider
                                                      .patientImageModel.postImages
                                                      .elementAt(index)
                                                      .image,
                                                  index: index,
                                                );
                                              },
                                            ),
                                          )
                                      : Container(),
                                 Align(

                                   alignment: Alignment.bottomRight,
                                                                    child: Padding(
                                                                      padding: const EdgeInsets.only(right:8.0),
                                                                      child: Container(
                              height: MediaQuery.of(context).size.height / 10,
                              width: MediaQuery.of(context).size.width / 5,
                              child: RaisedButton(
                                color: Colors.black,
                                child: Text("Compare",
                                      style: TextStyle(color: Colors.white)),
                                onPressed: () async {
                                    if (patientImageProvider
                                            .selectedData.length ==
                                        2) {
                                      setState(() {
                                        imageData1 =
                                            patientImageProvider.selectedData[0];
                                        imageData2 =
                                            patientImageProvider.selectedData[1];
                                      });
                                      _compareButton(_currentPage);
                                      // _uploadImages();
                                    } else {
                                      Fluttertoast.showToast(
                                          msg: "Kindly select two images");
                                    }
                                },
                              ),
                            ),
                                                                    ),
                                 ),
                                ],
                              ),
          );
                    }
              }else{
                return AppLoader();
              }
           }):AppLoader()
            ],
      )
      
         
    );
  }
}
