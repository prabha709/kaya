import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:kaya_skincare/utils/BaseUrl.dart';
import 'package:kaya_skincare/utils/Loader.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class DoctorProfileScreen extends StatefulWidget {
  final String patientId;

  DoctorProfileScreen({this.patientId});
  @override
  _DoctorProfileScreenState createState() => _DoctorProfileScreenState();
}

class _DoctorProfileScreenState extends State<DoctorProfileScreen> {
  final myController = TextEditingController();
  
  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
 
    Future<bool> onClicked() {
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Add Your Notes here'),
              content: Container(
                height: screenSize.height / 5,
                width: screenSize.width / 3,
                decoration:
                    BoxDecoration(border: Border.all(color: Colors.black)),
                child: TextFormField(
                  controller: myController,
                  maxLines: 5,
                  decoration: InputDecoration(border: InputBorder.none),
                ),
              ),
              actions: <Widget>[
                TextButton(
                  child: Text('Cancel'),
                  onPressed: () {
                    setState(() {});
                    Navigator.pop(context);
                  },
                ),
                TextButton(
                  child: Text('Submit'),
                  onPressed: () async {
                    if (myController.text == null) {
                      Fluttertoast.showToast(msg: "This Field is Required");
                    } else {
                      SharedPreferences prefs =
                          await SharedPreferences.getInstance();
                      String token = prefs.getString("token");
                      String url =
                          BaseUrl.url + "doctor-notes/${widget.patientId}";
                      Map data = {"notes": myController.text};
                      var response = await http.post("$url",
                          headers: {"Authorization": "token $token"},
                          body: data);

                      if (response.statusCode == 200) {
                        Fluttertoast.showToast(
                            msg: "Data Submitted Successfully");
                        Navigator.pop(context);
                      } else {
                        Fluttertoast.showToast(msg: "Internal server Error");
                      }
                    }
                  },
                ),
              ],
            );
          });
    }

    getNotes() async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString("token");
      String url = BaseUrl.url + "doctor-notes/${widget.patientId}";

      var response =
          await http.get("$url", headers: {"Authorization": "token $token"});

      if (response.statusCode == 200) {
        var jsonObject = json.decode(response.body);
        return jsonObject["data"];
      } else {
        return null;
      }
    }

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        flexibleSpace: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/bckg_img.png"),
              fit: BoxFit.cover,
            ),
          ),
        ),
        centerTitle: true,
        toolbarHeight: 160.0,
        title: Container(
          decoration: BoxDecoration(
            color: Color(0xffF0F0F0),
            borderRadius: BorderRadius.all(Radius.circular(35)),
          ),
          width: screenSize.width / 2.5,
          height: screenSize.height / 12,
          child: Center(
              child: Text(
            "Client Notes",
            style: TextStyle(color: Colors.black, fontSize: 25),
          )),
        ),
      ),
      body: FutureBuilder(
        future: getNotes(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.data == null) {
              return Center(
                child: Text("No Record Found"),
              );
            } else {
              return ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) {
                    return ExpansionTile(
                      title: Text(
                        snapshot.data[index]["created_date"].toString(),
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(18.0),
                          child: Card(
                            elevation: 5.0,
                            child: Container(
                              height: screenSize.height / 3,
                              width: screenSize.width,
                              child: Center(
                                child: Text(
                                  snapshot.data[index]["notes"],
                                  style: TextStyle(fontSize: 18.0),
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    );
                  });
            }
          } else {
            return AppLoader();
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        backgroundColor: Colors.black,
        onPressed: () {
          onClicked();
        },
      ),
    );
  }
}
