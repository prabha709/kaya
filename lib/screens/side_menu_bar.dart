import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kaya_skincare/Provider/DignosisImageProvider.dart';
import 'package:kaya_skincare/Provider/PatientListProvider.dart';
import 'package:kaya_skincare/Provider/ServiceCheckProvider.dart';
import 'package:kaya_skincare/screens/DiagonaliseImage.dart';
import 'package:kaya_skincare/screens/DoctorProfileScreen.dart';
import 'package:kaya_skincare/screens/Services.dart';
import 'package:kaya_skincare/screens/patient_details_screen_content.dart';
import 'package:kaya_skincare/utils/BaseUrl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:camera/camera.dart';
import 'MeasureImages.dart';
import 'before_after_screen_2.dart';

class SideMenuBar extends StatefulWidget {
  final List<CameraDescription> cameras;
  final int patientIndex;
  final int initialIndex;
  final TextEditingController clearSavedData;
  final String patientName;
  final String patientAge;

  SideMenuBar(
      {this.initialIndex,
      this.patientIndex,
      this.cameras,
      this.clearSavedData,
      this.patientName,
      this.patientAge
      });

  @override
  _SideMenuBarState createState() => _SideMenuBarState();
}

class _SideMenuBarState extends State<SideMenuBar> {
  
  ServiceCheckProvider serviceCheckProvider;
  PatientListProvider patientListProvider;
  
  int initIndex;

  bool checkData = false;

  Future<bool> checkSubmitedData() async {
    String url = BaseUrl.url + "patient-service-product/${widget.patientIndex}";
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");
    var response =
        await http.get("$url", headers: {"Authorization": "token $token"});
    if (response.statusCode == 200) {
      setState(() {
        checkData = true;
      });
      return true;
    } else {
      setState(() {
        checkData = false;
      });
      return false;
    }
  }
DignosisImageProvider dignosisImageProvider;
  @override
  void initState() {
    initIndex = widget.initialIndex ?? 1;
    checkSubmitedData();
    serviceCheckProvider = context.read<ServiceCheckProvider>();
    // dignosisImageProvider = context.read<DignosisImageProvider>();
    serviceCheckProvider.getDetails(widget.patientIndex);
  //  dignosisImageProvider.getDetails(widget.patientIndex);
    patientListProvider = context.read<PatientListProvider>();
    super.initState();
  }

  Future<bool> goback() {
    patientListProvider.searchData.clear();
    Navigator.pop(context);
  }
int _selectedIndex = 0;
List  tabData =[
  {
"tabName":"Client Details",
"tabIcon" :"assets/client.png"
  },
  {
"tabName":"Diagnosis",
"tabIcon" :"assets/Vector.png"
  },
  {
"tabName":"Post Treatment",
"tabIcon" :"assets/diagnosis.png"
  },
  {
"tabName":"Recommendations",
"tabIcon" :"assets/produt.png"
  },
  {
"tabName":"Measure Improvement",
"tabIcon" :"assets/measure.png"
  }
];
PageController pageController= PageController();
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: WillPopScope(
          onWillPop: goback,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
           Expanded(
             flex:2,
                        child: Container(
               
              
               decoration: BoxDecoration(
                 image: DecorationImage(
                   image: AssetImage("assets/KayaVertical.png"),
                   fit: BoxFit.fill
                 )
               ),
               child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                 children: [
                   Padding(
                     padding: const EdgeInsets.only(top:50.0, left: 10.0),
                     child: InkWell(
                       onTap: (){
                         Navigator.of(context).push(MaterialPageRoute(builder: (context) => DoctorProfileScreen(
                                  patientId: widget.patientIndex.toString(),
                                ),));
                       },
                                                             child: Column(
                         crossAxisAlignment: CrossAxisAlignment.start,
                         children: [
                           Card(
                            shape: RoundedRectangleBorder(
        borderRadius: const BorderRadius.all(
          Radius.circular(250),
        )),
                             elevation: 2.0,
                                                                     child: CircleAvatar(
                         radius: 25,
                         backgroundColor:
                             Colors.white,
                              child: Text(widget.patientName[0],
                                 style: TextStyle(
                                     color: Colors.black,
                                     fontSize: 25,
                                     fontFamily: "Montserrat",
                                     fontWeight: FontWeight.w500),
                              )
                       
                       ),
                           ),
                           Text(widget.patientName,
                                 style: TextStyle(
                                     color: Colors.black,
                                     fontSize: 20,
                                     fontFamily: "Montserrat",
                                     fontWeight: FontWeight.w600),

                              ),
                              Text("${widget.patientAge} Years Old",
                                 style: TextStyle(
                                     color: Colors.black,
                                     fontSize: 18,
                                     fontFamily: "Montserrat",
                                     fontWeight: FontWeight.w400),
                              )

                         ],
                       ),
                     ),
                   ),
                   SizedBox(height: 10.0,),

                                  ListView.builder(
                                    shrinkWrap: true,
                                    itemCount: tabData.length,
                                    itemBuilder: (context, index){
                                      return InkWell(
                                        onTap: (){
                                          setState(() {
                                            _selectedIndex= index;
                                            pageController.jumpToPage(index);
                                          });

                                        },
                                                                            child: Padding(
                                                                              padding: const EdgeInsets.all(8.0),
                                                                              child: Container(
                                          child: Row(
                                            children: [
                                              AnimatedContainer(
                                                duration: Duration(milliseconds: 300),
                                                height: (_selectedIndex== index)?50:0,
                                                width: 5.0,
                                                color: Colors.black,
                                                 

                                              ),
                                              Expanded(
                                                child: ListTile(
                                                  leading:Image.asset(tabData[index]["tabIcon"]) ,
                                                  title: Text(tabData[index]["tabName"],
                                          style: TextStyle(
                                              fontSize: 17,
                                              fontWeight: FontWeight.w500,
                                              fontFamily: "Montserrat",
                                              color: Colors.black),),
                                                ),
                                              )

                                            ],
                                          ),
                                        ),
                                                                            ),
                                      );

                                    })

                 ],
               ),
             ),
           ),
           Expanded(
             flex: 7,
             child: PageView(
               controller: pageController,
               physics: NeverScrollableScrollPhysics(),
               children: [
//  DoctorProfileScreen(
//                                 patientId: widget.patientIndex.toString(),
//                               ),
                              PatientDetailsScreenContent(
                                patientIndex: widget.patientIndex,
                              ),
                              DiagonaliseImage(
                                patientIndex: widget.patientIndex,
                              ),
                              BeforeAfterScreen2(
                                patientId: widget.patientIndex,
                              ),
                              Services(
                                patientId: widget.patientIndex,
                              ),
                              MeasureImages(
                                patientId: widget.patientIndex,
                              ),
               ],),
           )
            ],
          ),
        ));
  }
}
