import 'package:flutter/material.dart';
import 'package:kaya_skincare/utils/BaseUrl.dart';
import 'package:http/http.dart' as http;
import 'package:kaya_skincare/utils/Loader.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class ServiceCheckSucess extends StatefulWidget {
  final int patientId;

  ServiceCheckSucess({this.patientId});
  @override
  _ServiceCheckSucessState createState() => _ServiceCheckSucessState();
}

class _ServiceCheckSucessState extends State<ServiceCheckSucess> {
  getSelectedData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");
    String url = BaseUrl.url + "patient-service-product/${widget.patientId}";
    var response =
        await http.get("$url", headers: {"Authorization": "token $token"});
    if (response.statusCode == 200) {
      var jsonObject = json.decode(response.body);
      return jsonObject["patient_product_service"];
    } else {
      return null;
    }
  }

  @override
  void initState() {
    getSelectedData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(
          flexibleSpace: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/bckg_img.png"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          elevation: 0.0,
          automaticallyImplyLeading: false,
          title: Container(
            decoration: BoxDecoration(
              color: Color(0xffF0F0F0),
              borderRadius: BorderRadius.all(Radius.circular(35)),
            ),
            width: size.width / 2.5,
            height: size.height / 12,
            child: Center(
                child: Text(
              "Submission Complete",
              style: TextStyle(color: Colors.black, fontSize: 25),
            )),
          ),
          toolbarHeight: 160.0,
        ),
        body: FutureBuilder(
          future: getSelectedData(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              if (snapshot.data == null) {
                return Center(
                  child: Text("No Data Found"),
                );
              } else {
                return SingleChildScrollView(
                  physics: ScrollPhysics(),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      snapshot.data["service"].length != 0
                          ? Padding(
                              padding:
                                  const EdgeInsets.only(top: 18.0, left: 18.0),
                              child: Text(
                                "Selected Kaya Expert Services",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18.0),
                              ),
                            )
                          : Container(),
                      ListView.builder(
                          shrinkWrap: true,
                          key: UniqueKey(),
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: snapshot.data["service"].length,
                          itemBuilder: (context, index) {
                            return Card(
                              elevation: 3.0,
                              child: ListTile(
                                title: Text(
                                    snapshot.data["service"][index]["data"]),
                              ),
                            );
                          }),
                      snapshot.data["product"].length != 0
                          ? Padding(
                              padding:
                                  const EdgeInsets.only(top: 18.0, left: 18.0),
                              child: Text(
                                "Selected Kaya Expert Products",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18.0),
                              ),
                            )
                          : Container(),
                      ListView.builder(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          key: UniqueKey(),
                          itemCount: snapshot.data["product"].length,
                          itemBuilder: (context, index) {
                            return Card(
                              elevation: 3.0,
                              child: ListTile(
                                title: Text(
                                    snapshot.data["product"][index]["data"]),
                              ),
                            );
                          }),
                      snapshot.data["doctor_service"].length != 0
                          ? Padding(
                              padding:
                                  const EdgeInsets.only(top: 18.0, left: 18.0),
                              child: Text(
                                "Selected Doctor Service",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18.0),
                              ),
                            )
                          : Container(),
                      ListView.builder(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          key: UniqueKey(),
                          itemCount: snapshot.data["doctor_service"].length,
                          itemBuilder: (context, index) {
                            return Card(
                              elevation: 3.0,
                              child: ListTile(
                                title: Text(snapshot.data["doctor_service"]
                                    [index]["data"]),
                              ),
                            );
                          }),
                    ],
                  ),
                );
              }
            } else {
              return AppLoader();
            }
          },
        )
        // body: Container(
        //   height: size.height,
        //   width: size.width,
        //   child: Center(
        //       child: Text(
        //           "Thank you, An email of your selected products & Services is sent to the Clinic Manager .", style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold),)),
        // ),
        );
  }
}
