import 'package:flutter/material.dart';
import 'package:kaya_skincare/Provider/PatientDetailsProvider.dart';
import 'package:kaya_skincare/Provider/product_recommendation_provider.dart';
import 'package:kaya_skincare/screens/DoctorService.dart';
import 'package:kaya_skincare/utils/BaseUrl.dart';
import 'package:kaya_skincare/utils/Loader.dart';
import 'package:multi_select_item/multi_select_item.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

List savedData1 = List<String>();

class Products extends StatefulWidget {
  final int patientId;
  final List selectedServices;

  Products({this.patientId, this.selectedServices});

  @override
  _ProductsState createState() => _ProductsState();
}

class _ProductsState extends State<Products> {
  bool loaddservice = false;
  bool checkForTreatmentsClicked = false;
  bool checkForTreatments2Clicked = false;
  bool loading = false;
  bool checkForProductsClicked = false;
  bool checkForProductsClicked2 = false;
  ProductRecommendationProvider productRecommendationProvider;
  List<bool> checkBoxValuesListForServices1 = [];
  List<bool> checkBoxValuesListForServices2 = [];
  List<bool> checkBoxValuesListForProducts1 = [];
  List<bool> checkBoxValuesListForProducts2 = [];
  List<String> service1SelectedCheckText = [];
  List<String> service2SelectedCheckText = [];
  List<String> product1SelectedCheckText = [];
  List<String> product2SelectedCheckText = [];

  PatientDetailsProvider patientDetailsProvider;
  @override
  void initState() {
    savedData1.clear();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ProductRecommendationProvider productRecommendationProvider =
        Provider.of<ProductRecommendationProvider>(context, listen: false);
    Size size = MediaQuery.of(context).size;
    print(widget.selectedServices);
    return loaddservice == false
        ? Scaffold(
            body: FutureBuilder(
              future: productRecommendationProvider
                  .getProductRecommendations(widget.patientId),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  if (!snapshot.data) {
                    return Center(
                        child:
                            Text("Kindly Fill the Forms in the patient Tab"));
                  } else {
                    return ListView.builder(
                        physics: ScrollPhysics(),
                        itemCount: productRecommendationProvider
                            .productRecommendationModel.resp.length,
                        itemBuilder: (context, i) {
                          return Card(
                            child: ExpansionTile(
                              maintainState: true,
                              initiallyExpanded: false,
                              title: Text(
                                  "For " +
                                      productRecommendationProvider
                                          .productRecommendationModel
                                          .resp[i]
                                          .concern,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18.0)),
                              children: [
                                ListView.builder(
                                    physics: ScrollPhysics(),
                                    shrinkWrap: true,
                                    itemCount: productRecommendationProvider
                                        .productRecommendationModel
                                        .resp[i]
                                        .product
                                        .length,
                                    itemBuilder: (context, index) {
                                      return Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: CheckboxData(
                                          index: index,
                                          productName:
                                              productRecommendationProvider
                                                  .productRecommendationModel
                                                  .resp[i]
                                                  .product[index]
                                                  .productName,
                                          patientId: widget.patientId,
                                          productImage:
                                              productRecommendationProvider
                                                  .productRecommendationModel
                                                  .resp[i]
                                                  .product[index]
                                                  .productImage,
                                        ),
                                      );
                                    })
                              ],
                            ),
                          );
                        });
                  }
                } else {
                  return AppLoader();
                }
              },
            ),
            // floatingActionButton: Container(
            //   height: 50,
            //   width: 135,
            //   child: ElevatedButton(
            //     style: ElevatedButton.styleFrom(
            //       primary: AppColors.containerBlack,
            //     ),
            //     child: Text(
            //       "Next",
            //       style: TextStyle(color: AppColors.colorWhite, fontSize: 18.0),
            //     ),
            //     onPressed: () {
            //       setState(() {
            //         loaddservice = true;
            //       });
            //     },
            //   ),
            // ),
          )
        : DoctorService(
            patientId: widget.patientId,
            selectedProducts: savedData1,
            selectedServices: widget.selectedServices,
          );
  }
}

class CheckboxData extends StatefulWidget {
  final String productName;
  final String productImage;
  final int patientId;

  final int index;
  CheckboxData(
      {this.index, this.productName, this.productImage, this.patientId});
  @override
  _CheckboxDataState createState() => _CheckboxDataState();
}

class _CheckboxDataState extends State<CheckboxData> {
  MultiSelectController controller = new MultiSelectController();
  @override
  Widget build(BuildContext context) {
    return MultiSelectItem(
      isSelecting: controller.isSelecting,
      onSelected: () async {
        setState(() {
          controller.toggle(widget.index);
        });
        SharedPreferences prefs = await SharedPreferences.getInstance();
        if (controller.isSelected(widget.index) == true) {
          setState(() {
            savedData1.add(widget.productName);
          });
          prefs.setStringList("selectedpr${widget.patientId}", savedData1);
          var data = prefs.getStringList("selectedpr${widget.patientId}");
          print(data);
        } else {
          setState(() {
            savedData1.remove(widget.productName);
          });
          prefs.setStringList("selectedpr${widget.patientId}", savedData1);
          var data = prefs.getStringList("selectedpr${widget.patientId}");
          print(data);
        }
      },
      child: Card(
        color:
            controller.isSelected(widget.index) ? Colors.black : Colors.white,
        elevation: 3.0,
        child: ListTile(
          leading: CircleAvatar(
              backgroundImage:
                  NetworkImage(BaseUrl.imageurl + widget.productImage)),
          title: Text(
            widget.productName,
            style: TextStyle(
                color: controller.isSelected(widget.index)
                    ? Colors.white
                    : Colors.black),
          ),
        ),
      ),
    );
  }
}
