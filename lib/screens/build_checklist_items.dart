import 'package:flutter/material.dart';
import 'package:kaya_skincare/Provider/Form1Provider.dart';
import 'package:kaya_skincare/utils/app_colors.dart';
import 'package:provider/provider.dart';

class BuildCheckListItems extends StatefulWidget {
  final int index;
  final List<String> selectedQuestions;

  final List<String> selectedAnswers;
  final List<String> selectedRadios;

  List questionOptions;

  BuildCheckListItems(this.index, this.selectedQuestions, this.selectedAnswers, this.selectedRadios);

  @override
  _BuildCheckListItemsState createState() => _BuildCheckListItemsState();
}

class _BuildCheckListItemsState extends State<BuildCheckListItems> {
  String selectedRadio;
  List<String> selectedRadios = [];
  Form1Provider form1provider;

  @override
  void initState() {
    form1provider = context.read<Form1Provider>();
    int lengthOfQuestions = form1provider.form1model.resp.length - 1;
    int lengthofOptionsforcheck = form1provider
        .form1model.checkbox[0].questionType.questionOptions.option.length;

    super.initState();
  }

  onChangeRadioButton(val) {
    form1provider = context.read<Form1Provider>();
    widget.selectedQuestions[widget.index] =
        form1provider.form1model.resp[widget.index].questionType.question;
    print(widget.selectedQuestions);

    setState(() {
      print("selected radio " + val.toString());
      widget.selectedRadios[widget.index] = val.toString();
      form1provider.form1selectedAnswers[widget.index] = val;
      print(form1provider.form1selectedAnswers);
      selectedRadio = form1provider.form1selectedAnswers[widget.index]??val;
    });
  }

  @override
  Widget build(BuildContext context) {
    form1provider = Provider.of<Form1Provider>(context);
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Row(
            children: [
              ///Option 1
              Radio(
                value: "${widget.index}M",
                groupValue: selectedRadio,
                activeColor: AppColors.colorAccent,
                onChanged: (val) {
                  onChangeRadioButton(val);

                  widget.selectedAnswers[widget.index] = form1provider
                      .form1model
                      .resp[widget.index]
                      .questionType
                      .questionOptions
                      .option[0]
                      .option1
                      .text[0]
                      .data1;
                  print(widget.selectedAnswers);
                },
              ),
              Text(form1provider.form1model.resp[widget.index].questionType
                  .questionOptions.option[0].option1.text[0].data1),
            ],
          ),

          ///Option 2
          Row(
            children: [
              Radio(
                value: "${widget.index}N",
                groupValue: selectedRadio,
                activeColor: AppColors.colorAccent,
                onChanged: (val) {
                  onChangeRadioButton(val);

                  widget.selectedAnswers[widget.index] = form1provider
                      .form1model
                      .resp[widget.index]
                      .questionType
                      .questionOptions
                      .option[1]
                      .option2
                      .text[0]
                      .data1;
                  print(widget.selectedAnswers);
                },
              ),
              Text(form1provider.form1model.resp[widget.index].questionType
                  .questionOptions.option[1].option2.text[0].data1),
            ],
          ),

          ///Option 3
          form1provider.form1model.resp[widget.index].questionType
                      .questionOptions.option[2].option3.text[0].data1 !=
                  "None"
              ? Row(
                  children: [
                    Radio(
                      value: "${widget.index}O",
                      groupValue: selectedRadio,
                      activeColor: AppColors.colorAccent,
                      onChanged: (val) {
                        onChangeRadioButton(val);
                        widget.selectedAnswers[widget.index] = form1provider
                            .form1model
                            .resp[widget.index]
                            .questionType
                            .questionOptions
                            .option[2]
                            .option3
                            .text[0]
                            .data1;
                        print(widget.selectedAnswers);
                      },
                    ),
                    form1provider.form1model.resp[widget.index].questionType
                        .questionOptions.option[2].option3.text[0].data1 !=
                        "None"
                        ? Text(form1provider
                        .form1model
                        .resp[widget.index]
                        .questionType
                        .questionOptions
                        .option[2]
                        .option3
                        .text[0]
                        .data1)
                        : Container(),

                  ],
                )
              : Container(),

          ///Option 4
          form1provider.form1model.resp[widget.index].questionType
                      .questionOptions.option[3].option4.text[0].data1 !=
                  "None"
              ? Row(
                children: [
                  Radio(
                      value: "${widget.index}P",
                      groupValue: selectedRadio,
                      activeColor: AppColors.colorAccent,
                      onChanged: (val) {
                        onChangeRadioButton(val);

                        widget.selectedAnswers[widget.index] = form1provider
                            .form1model
                            .resp[widget.index]
                            .questionType
                            .questionOptions
                            .option[3]
                            .option4
                            .text[0]
                            .data1;
                        print(widget.selectedAnswers);
                      },
                    ),

                  form1provider.form1model.resp[widget.index].questionType
                      .questionOptions.option[3].option4.text[0].data1 !=
                      "None"
                      ? Text(form1provider.form1model.resp[widget.index].questionType
                      .questionOptions.option[3].option4.text[0].data1)
                      : Container(),
                ],
              )
              : Container(),



          ///Option 5
          form1provider.form1model.resp[widget.index].questionType
                      .questionOptions.option[4].option5.text[0].data1 !=
                  "None"
              ? Row(
                children: [
                  Radio(
                      value: "${widget.index}",
                      groupValue: selectedRadio,
                      activeColor: AppColors.colorAccent,
                      onChanged: (val) {
                        onChangeRadioButton(val);

                        widget.selectedAnswers[widget.index] = form1provider
                            .form1model
                            .resp[widget.index]
                            .questionType
                            .questionOptions
                            .option[4]
                            .option5
                            .text[0]
                            .data1;
                        print(widget.selectedAnswers);
                      }),
                  form1provider.form1model.resp[widget.index].questionType
                      .questionOptions.option[4].option5.text[0].data1 !=
                      "None"
                      ? Text(form1provider.form1model.resp[widget.index].questionType
                      .questionOptions.option[4].option5.text[0].data1)
                      : Container(),
                ],
              )
              : Container(),

        ],
      ),
    );
  }
}
