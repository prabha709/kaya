import 'package:flutter/material.dart';
import 'package:kaya_skincare/Provider/PatientListProvider.dart';
import 'package:kaya_skincare/screens/know_your_skin_screen.dart';
import 'package:kaya_skincare/screens/patient_details_information_screen.dart';
import 'package:kaya_skincare/screens/patient_images_screen.dart';
import 'package:kaya_skincare/utils/app_colors.dart';
import 'package:provider/provider.dart';

class PatientDetailsScreenContent extends StatefulWidget {
  final int patientIndex;

  PatientDetailsScreenContent({this.patientIndex});

  @override
  _PatientDetailsScreenContentState createState() =>
      _PatientDetailsScreenContentState();
}

class _PatientDetailsScreenContentState
    extends State<PatientDetailsScreenContent>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  int _selectedIndex = 0;
  Color disabledTextColor = Color(0xffc5c5c5);
  Color disabledBoxColor = Color(0xffc5c5c5);
  Color enabledTextColor = Colors.black;

  Color enabledBoxColor = Color(0xffCD8B6D);
  Color defaultTextColor = Color(0xffc5c5c5);
  Color defaultBoxColor = Color(0xffc5c5c5);

  @override
  void initState() {
    patientListProvider = context.read<PatientListProvider>();
    _tabController = TabController(length: 3, vsync: this);

    _tabController.addListener(() {
      setState(() {
        _selectedIndex = _tabController.index;
      });
      print("Selected Index: " + _tabController.index.toString());
    });
    super.initState();
  }

  PatientListProvider patientListProvider;
  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        leading: InkWell(
            onTap: () async{
              await patientListProvider.getDetails();
              Navigator.pop(context);
            },
            child: Icon(
              Icons.chevron_left_sharp,
              size: 40.0,
              color: Colors.black,
            )),
       
        toolbarHeight: 160.0,
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0.0,
        automaticallyImplyLeading: false,
        title: Container(
          decoration: BoxDecoration(
            color: Color(0xffF0F0F0),
            borderRadius: BorderRadius.all(Radius.circular(35)),
          ),
          width: screenSize.width / 2.5,
          height: screenSize.height / 12,
          child: Center(
              child: Text(
            "Client Details",
            style: TextStyle(color: Colors.black, fontSize: 25,fontFamily: "Montserrat",fontWeight: FontWeight.w600),
          )),
        ),
        bottom: TabBar(
          
          controller: _tabController,
          unselectedLabelColor: Colors.black,
          labelColor: Colors.white,
          indicator: BoxDecoration(
            borderRadius: BorderRadius.circular(50),
            color: Colors.black,
          ),
          tabs: [
            Container(
              height: screenSize.height / 17,
              child: Center(
                child: Text(
                  'Information',
                  style: TextStyle(fontSize: 18,fontFamily: "Montserrat",fontWeight: FontWeight.w500),
                ),
              ),
            ),
            Text(
              'Know your Skin',
              style: TextStyle(fontSize: 18,fontFamily: "Montserrat",fontWeight: FontWeight.w500),
            ),
            Text(
              'Images',
              style: TextStyle(fontSize: 18,fontFamily: "Montserrat",fontWeight: FontWeight.w500),
            ),
          ],
        ),
      ),
      body: TabBarView(
        physics: NeverScrollableScrollPhysics(),
        controller: _tabController, children: [
        PatientDetailsInfoScreen(
          patientIndex: widget.patientIndex,
        ),
        KNowYouSkinScreen(
          patientIndexId: widget.patientIndex,
        ),
        PatientImagesScreen(patientindexId: widget.patientIndex)
      ]),
    );
  }
}
