// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:kaya_skincare/Model/saved_question_form1_response.dart';
// import 'package:kaya_skincare/Provider/Form1Provider.dart';
// import 'package:kaya_skincare/Provider/PatientDetailsProvider.dart';
// import 'package:kaya_skincare/screens/know_your_skin_screen.dart';
// import 'package:kaya_skincare/screens/form_2_screen.dart';
// import 'package:kaya_skincare/utils/app_colors.dart';
// import 'package:provider/provider.dart';

// class SelectedAnswersForm1 extends StatefulWidget {
//   final SavedAnswersForm1Response form1SavedAnswerResponse;
//   final int formNumber;

//   SelectedAnswersForm1({this.form1SavedAnswerResponse, this.formNumber});

//   @override
//   _SelectedAnswersForm1State createState() => _SelectedAnswersForm1State();
// }

// class _SelectedAnswersForm1State extends State<SelectedAnswersForm1> {
//   Form1Provider form1provider;
//   bool navigateToForm2 = false;
//   bool navigateToForm1 = false;
//   bool editClicked = false;

//   PatientDetailsProvider patientDetailsProvider;

//   getForm1Answers() async {
//     form1provider = context.read<Form1Provider>();
//     patientDetailsProvider = context.read<PatientDetailsProvider>();
//     String patientId = patientDetailsProvider.patientDetailsModel.id.toString();
//     form1provider.checkFormPage(patientId);
//   }

//   @override
//   void initState() {
//     getForm1Answers();
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     form1provider = Provider.of<Form1Provider>(context);
//     Size screenSize = MediaQuery.of(context).size;
//     if (navigateToForm2) return Form2Screen();
//     if (navigateToForm1)
//       return KNowYouSkinScreen();
//     else
//       return Scaffold(
//         backgroundColor: Colors.white,
//         body: form1provider.savedAnswersForm1Response == null
//             ? Center(
//                 child: CircularProgressIndicator(),
//               )
//             : patientDetailsProvider.patientDetailsModel == null
//                 ? CircularProgressIndicator(
//                     backgroundColor: Colors.red,
//                   )
//                 : SingleChildScrollView(
//                     child: Column(
//                       crossAxisAlignment: CrossAxisAlignment.start,
//                       children: [
//                         Padding(
//                           padding: const EdgeInsets.only(left: 8.0, top: 8.0),
//                           child: Text(
//                             "Patient Form 1",
//                             style: TextStyle(fontSize: 25),
//                           ),
//                         ),
//                         Padding(
//                           padding: const EdgeInsets.only(left: 8.0, top: 18.0),
//                           child: Row(
//                             children: [
//                               Text(
//                                 "Name - ",
//                                 style: TextStyle(fontSize: 20),
//                               ),
//                               Text(
//                                 patientDetailsProvider
//                                     .patientDetailsModel.patientName,
//                                 style: TextStyle(
//                                     color: AppColors.colorAccent, fontSize: 20),
//                               ),
//                             ],
//                           ),
//                         ),
//                         Padding(
//                           padding: const EdgeInsets.only(left: 8.0, top: 8.0),
//                           child: Row(
//                             children: [
//                               Text(
//                                 "Age - ",
//                                 style: TextStyle(fontSize: 20),
//                               ),
//                               Text(
//                                 patientDetailsProvider.patientDetailsModel.age
//                                     .toString(),
//                                 style: TextStyle(
//                                     color: AppColors.colorAccent, fontSize: 20),
//                               ),
//                             ],
//                           ),
//                         ),
//                         SizedBox(height: 10),

//                         SizedBox(height: 25),
//                         // Text(
//                         //   "Selected Form${widget.formNumber.toString()} answers",
//                         //   style: TextStyle(
//                         //       fontSize: 24,
//                         //       fontWeight: FontWeight.w500,
//                         //       color: Colors.blue),
//                         // ),
//                         Padding(
//                           padding: const EdgeInsets.symmetric(horizontal: 15),
//                           child: Row(
//                               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                               children: [
//                                 Text(
//                                   "General Details",
//                                   style: TextStyle(fontSize: 25),
//                                 ),
//                               ]),
//                         ),
//                         SizedBox(height: 20),
//                         Container(
//                           height: screenSize.height * 0.45,
//                           child: ListView.builder(
//                             shrinkWrap: true,
//                             itemCount:
//                                 widget.form1SavedAnswerResponse.question.length,
//                             itemBuilder: (context, index) {
//                               return Container(
//                                 padding: EdgeInsets.symmetric(horizontal: 15),
//                                 child: Column(
//                                   crossAxisAlignment: CrossAxisAlignment.start,
//                                   children: [
//                                     Row(
//                                       crossAxisAlignment:
//                                           CrossAxisAlignment.baseline,
//                                       children: [
//                                         Text(
//                                           (index + 1).toString(),
//                                           style: TextStyle(fontSize: 25),
//                                         ),
//                                         SizedBox(width: 10),
//                                              Text(
//                                               widget.form1SavedAnswerResponse
//                                                   .question[index]
//                                                   .toString(),
//                                               style: TextStyle(fontSize: 25),
//                                               overflow: TextOverflow.fade,
//                                             ),
//                                       ],
//                                     ),
//                                     SizedBox(height: 10),
//                                     Row(
//                                       children: [
//                                         SizedBox(width: 35),
//                                         Text(
//                                           widget.form1SavedAnswerResponse
//                                               .answer[index],
//                                           style: TextStyle(
//                                               fontSize: 18,
//                                               color: Colors.green),
//                                         ),
//                                       ],
//                                     ),
//                                     SizedBox(height: 35),
//                                   ],
//                                 ),
//                               );
//                             },
//                           ),
//                         ),
//                         Row(
//                           children: [
//                             Visibility(
//                               visible: widget.formNumber == 2,
//                               child: MaterialButton(
//                                 shape: RoundedRectangleBorder(
//                                   borderRadius: BorderRadius.circular(30.0),
//                                 ),
//                                 onPressed: () {
//                                   setState(() {
//                                     navigateToForm1 = true;
//                                   });
//                                 },
//                                 child: Text("Previous"),
//                                 color: AppColors.colorAccent,
//                                 textColor: Colors.white,
//                               ),
//                             ),
//                             Spacer(),
//                             Visibility(
//                               visible: widget.formNumber == 1,
//                               child: MaterialButton(
//                                 shape: RoundedRectangleBorder(
//                                   borderRadius: BorderRadius.circular(30.0),
//                                 ),
//                                 onPressed: () {
//                                   setState(() {
//                                     navigateToForm2 = true;
//                                   });
//                                 },
//                                 textColor: Colors.white,
//                                 child: Text("Next"),
//                                 color: AppColors.colorAccent,
//                               ),
//                             ),
//                           ],
//                         )
//                       ],
//                     ),
//                   ),
//       );
//   }
// }
