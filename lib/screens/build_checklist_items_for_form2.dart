import 'package:flutter/material.dart';
import 'package:kaya_skincare/Provider/form2_provider.dart';
import 'package:provider/provider.dart';

class BuildCheckListItemsForForm2 extends StatefulWidget {
  final int index;
  final List<String> selectedQuestions;
  final int patientId;

  final List<String> selectedAnswers;
  final List<String> selectedOptions;
  final List<String> selectedOptionsForm;

  BuildCheckListItemsForForm2(
      this.index,
      this.selectedQuestions,
      this.selectedAnswers,
      this.selectedOptions,
      this.patientId,
      this.selectedOptionsForm);

  @override
  _BuildCheckListItemsForForm2State createState() =>
      _BuildCheckListItemsForForm2State();
}

class _BuildCheckListItemsForForm2State
    extends State<BuildCheckListItemsForForm2> {
  String selectedRadio;

  Form2Provider form2provider;
  bool option1Value = false;

  bool option2Value = false;

  bool option3Value = false;

  bool option4Value = false;

  bool option5Value = false;

  bool option6Value = false;
  List selectedOptions;

  @override
  void initState() {
    form2provider = context.read<Form2Provider>();
    // int lengthOfQuestions = form2provider.form2model.resp.length - 1;
    super.initState();
  }

  onCheckedOrUnchecked(bool isChecked, String option) {
    isChecked
        ? widget.selectedOptions.add(option)
        : widget.selectedOptions.remove(option);
    print(widget.selectedOptions);
  }

  // onChangeRadioButton(val) {
  //   widget.selectedQuestions[widget.index] =
  //       form2provider.form2model.resp[widget.index].questionType.question;
  //
  //   print(widget.selectedQuestions);
  //
  //   setState(() {
  //     selectedRadio = val;
  //     print("selected radio " + selectedRadio.toString());
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    form2provider = Provider.of<Form2Provider>(context);
    print(form2provider.form2model.resp[0].questionType.option.length);
    return ListView.builder(
      physics: ScrollPhysics(),
        shrinkWrap: true,
        itemCount: form2provider.form2model.resp[0].questionType.option.length,
        itemBuilder: (context, index) {
          return Card(
            elevation: 5.0,
            child: CheckboxData(
              productName: form2provider
                  .form2model.resp[0].questionType.option[index].options,
              selectedAnswers: widget.selectedAnswers,
              patientId: widget.patientId,
              selectedOptionsForm: widget.selectedOptionsForm,
            ),
          );
        });
  }
}

class CheckboxData extends StatefulWidget {
  final String productName;

  final int patientId;
  List<String> selectedOptionsForm;
  List<String> selectedAnswers;

  CheckboxData(
      {this.selectedAnswers,
      this.productName,
      this.patientId,
      this.selectedOptionsForm});

  @override
  _CheckboxDataState createState() => _CheckboxDataState();
}

List savedData = [];

class _CheckboxDataState extends State<CheckboxData> {
  bool isChecked = false;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: CheckboxListTile(
        activeColor: Colors.black,
        controlAffinity: ListTileControlAffinity.leading,
        title: Text(widget.productName),
        onChanged: (bool value) {
          setState(() {
            isChecked = isChecked ? false : true;
          });
          if (value) {
            widget.selectedOptionsForm.add(widget.productName);
          } else {
            widget.selectedOptionsForm.remove(widget.productName);
          }
          print(widget.selectedOptionsForm);
        },
        value: isChecked,

        // onSelected: (List<String> checked) async {
        //   SharedPreferences prefs = await SharedPreferences.getInstance();
        //   prefs.setString("form3${widget.patientId}", json.encode(checked));
        //   var data = prefs.getString("form3${widget.patientId}");
        //   var data1 = json.decode(data);
        //   final data2 = (data1 as List)?.map((e) => e as String)?.toList();
        //
        //   setState(() {
        //     widget.selectedAnswers = data2;
        //   });
        //   print("data1 $data1");
        // },
        // checked: widget.selectedAnswers,
      ),
    );
  }
}
