import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:kaya_skincare/Provider/PatientDetailsProvider.dart';
import 'package:kaya_skincare/Provider/ServiceCheckProvider.dart';
import 'package:kaya_skincare/Provider/product_recommendation_provider.dart';
import 'package:kaya_skincare/screens/DoctorService.dart';

import 'package:kaya_skincare/screens/ServiceCheckSuccess.dart';
import 'package:kaya_skincare/screens/ServiceFailsCheck.dart';
import 'package:kaya_skincare/utils/BaseUrl.dart';
import 'package:kaya_skincare/utils/Loader.dart';
import 'package:kaya_skincare/utils/app_colors.dart';
import 'package:multi_select_item/multi_select_item.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

bool selectedRadio = false;
bool loading = false;
List savedData1 = List<String>();
List savedData2 = List<String>();
List<bool> checkBoxValuesListForServices1 = [];
List<String> service2SelectedCheckText = [];
List<String> service1SelectedCheckText = [];

class Services extends StatefulWidget {
  final int patientId;

  Services({
    this.patientId,
  });

  @override
  _ServicesState createState() => _ServicesState();
}

class _ServicesState extends State<Services> {
  bool checkForTreatmentsClicked = false;
  bool checkForTreatments2Clicked = false;
  bool loadDoctorService = false;
  bool loadProducts = false;
  bool checkData = false;
  bool checkForProductsClicked = false;
  bool checkForProductsClicked2 = false;
  bool _result;
  bool isSelected = false;
  ProductRecommendationProvider productRecommendationProvider;

  List<bool> checkBoxValuesListForServices2 = [];
  List<bool> checkBoxValuesListForProducts1 = [];
  List<bool> checkBoxValuesListForProducts2 = [];

  List<String> product1SelectedCheckText = [];
  List<String> product2SelectedCheckText = [];

  PatientDetailsProvider patientDetailsProvider;
  ServiceCheckProvider serviceCheckProvider;

  Future<bool> checkSubmitedData() async {
    String url = BaseUrl.url + "patient-service-product/${widget.patientId}";
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");
    var response =
        await http.get("$url", headers: {"Authorization": "token $token"});
    if (response.statusCode == 200) {
      if (mounted) {
        setState(() {
          checkData = true;
        });
      }
      return true;
    } else {
      if (mounted) {
        setState(() {
          checkData = false;
        });
      }
      return false;
    }
  }

  @override
  void initState() {
    savedData1.clear();
    savedData2.clear();
    serviceCheckProvider = context.read<ServiceCheckProvider>();

    serviceCheckProvider.getDetails(widget.patientId);
    checkSubmitedData();

    super.initState();
    getResult();
  }

  getResult() async {
    var data = await serviceCheckProvider.getDetails(widget.patientId);

    if (mounted) {
      setState(() {
        _result = data;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    productRecommendationProvider =
        Provider.of<ProductRecommendationProvider>(context, listen: false);
    Size size = MediaQuery.of(context).size;
    // readJson();

    return serviceCheckProvider.serviceCheckModel.resp == false
        ? loadDoctorService == false
            ? Scaffold(
                appBar: AppBar(
                 
                  centerTitle: true,
                  backgroundColor: Colors.white,
                  elevation: 0.0,
                  automaticallyImplyLeading: false,
                  title: Container(
                    decoration: BoxDecoration(
                      color: Color(0xffF0F0F0),
                      borderRadius: BorderRadius.all(Radius.circular(35)),
                    ),
                    width: size.width / 2.5,
                    height: size.height / 12,
                    child: Center(
                        child: Text(
                      "Kaya Expert Services",
                      style: TextStyle(color: Colors.black, fontSize: 25,fontFamily: "Montserrat",fontWeight: FontWeight.w600),
                    )),
                  ),
                  toolbarHeight: 160.0,
                ),
                body: FutureBuilder(
                  future: productRecommendationProvider
                      .getProductRecommendations(widget.patientId),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.done) {
                      if (snapshot.data == false) {
                        return ServiceFailsCheck();
                      } else {
                        return SingleChildScrollView(
                          child: Column(
                            children: [
                              Card(
                                child: ExpansionTile(
                                  maintainState: true,
                                  initiallyExpanded: false,
                                  title: Text("Kaya Expert Services",style: TextStyle(fontFamily: "Montserrat",),),
                                  children: [
                                    ListView.builder(
                                        shrinkWrap: true,
                                        physics: ScrollPhysics(),
                                        itemCount: productRecommendationProvider
                                            .productRecommendationModel
                                            .resp
                                            .length,
                                        itemBuilder: (context, i) {
                                          return Card(
                                              child: ExpansionTile(
                                                  maintainState: true,
                                                  initiallyExpanded: false,
                                                  title: Text(
                                                      "For " +
                                                          productRecommendationProvider
                                                              .productRecommendationModel
                                                              .resp[i]
                                                              .concern,
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 18.0)),
                                                  children: [
                                                ListView.builder(
                                                    physics: ScrollPhysics(),
                                                    padding: EdgeInsets.zero,
                                                    itemCount:
                                                        productRecommendationProvider
                                                            .productRecommendationModel
                                                            .resp[i]
                                                            .service
                                                            .length,
                                                    shrinkWrap: true,
                                                    itemBuilder:
                                                        (context, index) {
                                                      return Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .symmetric(
                                                                horizontal: 30),
                                                        child: Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                    .all(8.0),
                                                            child: CheckboxData(
                                                              index: index,
                                                              patientId: widget
                                                                  .patientId,
                                                              serviceNames:
                                                                  productRecommendationProvider
                                                                      .productRecommendationModel
                                                                      .resp[i]
                                                                      .service[
                                                                          index]
                                                                      .serviceName,
                                                              showPopup: productRecommendationProvider
                                                                  .productRecommendationModel
                                                                  .resp[0]
                                                                  .pregBfeedStatus,
                                                            )),
                                                      );
                                                    })
                                              ]));
                                        })
                                  ],
                                ),
                              ),
                              Card(
                                child: ExpansionTile(
                                  title: Text("Kaya Expert Products"),
                                  children: [
                                    ListView.builder(
                                        shrinkWrap: true,
                                        physics: ScrollPhysics(),
                                        itemCount: productRecommendationProvider
                                            .productRecommendationModel
                                            .resp
                                            .length,
                                        itemBuilder: (context, i) {
                                          return Card(
                                            child: ExpansionTile(
                                              maintainState: true,
                                              initiallyExpanded: false,
                                              title: Text(
                                                  "For " +
                                                      productRecommendationProvider
                                                          .productRecommendationModel
                                                          .resp[i]
                                                          .concern,
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 18.0)),
                                              children: [
                                                ListView.builder(
                                                    physics: ScrollPhysics(),
                                                    shrinkWrap: true,
                                                    itemCount:
                                                        productRecommendationProvider
                                                            .productRecommendationModel
                                                            .resp[i]
                                                            .product
                                                            .length,
                                                    itemBuilder:
                                                        (context, index) {
                                                      return Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .all(8.0),
                                                        child: CheckboxData2(
                                                          index: index,
                                                          productName:
                                                              productRecommendationProvider
                                                                  .productRecommendationModel
                                                                  .resp[i]
                                                                  .product[
                                                                      index]
                                                                  .productName,
                                                          patientId:
                                                              widget.patientId,
                                                          productImage:
                                                              productRecommendationProvider
                                                                  .productRecommendationModel
                                                                  .resp[i]
                                                                  .product[
                                                                      index]
                                                                  .productImage,
                                                        ),
                                                      );
                                                    })
                                              ],
                                            ),
                                          );
                                        })
                                  ],
                                ),
                              )
                            ],
                          ),
                        );
                      }
                    } else {
                      return AppLoader();
                    }
                  },
                ),
                floatingActionButton: _result == true
                    ? Container(
                        height: 50,
                        width: 135,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: AppColors.containerBlack,
                          ),
                          child: loading == false
                              ? Text(
                                  "Next",
                                  style: TextStyle(
                                      color: AppColors.colorWhite,
                                      fontSize: 18.0),
                                )
                              : Center(
                                  child: CircularProgressIndicator(),
                                ),
                          onPressed: () async {
                            if (productRecommendationProvider
                                    .productRecommendationModel
                                    .resp[0]
                                    .pregBfeedStatus ==
                                true) {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      title: Text('Confirm prescription'),
                                      content: Text(
                                          'The client is currently pregnant/breast-feeding.\nAre you sure you want to still prescribe these services?'),
                                      actions: <Widget>[
                                        TextButton(
                                          child: Text('NO'),
                                          onPressed: () {
                                            Navigator.pop(context);
                                          },
                                        ),
                                        TextButton(
                                          child: Text('YES'),
                                          onPressed: () async {
                                            setState(() {
                                              loadDoctorService = true;
                                            });
                                            Navigator.pop(context);
                                          },
                                        ),
                                      ],
                                    );
                                  });
                            } else {
                              setState(() {
                                loadDoctorService = true;
                              });
                            }
                          },
                        ),
                      )
                    : Container())
            : DoctorService(
                patientId: widget.patientId,
                selectedProducts: savedData2,
                selectedServices: savedData1,
              )
        : ServiceCheckSucess(
            patientId: widget.patientId,
          );
  }
}

class CheckboxData extends StatefulWidget {
  final String serviceNames;
  final int patientId;
  final int index;
  final bool showPopup;

  CheckboxData({this.index, this.serviceNames, this.patientId, this.showPopup});

  @override
  _CheckboxDataState createState() => _CheckboxDataState();
}

class _CheckboxDataState extends State<CheckboxData> {
  MultiSelectController controller = new MultiSelectController();

  @override
  Widget build(BuildContext context) {
    return MultiSelectItem(
      isSelecting: controller.isSelecting,
      onSelected: () async {
        setState(() {
          controller.toggle(widget.index);
        });

        if (controller.isSelected(widget.index) == true) {
          setState(() {
            savedData1.add(widget.serviceNames);
          });
          print(savedData1);
        } else {
          setState(() {
            savedData1.remove(widget.serviceNames);
          });
          print(savedData1);
        }
      },
      child: Card(
        color:
            controller.isSelected(widget.index) ? Colors.black : Colors.white,
        elevation: 3.0,
        child: ListTile(
          title: Text(
            widget.serviceNames,
            style: TextStyle(
              color: controller.isSelected(widget.index)
                  ? Colors.white
                  : Colors.black,
            ),
          ),
        ),
      ),
    );
  }
}

class CheckboxData2 extends StatefulWidget {
  final String productName;
  final String productImage;
  final int patientId;

  final int index;
  CheckboxData2(
      {this.index, this.productName, this.productImage, this.patientId});
  @override
  _CheckboxData2State createState() => _CheckboxData2State();
}

class _CheckboxData2State extends State<CheckboxData2> {
  MultiSelectController controller = new MultiSelectController();
  @override
  Widget build(BuildContext context) {
    return MultiSelectItem(
      isSelecting: controller.isSelecting,
      onSelected: () async {
        setState(() {
          controller.toggle(widget.index);
        });
        SharedPreferences prefs = await SharedPreferences.getInstance();
        if (controller.isSelected(widget.index) == true) {
          setState(() {
            savedData2.add(widget.productName);
          });
          print(savedData2);
        } else {
          setState(() {
            savedData2.remove(widget.productName);
          });
        }
      },
      child: Card(
        color:
            controller.isSelected(widget.index) ? Colors.black : Colors.white,
        elevation: 3.0,
        child: ListTile(
          leading: CircleAvatar(
              backgroundImage:
                  NetworkImage(BaseUrl.imageurl + widget.productImage)),
          title: Text(
            widget.productName,
            style: TextStyle(
                color: controller.isSelected(widget.index)
                    ? Colors.white
                    : Colors.black),
          ),
        ),
      ),
    );
  }
}
