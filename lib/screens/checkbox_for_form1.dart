import 'package:flutter/material.dart';
import 'package:kaya_skincare/Provider/Form1Provider.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CheckBoxForm1 extends StatefulWidget {
  final List<String> checkBoxData;
  final String patientid;

  CheckBoxForm1({this.checkBoxData, this.patientid});

  @override
  _CheckBoxForState createState() => _CheckBoxForState();
}

class _CheckBoxForState extends State<CheckBoxForm1> {
  bool option1Value = false;

  bool option2Value = false;

  bool option3Value = false;

  bool option4Value = false;

  bool option5Value = false;
  Form1Provider form1provider;

  @override
  Widget build(BuildContext context) {
    form1provider = Provider.of<Form1Provider>(context);
    return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
        
          ///Option 1 Checkbox
          Row(
            children: [
              option5Value
                  ? Container()
                  : Checkbox(
                      value: option1Value,
                      activeColor: Colors.black,
                      checkColor: Colors.white,
                      onChanged: (val) async {
                        SharedPreferences prefs =
                            await SharedPreferences.getInstance();
                        setState(() {
                          option1Value = val ? true : false;
                          val
                              ? widget.checkBoxData.add(form1provider
                                  .form1model
                                  .checkbox[0]
                                  .questionType
                                  .questionOptions
                                  .option[0]
                                  .option1
                                  .text[0]
                                  .data1)
                              : widget.checkBoxData.remove(form1provider
                                  .form1model
                                  .checkbox[0]
                                  .questionType
                                  .questionOptions
                                  .option[0]
                                  .option1
                                  .text[0]
                                  .data1);
                        });
                        prefs.setString(
                            widget.patientid.toString() + "Check1",
                            form1provider
                                .form1model
                                .checkbox[0]
                                .questionType
                                .questionOptions
                                .option[0]
                                .option1
                                .text[0]
                                .data1);
                      },
                    ),
              option5Value
                  ? Container()
                  : Text(form1provider?.form1model != null
                      ? form1provider
                              ?.form1model
                              ?.checkbox
                              ?.first
                              ?.questionType
                              ?.questionOptions
                              ?.option
                              ?.first
                              ?.option1
                              ?.text
                              ?.first
                              ?.data1 ??
                          ""
                      : ""),
            ],
          ),

          ///Option 2
          Row(
            children: [
              option5Value
                  ? Container()
                  : Checkbox(
                      value: option2Value,
                      activeColor: Colors.black,
                      checkColor: Colors.white,
                      onChanged: (val) async {
                        SharedPreferences prefs =
                            await SharedPreferences.getInstance();
                        setState(() {
                          option2Value = val ? true : false;
                          val
                              ? widget.checkBoxData.add(form1provider
                                  .form1model
                                  .checkbox[0]
                                  .questionType
                                  .questionOptions
                                  .option[1]
                                  .option2
                                  .text[0]
                                  .data1)
                              : widget.checkBoxData.remove(form1provider
                                  .form1model
                                  .checkbox[0]
                                  .questionType
                                  .questionOptions
                                  .option[1]
                                  .option2
                                  .text[0]
                                  .data1);
                        });
                        prefs.setString(
                            widget.patientid.toString() + "Check2",
                            form1provider
                                .form1model
                                .checkbox[0]
                                .questionType
                                .questionOptions
                                .option[1]
                                .option2
                                .text[0]
                                .data1);
                      },
                    ),
              option5Value
                  ? Container()
                  : Text(form1provider.form1model == null
                      ? ""
                      : form1provider.form1model.checkbox[0].questionType
                          .questionOptions.option[1].option2.text[0].data1),
            ],
          ),

          ///Option 3
          Row(
            children: [
              option5Value
                  ? Container()
                  : Checkbox(
                      value: option3Value,
                      activeColor: Colors.black,
                      checkColor: Colors.white,
                      onChanged: (val) async {
                        SharedPreferences prefs =
                            await SharedPreferences.getInstance();
                        setState(() {
                          option3Value = val ? true : false;
                          val
                              ? widget.checkBoxData.add(form1provider
                                  .form1model
                                  .checkbox[0]
                                  .questionType
                                  .questionOptions
                                  .option[2]
                                  .option3
                                  .text[0]
                                  .data1)
                              : widget.checkBoxData.remove(form1provider
                                  .form1model
                                  .checkbox[0]
                                  .questionType
                                  .questionOptions
                                  .option[2]
                                  .option3
                                  .text[0]
                                  .data1);
                        });
                        prefs.setString(
                            widget.patientid.toString() + "Check3",
                            form1provider
                                .form1model
                                .checkbox[0]
                                .questionType
                                .questionOptions
                                .option[2]
                                .option3
                                .text[0]
                                .data1);
                      },
                    ),
              option5Value
                  ? Container()
                  : Text(form1provider.form1model != null
                      ? form1provider.form1model.checkbox[0].questionType
                          .questionOptions.option[2].option3.text[0].data1
                      : ""),
            ],
          ),

          ///Option 4
          Row(
            children: [
              option5Value
                  ? Container()
                  : Checkbox(
                      value: option4Value,
                      activeColor: Colors.black,
                      checkColor: Colors.white,
                      onChanged: (bool val) async {
                        SharedPreferences prefs =
                            await SharedPreferences.getInstance();
                        setState(() {
                          option4Value = val ? true : false;
                          val
                              ? widget.checkBoxData.add(form1provider
                                  .form1model
                                  .checkbox[0]
                                  .questionType
                                  .questionOptions
                                  .option[3]
                                  .option4
                                  .text[0]
                                  .data1)
                              : widget.checkBoxData.remove(form1provider
                                  .form1model
                                  .checkbox[0]
                                  .questionType
                                  .questionOptions
                                  .option[3]
                                  .option4
                                  .text[0]
                                  .data1);
                        });
                        prefs.setString(
                            widget.patientid.toString() + "Check4",
                            form1provider
                                .form1model
                                .checkbox[0]
                                .questionType
                                .questionOptions
                                .option[3]
                                .option4
                                .text[0]
                                .data1);
                      },
                    ),
              option5Value
                  ? Container()
                  : Text(form1provider.form1model != null
                      ? form1provider
                              ?.form1model
                              ?.checkbox[0]
                              ?.questionType
                              ?.questionOptions
                              ?.option[3]
                              ?.option4
                              ?.text[0]
                              ?.data1 ??
                          ""
                      : ""),
            ],
          ),
          Row(
            children: [
              Checkbox(
                activeColor: Colors.black,
                checkColor: Colors.white,
                value: option5Value,
                onChanged: (bool val) {
                  setState(() {
                    option5Value = val ? true : false;
                    val
                        ? widget.checkBoxData.add("none")
                        : widget.checkBoxData.remove("none");
                  });
                },
              ),
              Text("None"),
            ],
          ),
        ]);
  }
}
