import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:kaya_skincare/screens/ServiceCheckSuccess.dart';
import 'package:kaya_skincare/utils/BaseUrl.dart';
import 'package:kaya_skincare/utils/app_colors.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SelectedRecommendation extends StatefulWidget {
  final int patientId;
  bool showAppBar;
  final List selectedServices;
  final List selectedProducts;
  final List selectedDservices;
  final List selectedCount;

  SelectedRecommendation(
      {this.patientId,
      this.showAppBar,
      this.selectedDservices,
      this.selectedProducts,
      this.selectedServices,
      this.selectedCount});

  @override
  _SelectedRecommendationState createState() => _SelectedRecommendationState();
}

class _SelectedRecommendationState extends State<SelectedRecommendation> {
  List<String> services;
  List<String> products;
  List<String> dservices;
  List<String> dcount;

  getStoredservice() async {
    setState(() {
      services = widget.selectedServices;
      products = widget.selectedProducts;
      dservices = widget.selectedDservices;
      dcount = widget.selectedCount;
    });
    return services;
  }

  bool loading = false;
  bool success = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return success == false
        ? Scaffold(
            appBar: widget.showAppBar == null
                ? AppBar(
                    flexibleSpace: Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage("assets/bckg_img.png"),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    centerTitle: true,
                    backgroundColor: Colors.white,
                    elevation: 0.0,
                    automaticallyImplyLeading: false,
                    title: Container(
                      decoration: BoxDecoration(
                        color: Color(0xffF0F0F0),
                        borderRadius: BorderRadius.all(Radius.circular(35)),
                      ),
                      width: size.width / 2.5,
                      height: size.height / 12,
                      child: Center(
                          child: Text(
                        "Selected Data",
                        style: TextStyle(color: Colors.black, fontSize: 25),
                      )),
                    ),
                    toolbarHeight: 160.0,
                  )
                : null,
            body: SingleChildScrollView(
              child: Column(
                children: [
                  FutureBuilder(
                    future: getStoredservice(),
                    builder: (context, snapshot) {
                      if (snapshot.data == null) {
                        return Center(
                          child: Text("No Selected Data"),
                        );
                      } else {
                        return SingleChildScrollView(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(18.0),
                                child: Text(
                                  "Selected Kaya Expert Services",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20.0),
                                ),
                              ),
                              snapshot.data.length != 0
                                  ? ListView.builder(
                                      physics: ScrollPhysics(),
                                      key: UniqueKey(),
                                      shrinkWrap: true,
                                      itemCount: snapshot.data.length,
                                      itemBuilder: (context, index) {
                                        return Padding(
                                          padding: const EdgeInsets.only(
                                              top: 8.0,
                                              left: 18.0,
                                              right: 18.0),
                                          child: Card(
                                              elevation: 3.0,
                                              child: Container(
                                                height: 50,
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 13.0, left: 8.0),
                                                  child: Text(
                                                    services[index],
                                                    style: TextStyle(
                                                        fontSize: 18.0),
                                                  ),
                                                ),
                                              )),
                                        );
                                      })
                                  : Align(
                                      alignment: Alignment.topLeft,
                                      child: Padding(
                                        padding: const EdgeInsets.only(
                                            top: 1.5, bottom: 5.0, left: 25.0),
                                        child: Text(
                                          "No Services Selected",
                                          style: TextStyle(fontSize: 18.0),
                                        ),
                                      )),
                            ],
                          ),
                        );
                      }
                    },
                  ),
                  SelectedProducts(
                    patientId: widget.patientId,
                    selectProducts: widget.selectedProducts,
                  ),
                  SelectedDoctorServices(
                    dcounts: widget.selectedCount,
                    patientId: widget.patientId,
                    selectedDservices: widget.selectedDservices,
                  )
                ],
              ),
            ),
            floatingActionButton: widget.showAppBar == null
                ? Container(
                    height: 50.0,
                    width: 120.0,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary: AppColors.containerBlack),
                      child: loading == false
                          ? Text("Submit")
                          : Center(
                              child: CircularProgressIndicator(),
                            ),
                      onPressed: () async {
                        SharedPreferences prefs =
                            await SharedPreferences.getInstance();

                        setState(() {
                          loading = true;
                        });
                        if (dservices != null) {
                          Map data = {
                            "service": services,
                            "product": products,
                            "doctor_service": dcount
                          };
                          print(data);
                          String token = prefs.getString("token");
                          String url =
                              BaseUrl.url + "servicelist/${widget.patientId}";
                          var response = await http.post("$url",
                              headers: {
                                "Authorization": "token $token",
                                "Content-Type": "application/json"
                              },
                              body: json.encode(data));
                          if (response.statusCode == 200) {
                            setState(() {
                              loading = false;
                              success = true;
                            });
                            return Fluttertoast.showToast(
                                msg: "Data Submitted Sucessfully",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.BOTTOM,
                                timeInSecForIosWeb: 1,
                                backgroundColor: Colors.green,
                                textColor: Colors.white,
                                fontSize: 16.0);
                          } else if (response.statusCode == 406) {
                            setState(() {
                              loading = false;
                            });
                            Fluttertoast.showToast(
                                msg: "Data Already Submitted",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.BOTTOM,
                                timeInSecForIosWeb: 1,
                                backgroundColor: Colors.red,
                                textColor: Colors.white,
                                fontSize: 16.0);
                          } else {
                            setState(() {
                              loading = false;
                            });

                            print(response.body);
                            Fluttertoast.showToast(
                                msg: "Internal Server Error",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.BOTTOM,
                                timeInSecForIosWeb: 1,
                                backgroundColor: Colors.red,
                                textColor: Colors.white,
                                fontSize: 16.0);
                          }
                        } else {
                          Fluttertoast.showToast(
                              msg: "Doctor Service is required",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.BOTTOM,
                              timeInSecForIosWeb: 1,
                              backgroundColor: Colors.red,
                              textColor: Colors.white,
                              fontSize: 16.0);
                        }
                      },
                    ),
                  )
                : Container(),
          )
        : ServiceCheckSucess(
            patientId: widget.patientId,
          );
  }
}

class SelectedProducts extends StatefulWidget {
  final int patientId;
  final List selectProducts;

  SelectedProducts({this.patientId, this.selectProducts});

  @override
  _SelectedProductsState createState() => _SelectedProductsState();
}

class _SelectedProductsState extends State<SelectedProducts> {
  List<String> products;
  getStoredProducts() async {
    products = widget.selectProducts;
    return products;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getStoredProducts(),
      builder: (context, snapshot) {
        if (snapshot.data == null) {
          return Center(
            child: Text("No Selected Data"),
          );
        } else {
          return SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: Text(
                    "Selected Kaya Expert Products",
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
                  ),
                ),
                products.length != 0
                    ? ListView.builder(
                        physics: ScrollPhysics(),
                        key: UniqueKey(),
                        shrinkWrap: true,
                        itemCount: products == null ? 0 : products.length,
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: const EdgeInsets.only(
                                top: 8.0, left: 18.0, right: 18.0),
                            child: Card(
                                elevation: 3.0,
                                child: Container(
                                  height: 50.0,
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 8.0, top: 13.0),
                                    child: Text(
                                      products[index],
                                      style: TextStyle(fontSize: 18.0),
                                    ),
                                  ),
                                )),
                          );
                        })
                    : Align(
                        alignment: Alignment.topLeft,
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 25.0, top: 1.5, bottom: 5.0),
                          child: Text(
                            "No Products Selected",
                            style: TextStyle(fontSize: 18.0),
                          ),
                        )),
              ],
            ),
          );
        }
      },
    );
  }
}

class SelectedDoctorServices extends StatefulWidget {
  final int patientId;
  final List selectedDservices;
  final List<String> dcounts;

  SelectedDoctorServices(
      {this.patientId, this.selectedDservices, this.dcounts});
  @override
  _SelectedDoctorServicesState createState() => _SelectedDoctorServicesState();
}

class _SelectedDoctorServicesState extends State<SelectedDoctorServices> {
  List<String> dservices;

  getDoctorServices() async {
    dservices = widget.selectedDservices;
    return dservices;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getDoctorServices(),
      builder: (context, snapshot) {
        if (snapshot.data == null) {
          return Center(
            child: Text("No Selected Data"),
          );
        } else {
          return SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: Text(
                    "Selected Doctor Services",
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
                  ),
                ),
                dservices.length != 0
                    ? ListView.builder(
                        physics: ScrollPhysics(),
                        key: UniqueKey(),
                        shrinkWrap: true,
                        itemCount: snapshot.data.length,
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: const EdgeInsets.only(
                                top: 1.5, left: 18.0, right: 18.0),
                            child: Card(
                                elevation: 3.0,
                                child: Container(
                                  height: 50.0,
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 8.0, top: 13.0),
                                    child: Text(
                                      widget.dcounts[index].toString(),
                                      style: TextStyle(fontSize: 18.0),
                                    ),
                                  ),
                                )),
                          );
                        })
                    : Align(
                        alignment: Alignment.topLeft,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 25.0, top: 1.5),
                          child: Text(
                            "No Doctor Services Selected",
                            style: TextStyle(fontSize: 18.0),
                          ),
                        )),
              ],
            ),
          );
        }
      },
    );
  }
}
