import 'package:flutter/material.dart';

class ServiceFailsCheck extends StatefulWidget {
  @override
  _ServiceFailsCheckState createState() => _ServiceFailsCheckState();
}

class _ServiceFailsCheckState extends State<ServiceFailsCheck> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Center(
            child: Text(
                "Please complete the know Your skin to see recommendations.")),
      ),
    );
  }
}
