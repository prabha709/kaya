import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_downloader/image_downloader.dart';
import 'package:kaya_skincare/Provider/BeforeAfterProvider.dart';
import 'package:kaya_skincare/Provider/PatientImageProvider.dart';
import 'package:kaya_skincare/accessCamera/OpenCameraPosttre.dart';

import 'package:kaya_skincare/utils/BaseUrl.dart';
import 'package:kaya_skincare/utils/Loader.dart';
import 'package:kaya_skincare/utils/app_colors.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:solid_bottom_sheet/solid_bottom_sheet.dart';
import 'package:tflite/tflite.dart';
import 'package:http/http.dart' as http;

String selectedsession = "All";

class BeforeAfterScreen2 extends StatefulWidget {
  final int patientId;

  BeforeAfterScreen2({
    this.patientId,
  });
  @override
  _BeforeAfterScreen2State createState() => _BeforeAfterScreen2State();
}

class _BeforeAfterScreen2State extends State<BeforeAfterScreen2>
    with SingleTickerProviderStateMixin {
  SolidController _controller = SolidController();
  bool loading = false;
  bool selectImage = false;

  String imageData;
  PatientImageProvider patientImageProvider;
  bool showfab = true;
  Animation<double> _animation;
  AnimationController _animationController;
  String concernName="";
  loadModel() async {
    String res;
    res = await Tflite.loadModel(
        model: "assets/detect.tflite", labels: "assets/mask_labelmap.txt");

    print(res);
  }
getConcern() async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
var getData = prefs.getString("concern");
if(getData== null){
  return;
}else{

setState(() {
  concernName =getData;
});
}
}
int _currentPage = 0;
PageController pageController;
  @override
  void initState() {
    pageController= PageController(initialPage:_currentPage );
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 260),
    );

    final curvedAnimation =
        CurvedAnimation(curve: Curves.easeInOut, parent: _animationController);
    _animation = Tween<double>(begin: 0, end: 1).animate(curvedAnimation);
getConcern();
    super.initState();
  }

  Future<bool> _onButtonPressed(int cuurrenPage) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Select any one Concern?'),
            content: ConcernType(),
            actions: <Widget>[
              TextButton(
                child: Text('NO'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              TextButton(
                child: Text('YES'),
                onPressed: () async {
                  SharedPreferences prefs = await SharedPreferences.getInstance();
                  prefs.setString("concern", selectedsession);
                  setState(() {
  concernName =selectedsession;
});
                  _uploadImages(selectedsession,cuurrenPage);
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }

  _uploadImages(String concernName, int currenrPage) async {
    String url = BaseUrl.url + "patient_before_after/${widget.patientId}";
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
    });
    String token = prefs.getString("token");
    Map<String, String> data = {"Authorization": "token $token"};
    Map<String, String> concerndata = {
      "concern_type": concernName,
      "image": imageData
    };

    var request = http.MultipartRequest("POST", Uri.parse("$url"));
    request.headers.addAll(data);

    request.fields.addAll(concerndata);

    var res = await request.send();
    final respStr = res.stream.bytesToString();
    print(res.statusCode);
    if (res.statusCode == 200) {
      setState(() {
        loading = false;
        _currentPage=0;

      });
 pageController.animateToPage(
      _currentPage,
      duration: Duration(milliseconds: 350),
      curve: Curves.easeIn,
    );
      Fluttertoast.showToast(
          msg: "Your Result is Ready",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.green,
          textColor: Colors.black,
          fontSize: 16.0);
    } else if (res.statusCode == 204) {
      setState(() {
        loading = false;
      });
      Fluttertoast.showToast(
          msg: "No Face Found in Image",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    } else {
      setState(() {
        loading = false;
      });
      Fluttertoast.showToast(
          msg: "Internal Server Error",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
    print(respStr);

    // setState(() {
    //   result= respStr;
    // });
    return respStr;
  }

  @override
  Widget build(BuildContext context) {
    patientImageProvider =
        Provider.of<PatientImageProvider>(context, listen: false);
    BeforeAfterProvider beforeAfterProvider =
        Provider.of<BeforeAfterProvider>(context, listen: false);
    Size screenSize = MediaQuery.of(context).size;
    double screenHeight = screenSize.height;
    double screenWidth = screenSize.width;
    return Scaffold(
        appBar: AppBar(
          
          centerTitle: true,
          backgroundColor: Colors.white,
          elevation: 0.0,
          automaticallyImplyLeading: false,
          
          title: Column(
            children: [
              Container(
                decoration: BoxDecoration(
                  color: Color(0xffF0F0F0),
                  borderRadius: BorderRadius.all(Radius.circular(35)),
                ),
                width: screenSize.width / 2.5,
                height: screenSize.height / 12,
                child: Center(
                    child: Text(
                  "Post Treatment ",
                  style: TextStyle(color: Colors.black, fontSize: 25,fontWeight: FontWeight.w600,
                                                    
                                                    fontFamily: "Montserrat",),
                )),
              ),
            ],
          ),
          actions: [
            Padding(
              padding: const EdgeInsets.only(right: 18.0),
              child: CircleAvatar(
                backgroundColor: AppColors.containerBlack,
                radius: 30.0,
                child: Center(
                  child: IconButton(
                    icon: Icon(
                      Icons.camera,
                      size: 30.0,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      SystemChrome.setPreferredOrientations(
                          [DeviceOrientation.portraitUp]);
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => OpenCameraPost(
                            patientIndex: widget.patientId,
                          ),
                        ),
                      );

                      loadModel();
                    },
                  ),
                ),
              ),
            )
          ],
          toolbarHeight: 160.0,
        ),
        body: PageView(
          controller: pageController,
          children: [
            FutureBuilder(
                    future: beforeAfterProvider.getDetails(widget.patientId),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.done) {
                        if (snapshot.data == false) {
                          return Center(
                            child: Text("No Images Processed Yet!"),
                          );
                        } else {
                          return ListView.builder(
                              shrinkWrap: true,
                              itemCount: 1,
                              itemBuilder: (context, index) {
                                return Container(
                                  child: Column(
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Expanded(
                                                                                  child: Column(
                                              children: [
                                                Padding(
                                                  padding: const EdgeInsets.only(
                                                      top: 10.0),
                                                  child: Text(
                                                    "Pre Treatment",
                                                    style: TextStyle(
                                                        fontWeight: FontWeight.w600,
                                                        fontSize: 16,
                                                        fontFamily: "Montserrat",),
                                                  ),
                                                ),
                                                Card(
                                                  elevation: 5.0,
                                                  // color: Colors.amber,
                                                  child: Container(
                                                    
                                                    decoration: BoxDecoration(
                                                        border: Border.all(
                                                            color: Color(
                                                                0xffcce3bc))),
                                                    width: screenWidth / 3,
                                                    height: screenHeight -160,
                                                    child: FittedBox(
                                                      fit: BoxFit.fill,
                                                      child: Image.network(
                                                        BaseUrl.imageurl +
                                                            beforeAfterProvider
                                                                .beforeAfterModel
                                                                .resp
                                                                .last
                                                                .image,
                                                      ),
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                          Expanded(
                                                                                  child: Column(
                                              children: [
                                                Padding(
                                                  padding: const EdgeInsets.only(
                                                      top: 10.0),
                                                  child: Text(
                                                    "Post Treatment ($concernName)",
                                                    style: TextStyle(
                                                        fontWeight: FontWeight.w600,
                                                        fontSize: 16,
                                                        fontFamily: "Montserrat",
                                                        
                                                        ),
                                                  ),
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.all(8.0),
                                                  child: Card(
                                                    elevation: 5.0,
                                                    // color: Colors.grey[200],
                                                    child: Container(
                                                      width: screenWidth / 3,
                                                      height: screenHeight -160.0,
                                                      decoration: BoxDecoration(
                                                          border: Border.all(
                                                              color: Colors.black)),
                                                      child: FittedBox(
                                                        fit: BoxFit.fill,
                                                        child: InteractiveViewer(
                                                          child: Image.network(BaseUrl
                                                                  .imageurl +
                                                              beforeAfterProvider
                                                                  .beforeAfterModel
                                                                  .resp
                                                                  .last
                                                                  .processImage),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                );
                              });
                        }
                      } else {
                        return AppLoader();
                      }
                    }),
                    loading== false?FutureBuilder(
                future: patientImageProvider.getDetails(widget.patientId),
                 builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.done) {
                       if (snapshot.data == false) {
                      return Center(
                        child: Text("No Images Processed Yet!"),
                      );
                    } else{
                      return SingleChildScrollView(
                                              child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    "Previous Images",
                                    style: TextStyle(
                                        fontSize: 18.0,
                          fontWeight: FontWeight.w600,
                          fontFamily: "Montserrat"),
                                  ),
                                ),
                               
                                Container(
                                    height:
                                              MediaQuery.of(context).size.height /
                                                  2,
                                    child: ListView.builder(
                                      scrollDirection: Axis.horizontal,
                                      itemCount: patientImageProvider
                                          .patientImageModel.all.length,
                                      itemBuilder: (context, index) {
                                        return InkWell(
                                          onTap: () async {
                                            _controller.isOpened
                                                ? _controller.hide()
                                                : _controller.show();
                                            _onButtonPressed(_currentPage);
                                            setState(() {
                                              imageData = patientImageProvider
                                                  .patientImageModel.all
                                                  .elementAt(index)
                                                  .image;
                                            });

                                            // _uploadImages();
                                          },
                                          child: Column(
                                            children: [
                                              Text(patientImageProvider
                                                  .patientImageModel.all
                                                  .elementAt(index)
                                                  .createdDate, style: TextStyle(
                                        fontSize: 16.0,
                          fontWeight: FontWeight.w500,
                          fontFamily: "Montserrat"),),
                                              Expanded(
                                                            child: Card(
                                                  elevation: 2.0,
                                                  child: Container(
                                                    height: screenSize.height / 2.6,
                                                    width: screenSize.width / 4,
                                                    decoration: BoxDecoration(
                                                        color: Colors.green
                                                            .withOpacity(0.5),
                                                        image: DecorationImage(
                                                            fit: BoxFit.cover,
                                                            image: NetworkImage(BaseUrl
                                    .imageurl +
                                "${patientImageProvider.patientImageModel.all.elementAt(index).image}"))),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        );
                                      },
                                    ),
                                  ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    "Post Treatment Images",
                                    style: TextStyle(
                                        fontSize: 18.0,
                          fontWeight: FontWeight.w600,
                          fontFamily: "Montserrat"),
                                  ),
                                ),
                               
                             patientImageProvider
                                            .patientImageModel.postImages !=
                                        null
                                    ?  Container(
                                          height:
                                            MediaQuery.of(context).size.height /
                                                2,
                                          child: ListView.builder(
                                            scrollDirection: Axis.horizontal,
                                            itemCount: patientImageProvider
                                                .patientImageModel
                                                .postImages
                                                .length,
                                            itemBuilder: (context, index) {
                                              return   InkWell(
                                                onTap: () async {
                                                 String path =patientImageProvider.patientImageModel.postImages[index].image;

                                                    setState(() {
                                                      imageData = path;
                                                    });
                                                    _onButtonPressed(_currentPage);
                                                
                                                },
                                                child: Column(
                                                  children: [
                                                    Text(patientImageProvider
                                                        .patientImageModel
                                                        .postImages
                                                        .elementAt(index)
                                                        .createdDate ,style: TextStyle(
                                        fontSize: 16.0,
                          fontWeight: FontWeight.w500,
                          fontFamily: "Montserrat"),),
                                                    Expanded(
                                                                                child: Card(
                                                        elevation: 2.0,
                                                        child: Container(
                                                          height:
                                      screenSize.height / 2.6,
                                                          width: screenSize.width / 4,
                                                          decoration: BoxDecoration(
                                      color: Colors.green
                                          .withOpacity(0.5),
                                      image: DecorationImage(
                                          fit: BoxFit.cover,
                                          image: NetworkImage(
                                              BaseUrl.imageurl +
                                                  "${patientImageProvider.patientImageModel.postImages.elementAt(index).image}"))),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              );
                                            },
                                          ),
                                        ):Center(
                                          child: Text("No Post Images Found"),
                                        )
                                    
                              ],
                            ),
                      );
                    }
                    }else{
                   return AppLoader();
                 }
                 }
                    ):AppLoader()
          ],
        )
            
        );
  }
}

class ConcernType extends StatefulWidget {
  @override
  _ConcernTypeState createState() => _ConcernTypeState();
}

class _ConcernTypeState extends State<ConcernType> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Row(
            children: [
              Radio(
                  value: "All",
                  groupValue: selectedsession,
                  onChanged: (value) {
                    setState(() {
                      selectedsession = value;
                    });
                  }),
              Text("All"),
            ],
          ),
          Row(
            children: [
              Radio(
                  value: "Acne",
                  groupValue: selectedsession,
                  onChanged: (value) {
                    setState(() {
                      selectedsession = value;
                    });
                  }),
              Text("Acne"),
            ],
          ),
          Row(
            children: [
              Radio(
                  value: "Scar",
                  groupValue: selectedsession,
                  onChanged: (value) {
                    setState(() {
                      selectedsession = value;
                    });
                  }),
              Text("Scar"),
            ],
          ),
          Row(
            children: [
              Radio(
                  value: "Pigmented scar",
                  groupValue: selectedsession,
                  onChanged: (value) {
                    setState(() {
                      selectedsession = value;
                    });
                  }),
              Text("Pigmented scar"),
            ],
          ),
          Row(
            children: [
              Radio(
                  value: "Hyper pigmentation",
                  groupValue: selectedsession,
                  onChanged: (value) {
                    setState(() {
                      selectedsession = value;
                    });
                  }),
              Text("Hyper pigmentation"),
            ],
          ),
          // Row(
          //   children: [
          //     Radio(
          //         value: "Mole",
          //         groupValue: selectedsession,
          //         onChanged: (value) {
          //           setState(() {
          //             selectedsession = value;
          //           });
          //         }),
          //     Text("Mole"),
          //   ],
          // ),
          Row(
            children: [
              Radio(
                  value: "Melasma",
                  groupValue: selectedsession,
                  onChanged: (value) {
                    setState(() {
                      selectedsession = value;
                    });
                  }),
              Text("Melasma"),
            ],
          ),
          // Row(
          //   children: [
          //     Radio(
          //         value: "Open pore",
          //         groupValue: selectedsession,
          //         onChanged: (value) {
          //           setState(() {
          //             selectedsession = value;
          //           });
          //         }),
          //     Text("Open pore"),
          //   ],
          // ),
          // Row(
          //   children: [
          //     Radio(
          //         value: "Pih",
          //         groupValue: selectedsession,
          //         onChanged: (value) {
          //           setState(() {
          //             selectedsession = value;
          //           });
          //         }),
          //     Text("Pih"),
          //   ],
          // ),
          Row(
            children: [
              Radio(
                  value: "Dark circles",
                  groupValue: selectedsession,
                  onChanged: (value) {
                    setState(() {
                      selectedsession = value;
                    });
                  }),
              Text("Dark circles"),
            ],
          ),
        ],
      ),
    );
  }
}
