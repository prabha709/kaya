import 'dart:convert';
import 'dart:io';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:kaya_skincare/Provider/PatientDetailsProvider.dart';
import 'package:kaya_skincare/Provider/PatientListProvider.dart';
import 'package:kaya_skincare/screens/side_menu_bar.dart';
import 'package:kaya_skincare/utils/BaseUrl.dart';
import 'package:kaya_skincare/utils/app_colors.dart';
import 'package:kaya_skincare/utils/raised_button.dart';

import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EditPatient extends StatefulWidget {
  final int patientIndex;

  EditPatient({this.patientIndex});

  @override
  _EditPatientState createState() => _EditPatientState();
}

class _EditPatientState extends State<EditPatient> {
  String selectedDate;
  PatientDetailsProvider patientDetailsProvider;

  currentDate() {
    final DateTime now = DateTime.now();
    final String formatter = DateFormat('yyyy-MM-dd').format(now);
    // final String formatted = DateFormat.format(now);
    print("Date" + formatter);
    setState(() {
      selectedDate = formatter;
    });
  }

  @override
  void initState() {
    currentDate();
    patientDetailsProvider = context.read<PatientDetailsProvider>();
    patientDetailsProvider.getDetails(widget.patientIndex);
    if (patientDetailsProvider.patientDetailsModel.gender == "Male") {
      _value = 0;
    } else if (patientDetailsProvider.patientDetailsModel.gender == "Female") {
      _value = 1;
    } else if (patientDetailsProvider.patientDetailsModel.gender == "Other") {
      _value = 2;
    }
    // _value = patientDetailsProvider.patientDetailsModel.gender == "male" ? 0 : 1;
    if (_value == 0) {
      _pgender = "Male";
    } else if (_value == 1) {
      _pgender = "Female";
    } else if (_value == 2) {
      _pgender = "Other";
    }
    super.initState();
  }

  String _pname;
  String _pgender;
  String _pphone;
  String _email;
  String _age;
  bool loading = false;
  int _value;
  String code;

  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  upload(context) async {
    PatientListProvider patientListProvider =
        Provider.of<PatientListProvider>(context, listen: false);

    if (_pgender == null) {
      Fluttertoast.showToast(
          msg: "Please Select the Gender",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);
    }

    String url = BaseUrl.url + "patient/";
    if (_formkey.currentState.validate()) {
      setState(() {
        loading = true;
      });
      _formkey.currentState.save();

      try {
        Map data = {
          "patient_name": _pname,
          "age": _age,
          "gender": _pgender,
          "patient_email": _email,
          "phone": _pphone,
          'guest_code': code
        };
        SharedPreferences prefs = await SharedPreferences.getInstance();
        String token = prefs.getString("token");
        url = url + patientDetailsProvider.patientDetailsModel.id.toString();
        print(url);
        var response = await http.patch(
          "$url",
          body: json.encode(data),
          headers: {
            "Authorization": "token $token",
            "Content-Type": "application/json"
          },
        );

        var jsonData = json.decode(response.body);

        if (response.statusCode == 200) {
          setState(() {
            loading = false;
          });
          await patientListProvider.getDetails();

          Fluttertoast.showToast(
              msg: "Client Updated",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 3,
              backgroundColor: Colors.green,
              textColor: Colors.white,
              fontSize: 16.0);
          Navigator.pop(context);
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => SideMenuBar(patientIndex: jsonData["id"]),
            ),
          );
        } else {
          Fluttertoast.showToast(
              msg: jsonData.toString(),
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 3,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0);
          setState(() {
            loading = false;
          });
        }
      } on SocketException catch (error) {
        setState(() {
          loading = false;
        });
        Fluttertoast.showToast(
            msg: error.toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 3,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      } catch (e) {
        setState(() {
          loading = false;
        });
        Fluttertoast.showToast(
            msg: e.toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 3,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    patientDetailsProvider = Provider.of<PatientDetailsProvider>(context);
    if (patientDetailsProvider.patientDetailsModel == null) {
      return Center(
        child: CircularProgressIndicator(),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          toolbarHeight: 160.0,
          flexibleSpace: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/bckg_img.png"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          elevation: 0.0,
          title: Container(
            decoration: BoxDecoration(
              color: Color(0xffF0F0F0),
              borderRadius: BorderRadius.all(Radius.circular(35)),
            ),
            width: size.width / 2.5,
            height: size.height / 10,
            child: Center(
                child: Text(
              "Edit Client",
              style: TextStyle(color: Colors.black, fontSize: 25),
            )),
          ),
        ),
        body: SingleChildScrollView(
          child: Form(
            key: _formkey,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "Kindly edit the client details",
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(right: 58.0, left: 58.0, top: 38.0),
                  child: TextFormField(
                    inputFormatters: <TextInputFormatter>[
                      FilteringTextInputFormatter.allow(RegExp(r'[A-Za-z]')),
                    ],
                    initialValue:
                        patientDetailsProvider.patientDetailsModel.patientName,
                    validator: (value) {
                      if (value.isEmpty || value.length < 3) {
                        return "This Field is Required";
                      } else {
                        return null;
                      }
                    },
                    onSaved: (value) {
                      _pname = value;
                    },
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.red, width: 5.0),
                        ),
                        // hintText: ,
                        labelText: "Client Name"),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(right: 58.0, left: 58.0, top: 38.0),
                  child: TextFormField(
                    initialValue: patientDetailsProvider.patientDetailsModel.age
                        .toString(),
                    keyboardType: TextInputType.number,
                    inputFormatters: <TextInputFormatter>[
                      FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                    ],
                    validator: (value) {
                      if (value.isEmpty || value.length > 2) {
                        return "Invalid Age Input.";
                      } else {
                        return null;
                      }
                    },
                    onSaved: (value) {
                      _age = value;
                    },
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.red, width: 5.0),
                        ),
                        labelText: "Age"),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(right: 58.0, left: 58.0, top: 38.0),
                  child: TextFormField(
                    initialValue:
                        patientDetailsProvider.patientDetailsModel.patientEmail,
                    validator: (val) => !EmailValidator.validate(val)
                        ? 'Not a Valid Email.'
                        : null,
                    onSaved: (val) => _email = val,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.red, width: 5.0),
                        ),
                        labelText: "Email"),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 58.0, top: 38.0),
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: DropdownButton(
                      value: _value,
                      items: [
                        DropdownMenuItem(
                          child: Text("Male"),
                          value: 0,
                        ),
                        DropdownMenuItem(
                          child: Text("Female"),
                          value: 1,
                        ),
                        DropdownMenuItem(
                          child: Text("Other"),
                          value: 2,
                        ),
                      ],
                      onChanged: (value) {
                        setState(() {
                          _value = value;
                        });
                        if (_value == 0) {
                          setState(() {
                            _pgender = "Male";
                          });
                        } else if (_value == 1) {
                          setState(() {
                            _pgender = "Female";
                          });
                        } else if (_value == 2) {
                          setState(() {
                            _pgender = "Other";
                          });
                        }
                      },
                    ),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(right: 58.0, left: 58.0, top: 38.0),
                  child: TextFormField(
                    maxLength: 10,
                    initialValue:
                        patientDetailsProvider.patientDetailsModel.phone,
                    keyboardType: TextInputType.number,
                    inputFormatters: <TextInputFormatter>[
                      FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                    ],
                    validator: (value) {
                      RegExp regExp = new RegExp(
                        r"^(\d)\1+$",
                        caseSensitive: false,
                      );
                      if (value.length == 0) {
                        return 'Please enter mobile number';
                      } else if (regExp.hasMatch(value)) {
                        return 'Please enter valid mobile number';
                      } else if (value.length != 10) {
                        return "Enter Valid Mobile Number";
                      } else {
                        return null;
                      }
                    },
                    onSaved: (value) {
                      _pphone = value;
                    },
                    decoration: InputDecoration(
                        counterText: "",
                        border: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.red, width: 5.0),
                        ),
                        labelText: "Client Phone Number"),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(right: 58.0, left: 58.0, top: 38.0),
                  child: TextFormField(
                    maxLength: 10,
                    keyboardType: TextInputType.emailAddress,
                    initialValue:
                        patientDetailsProvider.patientDetailsModel.guestCode,
                    validator: (value) {
                      RegExp regExp = new RegExp(
                        r"^[a-zA-Z0-9]+$",
                        caseSensitive: false,
                      );
                      if (value.length == 0) {
                        return 'Please enter Guest Id';
                      } else if (!regExp.hasMatch(value)) {
                        return 'Please enter valid Guest Id';
                      } else {
                        return null;
                      }
                    },
                    onSaved: (value) {
                      code = value;
                    },
                    decoration: InputDecoration(
                      counterText: "",
                      border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.red, width: 5.0),
                      ),
                      hintText: "Guest code",
                      labelText: "Guest Code",
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 28.0),
                  child: MyRaisedButton(
                    loading: loading,
                    onPressed: upload,
                    title: "Submit",
                    buttonColor: AppColors.colorAccent,
                    textColor: Colors.white,
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }
  }
}
