import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:kaya_skincare/Provider/PatientDetailsProvider.dart';
import 'package:kaya_skincare/Provider/form2_provider.dart';

import 'package:kaya_skincare/screens/build_checklist_items_for_form2.dart';
import 'package:kaya_skincare/screens/form_2_screen.dart';
import 'package:http/http.dart' as http;
import 'package:kaya_skincare/utils/BaseUrl.dart';
import 'package:kaya_skincare/utils/Loader.dart';
import 'package:kaya_skincare/utils/app_colors.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'know_your_skin_screen.dart';

String selectedsession;

class Form2Load extends StatefulWidget {
  final int patientId;
  final int index;
  final List<String> selectedQuestions;
  final List<String> selectedAnswers;
  final List<String> selectedOptions;
  final List<String> selectedOptionsForm;

  Form2Load(
      {this.patientId,
      this.index,
      this.selectedAnswers,
      this.selectedOptions,
      this.selectedQuestions,
      this.selectedOptionsForm});

  @override
  _Form2LoadState createState() => _Form2LoadState();
}

class _Form2LoadState extends State<Form2Load> {
  // List selectedOptions;

  PatientDetailsProvider patientDetailProvider;
  int nextButtonCount = 0;
  int patientId;

  @override
  void initState() {
    //  _onButtonPressed();
    form2provider = context.read<Form2Provider>();
    patientDetailProvider = context.read<PatientDetailsProvider>();
    patientId = patientDetailProvider.patientDetailsModel.id;
    form2provider.getAnswers(patientId);

    int lengthOfOptions =
        form2provider?.form2model?.resp?.first?.questionType?.option?.length ??
            0;
    selectedOptionsForm = List<String>.generate(lengthOfOptions, (i) => "");
    super.initState();
  }

  Form2Provider form2provider;
  bool buttonState = true;
  List<String> selectedOptionsForm = [];
  bool dataSubmitted = false;
  @override
  Widget build(BuildContext context) {
    print(widget.patientId);
    form2provider = Provider.of<Form2Provider>(context, listen: false);
    return dataSubmitted
        ? Form2Screen(
            patientindexId: widget.patientId,
          )
        : Scaffold(
            floatingActionButton: Container(
              height: 60,
              width: 120,
              child: MaterialButton(
                disabledColor: Colors.grey,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0),
                ),
                textColor: Colors.white,
                onPressed: buttonState
                    ? () async {
                        List selectedOptionsFiltered = [];
                        print(widget.selectedOptionsForm);
                        widget.selectedOptionsForm.forEach((element) {
                          if (element != "") {
                            selectedOptionsFiltered.add(element);
                          }
                        });
                        if (selectedOptionsFiltered.isEmpty) {
                          return Fluttertoast.showToast(
                              msg: "Please select atleast one Option");
                        }
                        nextButtonCount++;
                        SharedPreferences prefs =
                            await SharedPreferences.getInstance();
                        // var data = prefs.getString("form3${widget.patientindexId}");
                        // print(data);
                        // var data1 = json.decode(data);
                        // final data2 = (data1 as List)
                        //     ?.map((e) => e as List)
                        //     ?.toList();

                        final data1 = selectedOptionsFiltered;
                        if (data1 == null) {
                          return Fluttertoast.showToast(
                              msg: "Please select atleast an option",
                              backgroundColor: Colors.red);
                        } else {
                          Map postData = {
                            "question": "What is you concern",
                            "answer": data1
                          };

                          String formId =
                              form2provider.form2model.resp[0].id.toString();

                          var result = await form2provider.submitFormDetails(
                              patientId: widget.patientId,
                              postData: postData,
                              formId: formId);
                          if (result == true)
                            setState(() {
                              dataSubmitted = true;
                            });
                          else {
                            Fluttertoast.showToast(
                                msg: "Something went Wrong",
                                backgroundColor: Colors.red);
                          }
                        }
                      }
                    : null,
                color: AppColors.colorAccent,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Next',
                      style: TextStyle(fontSize: 17),
                    ),
                    Icon(Icons.arrow_forward)
                  ],
                ),
              ),
            ),
            body: FutureBuilder(
                future: form2provider.getDetails(patientId),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    if (snapshot.data == false) {
                      return Center(
                        child: Text("No data Found"),
                      );
                    } else {
                      return Container(
                        width: MediaQuery.of(context).size.width,
                        child: SingleChildScrollView(
                          key: UniqueKey(),
                          child: Column(
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 8.0, top: 8.0),
                                child: Text(
                                  "Please select your concern areas?",
                                  style: TextStyle(fontSize: 20),
                                ),
                              ),
                              ListView.builder(
                                  key: UniqueKey(),
                                  shrinkWrap: true,
                                  itemCount:
                                      form2provider.form2model.resp.length,
                                  itemBuilder: (context, index) {
                                    return Padding(
                                      padding: const EdgeInsets.only(
                                          left: 16, right: 16.0),
                                      child: Align(
                                        alignment: Alignment.topLeft,
                                        child: BuildCheckListItemsForForm2(
                                            index,
                                            widget.selectedQuestions,
                                            widget.selectedAnswers,
                                            widget.selectedOptions,
                                            widget.patientId,
                                            widget.selectedOptionsForm),
                                      ),
                                    );
                                  }),
                            ],
                          ),
                        ),
                      );
                    }
                  } else {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                }),
          );
  }
}

class SkinType extends StatefulWidget {
  final int patientId;
  final BuildContext skinContext;
  final Function editSkin;

  SkinType({this.patientId, this.skinContext, this.editSkin});
  @override
  _SkinTypeState createState() => _SkinTypeState();
}

class _SkinTypeState extends State<SkinType> {
  bool editSkinType = false;
  bool updatedSkinType = false;
  Form2Provider form2provider;

  updateSkinType() async {
    form2provider = context.read<Form2Provider>();
    form2provider.getDetails(widget.patientId);
    int formId = form2provider.form2model.resp[0].form1id;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");
    String url = BaseUrl.url + "modify_skin_type/$formId";
    var response = await http.patch("$url",
        headers: {"Authorization": "token $token"},
        body: {"skin_type": selectedsession});
    if (response.statusCode == 200) {
      setState(() {
        updatedSkinType = true;
      });
      print(response.statusCode);
    } else {
      form2provider.setLoading(true);
      Fluttertoast.showToast(msg: "SkinType Not Updated");
      print(response.body);
    }
  }

  @override
  Widget build(BuildContext context) {
    form2provider = Provider.of<Form2Provider>(context, listen: false);
    return updatedSkinType == false
        ? Scaffold(
            body: editSkinType != false
                ? SingleChildScrollView(
                    child: Column(
                      children: [
                        Padding(
                            padding: const EdgeInsets.only(top: 18.0),
                            child: Text("Detected Skin Type",
                                style: TextStyle(
                                    fontSize: 15.0,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: "Montserrat"
                                    ))),
                        Row(
                          children: [
                            Radio(
                                value: "Dry",
                                groupValue: selectedsession,
                                onChanged: (value) {
                                  setState(() {
                                    selectedsession = value;
                                  });
                                }),
                            Text("Dry"),
                          ],
                        ),
                        Row(
                          children: [
                            Radio(
                                value: "Normal",
                                groupValue: selectedsession,
                                onChanged: (value) {
                                  setState(() {
                                    selectedsession = value;
                                  });
                                }),
                            Text("Normal"),
                          ],
                        ),
                        Row(
                          children: [
                            Radio(
                                value: "Oily",
                                groupValue: selectedsession,
                                onChanged: (value) {
                                  setState(() {
                                    selectedsession = value;
                                  });
                                }),
                            Text("Oily"),
                          ],
                        ),
                        Row(
                          children: [
                            Radio(
                                value: "Combination",
                                groupValue: selectedsession,
                                onChanged: (value) {
                                  setState(() {
                                    selectedsession = value;
                                  });
                                }),
                            Text("Combination"),
                          ],
                        ),
                        Row(
                          children: [
                            Radio(
                                value: "Sensitive",
                                groupValue: selectedsession,
                                onChanged: (value) {
                                  setState(() {
                                    selectedsession = value;
                                  });
                                }),
                            Text("Sensitive"),
                          ],
                        ),
                        TextButton(
                            onPressed: () {
                              updateSkinType();
                            },
                            child: Text("Update",
                                style: TextStyle(
                                  color: Colors.white,
                                )),
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(
                                  Colors.black),
                            ))
                      ],
                    ),
                  )
                : FutureBuilder(
                    future: form2provider.getDetails(widget.patientId),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.done) {
                        if (snapshot.data == false) {
                          return Text("No data Found");
                        } else {
                          return Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text("Your Skin type is",
                                  style: TextStyle(fontSize: 20.0,fontFamily: "Montserrat",fontWeight: FontWeight.w500)),
                              form2provider.form2model.resp[0].questionType.type
                                          .length ==
                                      1
                                  ? Text(
                                      form2provider.form2model.resp[0]
                                          .questionType.type[0].data,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w600,
                                          fontSize: 25.0,fontFamily: "Montserrat",))
                                  : Column(
                                      children: [
                                        Text(
                                            "${form2provider.form2model.resp[0].questionType.type[0].data} /",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 25.0)),
                                        Text(
                                            form2provider.form2model.resp[0]
                                                .questionType.type[1].data,
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 25.0))
                                      ],
                                    ),
                              // ListView.builder(
                              //     shrinkWrap: true,
                              //     itemCount: form2provider.form2model.resp[0]
                              //         .questionType.type.length,
                              //     itemBuilder: (context, index) {
                              //       return Center(
                              //           child: Text(
                              //               form2provider.form2model.resp[0]
                              //                   .questionType.type[index].data,
                              //               style: TextStyle(
                              //                   fontWeight: FontWeight.bold,
                              //                   fontSize: 25.0))
                              //                   );

                              //     }),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  TextButton(
                                    child: Text(
                                      "Edit",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    onPressed: () {
                                      // controller.updateTheButten();
                                      setState(() {
                                        editSkinType = true;
                                      });
                                    },
                                    style: ButtonStyle(
                                        backgroundColor:
                                            MaterialStateProperty.all<Color>(
                                                Colors.black)),
                                  ),
                                  TextButton(
                                    child: Text(
                                      "Continue",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    onPressed: () {
                                      widget.editSkin();
                                      Navigator.pop(widget.skinContext);
                                      // controller.updateTheButten();
                                    },
                                    style: ButtonStyle(
                                        backgroundColor:
                                            MaterialStateProperty.all<Color>(
                                                Colors.black)),
                                  ),
                                ],
                              )
                            ],
                          );
                        }
                      } else {
                        return AppLoader();
                      }
                    }),
          )
        : Center(
            child: Column(
              children: [
                Text("Skin Type Updated Sucessfully",
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 25.0)),
                TextButton(
                  child: Text(
                    "Continue",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    widget.editSkin();
                    Navigator.pop(widget.skinContext);
                    // controller.updateTheButten();
                  },
                  style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.black)),
                ),
              ],
            ),
          );
  }
}
