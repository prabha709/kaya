import 'package:flutter/material.dart';
import 'package:kaya_skincare/Provider/Form1Provider.dart';
import 'package:kaya_skincare/Provider/PatientDetailsProvider.dart';
import 'package:kaya_skincare/Provider/form2_provider.dart';
import 'package:kaya_skincare/screens/Form2Load.dart';
import 'package:kaya_skincare/utils/Loader.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Form2Screen extends StatefulWidget {
  final int patientindexId;

  Form2Screen({
    this.patientindexId,
  });

  @override
  _Form2ScreenState createState() => _Form2ScreenState();
}

class _Form2ScreenState extends State<Form2Screen> {
  Form2Provider form2provider;
  bool productsrec = false;

  bool datasub = false;
  int nextButtonCount = 0;
  List<String> selectedQuestionsForm2;
  List<String> selectedOptions = List<String>();
  List<String> selectedOptionsForm = List<String>();
  String imagePath1;

  String imagePath2;

  String imagePath3;

  List<String> selectedAnswersForm2;

  int lengthOfQuestionsForm2;
  bool buttonState = true;
  PatientDetailsProvider patientDetailProvider;
  Form1Provider form1provider;

  @override
  void initState() {
    form2provider = context.read<Form2Provider>();
    patientDetailProvider = context.read<PatientDetailsProvider>();
    int patientId = patientDetailProvider.patientDetailsModel.id;

    form2provider.getDetails(patientId);
    form2provider.getAnswers(patientId);
    int lengthOfOptions =
        form2provider?.form2model?.resp?.first?.questionType?.option?.length ??
            0;
    selectedOptionsForm = List<String>.generate(lengthOfOptions, (i) => "");
    // getButtonState();
    super.initState();
  }

  getButtonState() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      buttonState =
          sharedPreferences.getBool("buttonState${widget.patientindexId}") ??
              true;
    });
  }

  @override
  Widget build(BuildContext context) {
    print(widget.patientindexId);
    form2provider = Provider.of<Form2Provider>(context, listen: false);

    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      body: FutureBuilder(
          future: form2provider.getAnswers(widget.patientindexId),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              if (snapshot.data == false) {
                return Form2Load(
                  patientId: widget.patientindexId,
                  selectedQuestions: selectedQuestionsForm2,
                  selectedAnswers: selectedAnswersForm2,
                  selectedOptions: selectedOptions,
                  selectedOptionsForm: selectedOptionsForm,
                );
              } else {
                String sentence =
                    "${form2provider.form2Response.resp.skinType[0].data}";

                print(sentence);

                if (sentence == "Oily"|| sentence=="oily") {
                  imagePath1 = "assets/oily skin.gif";
                } else if (sentence == "dry") {
                  imagePath1 = "assets/Kaya Concern Dehydration.png";
                } else if (sentence == "Normal") {
                  imagePath1 = "assets/normal skin.gif";
                } else if (sentence == "Combination") {
                  imagePath1 = "assets/combination skin.gif";
                } else if (sentence == "Sensitive") {
                  imagePath1 = "assets/sensitive skin.gif";
                }

                return Container(
                  
                  padding: EdgeInsets.symmetric(horizontal: 25, vertical: 25),
                  child: Column(
                    // mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                                                child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Skin Type : ",
                                  style: TextStyle(
                                      fontSize: 30.0,
                                      fontWeight: FontWeight.w500),
                                ),
                                SizedBox(height: 15),
                                form2provider.form2Response.resp.skinType
                                            .length ==
                                        1
                                    ? Text(
                                        "${form2provider.form2Response.resp.skinType[0].data}",
                                        style: TextStyle(
                                            fontSize: 55.0,
                                            fontWeight: FontWeight.w900),
                                      )
                                    : Column(
                                        children: [
                                          Text(
                                            "${form2provider.form2Response.resp.skinType[0].data}",
                                            style: TextStyle(
                                                fontSize: 55.0,
                                                fontWeight: FontWeight.w900),
                                          ),
                                          Text("OR",
                                              style: TextStyle(
                                                fontSize: 25.0,
                                              )),
                                          Text(
                                            "${form2provider.form2Response.resp.skinType[1].data}",
                                            style: TextStyle(
                                                fontSize: 55.0,
                                                fontWeight: FontWeight.w900),
                                          )
                                        ],
                                      )
                              ],
                            ),
                           
                            Expanded(
                                                            child: SizedBox(
                                
                                  child: Image.asset(
                                    imagePath1,
                                  )),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              }
            } else {
              return AppLoader();
            }
          }),

      // SelectedAnswersForm1(form1SavedAnswerResponse: form2provider.savedAnswersFormResponse, formNumber: 2,)
    );
  }
}
