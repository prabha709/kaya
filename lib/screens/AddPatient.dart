import 'dart:convert';
import 'dart:io';

import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:kaya_skincare/Provider/PatientListProvider.dart';
import 'package:kaya_skincare/screens/side_menu_bar.dart';
import 'package:kaya_skincare/utils/BaseUrl.dart';
import 'package:kaya_skincare/utils/app_colors.dart';
import 'package:kaya_skincare/utils/raised_button.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AddPatient extends StatefulWidget {
  @override
  _AddPatientState createState() => _AddPatientState();
}

class _AddPatientState extends State<AddPatient> {
  String selectedDate;

  currentDate() {
    final DateTime now = DateTime.now();
    final String formatter = DateFormat('yyyy-MM-dd').format(now);
    // final String formatted = DateFormat.format(now);
    print("Date" + formatter);
    setState(() {
      selectedDate = formatter;
    });
  }

  @override
  void initState() {
    currentDate();
    super.initState();
  }

  File imageFile;
  final picker = ImagePicker();

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        imageFile = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  String _pname;
  String _pgender;
  String _pphone;
  String _email;
  String _age;
  String code;
  bool loading = false;
  int _value;

  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  upload(context) async {
    PatientListProvider patientListProvider =
        Provider.of<PatientListProvider>(context, listen: false);

    if (_pgender == null) {
      Fluttertoast.showToast(
          msg: "Please Select the Gender",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);
    }

    String url = BaseUrl.url + "patient/";
    if (_formkey.currentState.validate()) {
      setState(() {
        loading = true;
      });
      _formkey.currentState.save();

      try {
        Map<String, String> fieldData = {
          "patient_name": _pname,
          "age": _age,
          "gender": _pgender,
          "patient_email": _email,
          "phone": _pphone,
          "guest_code": code
        };
        SharedPreferences prefs = await SharedPreferences.getInstance();
        String token = prefs.getString("token");
        Map<String, String> headerData = {
          "Authorization": "token $token",
          "Content-Type": "application/json"
        };
        var request = http.MultipartRequest("POST", Uri.parse("$url"));
        request.headers.addAll(headerData);
        request.fields.addAll(fieldData);
        if (imageFile != null) {
          request.files.add(await http.MultipartFile.fromPath(
            "profile_pic",
            imageFile.path,
          ));
        }
        var response = await request.send();
        final respStr = await response.stream.bytesToString();
        print(response.statusCode);
        // var response = await http.post(
        //   "$url",
        //   body: json.encode(data),
        //   headers: headerData,
        // );

        var jsonData = json.decode(respStr);
        if (response.statusCode == 200) {
          setState(() {
            loading = false;
          });
          await patientListProvider.getDetails();

          Fluttertoast.showToast(
              msg: "Patient Added To Your List",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 3,
              backgroundColor: Colors.green,
              textColor: Colors.white,
              fontSize: 16.0);
          Navigator.pop(context);
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => SideMenuBar(
                patientIndex: jsonData["id"],
                patientAge: jsonData["age"].toString(),
                patientName: jsonData["patient_name"],
              ),
            ),
          );
        } else {
          Fluttertoast.showToast(
              msg: jsonData.toString(),
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 3,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0);
          setState(() {
            loading = false;
          });
        }
      } on SocketException catch (error) {
        setState(() {
          loading = false;
        });
        Fluttertoast.showToast(
            msg: error.toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 3,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      } catch (e) {
        setState(() {
          loading = false;
        });
        print(e.toString());
        Fluttertoast.showToast(
            msg: e.toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 3,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 160.0,
        flexibleSpace: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/bckg_img.png"),
              fit: BoxFit.cover,
            ),
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Container(
          decoration: BoxDecoration(
            color: Color(0xffF0F0F0),
            borderRadius: BorderRadius.all(Radius.circular(35)),
          ),
          width: size.width / 2.5,
          height: size.height / 10,
          child: Center(
              child: Text(
            "Add Client",
            style: TextStyle(color: Colors.black, fontSize: 25),
          )),
        ),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formkey,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Kindly fill the Patient Details",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 18.0),
                child: InkWell(
                  onTap: () {
                    getImage();
                  },
                  child: imageFile != null
                      ? CircleAvatar(
                          radius: 80.0,
                          backgroundColor: Colors.grey,
                          backgroundImage: FileImage(imageFile),
                        )
                      : CircleAvatar(
                          radius: 80.0,
                          backgroundColor: Colors.grey,
                          child: Icon(
                            Icons.camera_enhance_rounded,
                            size: 68,
                            color: Colors.black,
                          ),
                        ),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.only(right: 58.0, left: 58.0, top: 38.0),
                child: TextFormField(
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.allow(RegExp(r'[A-Z a-z]')),
                  ],
                  validator: (value) {
                    return (value.isEmpty || value.length < 3)
                        ? "This Field is Required"
                        : null;
                  },
                  onSaved: (value) {
                    _pname = value;
                  },
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.red, width: 5.0),
                      ),
                      hintText: "Client Name",
                      labelText: "Client Name"),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.only(right: 58.0, left: 58.0, top: 38.0),
                child: TextFormField(
                  keyboardType: TextInputType.number,
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                  ],
                  validator: (value) {
                    if (value.isEmpty || value.length > 2) {
                      return "Invalid Age Input.";
                    } else {
                      return null;
                    }
                  },
                  onSaved: (value) {
                    _age = value;
                  },
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.red, width: 5.0),
                      ),
                      hintText: "Age",
                      labelText: "Age"),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.only(right: 58.0, left: 58.0, top: 38.0),
                child: TextFormField(
                  validator: (val) => !EmailValidator.validate(val)
                      ? 'Not a Valid Email.'
                      : null,
                  onSaved: (val) => _email = val,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.red, width: 5.0),
                      ),
                      hintText: "Email",
                      labelText: "Email"),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 58.0, top: 38.0),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: DropdownButton(
                    hint: Text("Select Gender"),
                    value: _value,
                    items: [
                      // DropdownMenuItem(
                      //   child: Text("Gender"),
                      //   value: 0,
                      // ),
                      DropdownMenuItem(
                        child: Text("Male"),
                        value: 0,
                      ),
                      DropdownMenuItem(
                        child: Text("Female"),
                        value: 1,
                      ),
                      DropdownMenuItem(
                        child: Text("Other"),
                        value: 2,
                      ),
                    ],
                    onChanged: (value) {
                      setState(() {
                        _value = value;
                      });
                      if (_value == 0) {
                        setState(() {
                          _pgender = "Male";
                        });
                      } else if (_value == 1) {
                        _pgender = "Female";
                      } else if (_value == 2) {
                        _pgender = "Other";
                      }
                    },
                  ),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.only(right: 58.0, left: 58.0, top: 38.0),
                child: TextFormField(
                  maxLength: 10,
                  keyboardType: TextInputType.number,
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                  ],
                  validator: (value) {
                    RegExp regExp = new RegExp(
                      r"^(\d)\1+$",
                      caseSensitive: false,
                    );
                    if (value.length == 0) {
                      return 'Please enter mobile number';
                    } else if (regExp.hasMatch(value)) {
                      return 'Please enter valid mobile number';
                    } else if (value.length != 10) {
                      return "Enter Valid Mobile Number";
                    } else {
                      return null;
                    }
                  },
                  onSaved: (value) {
                    _pphone = value;
                  },
                  decoration: InputDecoration(
                    counterText: "",
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.red, width: 5.0),
                    ),
                    hintText: "Patient Phone Number",
                    labelText: "Patient Phone Number",
                  ),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.only(right: 58.0, left: 58.0, top: 38.0),
                child: TextFormField(
                  maxLength: 10,
                  keyboardType: TextInputType.emailAddress,
                  validator: (value) {
                    RegExp regExp = new RegExp(
                      r"^[a-zA-Z0-9]+$",
                      caseSensitive: false,
                    );
                    if (value.length == 0) {
                      return 'Please enter Guest Id';
                    } else if (!regExp.hasMatch(value)) {
                      return 'Please enter valid Guest Id';
                    } else {
                      return null;
                    }
                  },
                  onSaved: (value) {
                    code = value;
                  },
                  decoration: InputDecoration(
                    counterText: "",
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.red, width: 5.0),
                    ),
                    hintText: "Guest code",
                    labelText: "Guest Code",
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 28.0),
                child: MyRaisedButton(
                  loading: loading,
                  onPressed: upload,
                  title: "Submit",
                  buttonColor: AppColors.colorAccent,
                  textColor: Colors.white,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
