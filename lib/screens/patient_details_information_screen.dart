import 'package:flutter/material.dart';
import 'package:kaya_skincare/Provider/PatientDetailsProvider.dart';
import 'package:kaya_skincare/screens/EditPatient.dart';
import 'package:kaya_skincare/utils/BaseUrl.dart';
import 'package:kaya_skincare/utils/Loader.dart';
import 'package:kaya_skincare/utils/app_colors.dart';
import 'package:provider/provider.dart';

class PatientDetailsInfoScreen extends StatefulWidget {
  final int patientIndex;

  PatientDetailsInfoScreen({this.patientIndex});

  @override
  _PatientDetailsInfoScreenState createState() =>
      _PatientDetailsInfoScreenState();
}

class _PatientDetailsInfoScreenState extends State<PatientDetailsInfoScreen> {
  PatientDetailsProvider patientDetailsProvider;

  @override
  Widget build(BuildContext context) {
    patientDetailsProvider =
        Provider.of<PatientDetailsProvider>(context, listen: false);
    Size size = MediaQuery.of(context).size;
    return FutureBuilder(
      future: patientDetailsProvider.getDetails(widget.patientIndex),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.data == false) {
            return Center(
              child: Text("NO Data Found"),
            );
          } else {
            return Row(
              children: [
                Expanded(
                  child: Container(
                    margin: EdgeInsets.all(10.0),
                    child: Column(
                      children: [
                    
                        Expanded(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                         
                            children: [
                              Text(
                                "ID",
                                style: TextStyle(fontSize: 18,fontFamily: "Montserrat",fontWeight:FontWeight.w500),
                              ),
                              Spacer(),
                            
                              Text(
                                patientDetailsProvider.patientDetailsModel.id
                                    .toString(),
                                style: TextStyle(fontSize: 18,fontFamily: "Montserrat",fontWeight:FontWeight.w500),
                               
                              ),
                            ],
                          ),
                        ), 
                        Expanded(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                           
                            children: [
                              Text(
                                "Age",
                                style: TextStyle(fontSize: 18,fontFamily: "Montserrat",fontWeight:FontWeight.w500),
                              ),
                          
                              Text(
                                patientDetailsProvider.patientDetailsModel.age
                                    .toString(),
                                style: TextStyle(fontSize: 18,fontFamily: "Montserrat",fontWeight:FontWeight.w500),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            // crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Phone",
                                style: TextStyle(fontSize: 18,fontFamily: "Montserrat",fontWeight:FontWeight.w500),
                              ),
                            
                              Text(
                                patientDetailsProvider.patientDetailsModel.phone
                                    .toString(),
                                style: TextStyle(fontSize: 18,fontFamily: "Montserrat",fontWeight:FontWeight.w500),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                           
                            children: [
                              Text(
                                "Email",
                                style: TextStyle(fontSize: 18,fontFamily: "Montserrat",fontWeight:FontWeight.w500),
                              ),
                             
                            Flexible(
                                                              child: Text(
                                  patientDetailsProvider
                                      .patientDetailsModel.patientEmail
                                      .toString(),
                                  textAlign: TextAlign.right,
                                  style: TextStyle(fontSize: 18,fontFamily: "Montserrat",fontWeight:FontWeight.w500),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            // crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Gender",
                                style: TextStyle(fontSize: 18,fontFamily: "Montserrat",fontWeight:FontWeight.w500),
                              ),
                           
                              Text(
                                patientDetailsProvider
                                    .patientDetailsModel.gender
                                    .toString(),
                                style: TextStyle(fontSize: 18,fontFamily: "Montserrat",fontWeight:FontWeight.w500),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            // crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Guest Code",
                                style: TextStyle(fontSize: 18,fontFamily: "Montserrat",fontWeight:FontWeight.w500),
                              ),
                              Spacer(),
                              // SizedBox(width: size.width/4),
                              Text(
                                patientDetailsProvider
                                    .patientDetailsModel.guestCode
                                    .toString(),
                                style: TextStyle(fontSize: 18,fontFamily: "Montserrat",fontWeight:FontWeight.w500),
                                // Text(patientDetailsProvider.patientDetailsModel.dob.toString(),
                                //                         style: TextStyle(fontSize: 18),
                              ),
                            ],
                          ),
                        ),
                        
                        


                        
                      ],
                    ),
                  )),
                  Expanded(
                    child: Container(
                 margin: EdgeInsets.only(top:55,bottom: 25.0),
                  child: Column(
                    
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                     Stack(
                       children: [
                         patientDetailsProvider.patientDetailsModel.profilePic ==
                              null
                          ? CircleAvatar(
                              radius: 85,
                              backgroundColor: Color(0xffC4C4C4),
                              child: Text(
                                patientDetailsProvider
                                    .patientDetailsModel.patientName[0],
                                style: TextStyle(
                                    color: Colors.black, fontSize: 65,fontFamily: "Montserrat",fontWeight: FontWeight.w500),
                              ))
                          : CircleAvatar(
                              radius: 85,
                              backgroundColor: Color(0xffC4C4C4),
                              backgroundImage: NetworkImage(
                                BaseUrl.imageurl +
                                    patientDetailsProvider
                                        .patientDetailsModel.profilePic,
                              ),
                            ),
                                  Transform.translate(
                                    offset: Offset(10,2),
                                                                      child: InkWell(
                                                                        onTap: (){
                                                                                Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => EditPatient(
                                      patientIndex: widget.patientIndex,
                                    )));
                                                                        },

                                      child: CircleAvatar(
                                             backgroundColor: Colors.black,
                                      child: Icon(Icons.edit_outlined,color: Colors.white,),
                                    ),
                                                                      ),
                                  )
                       ],
                     ),
                     SizedBox(height: 10.0),
                               Text(
                                  patientDetailsProvider
                                      .patientDetailsModel.patientName,
                                      
                                  style: TextStyle(fontSize: 25,fontFamily: "Montserrat",fontWeight:FontWeight.w600 ),
                           
                                ),
                                
                               Text(
                                  "${patientDetailsProvider.patientDetailsModel.age} Years Old",
                                     
                                  style: TextStyle(fontSize: 18,fontFamily: "Montserrat",fontWeight:FontWeight.w400 ),
                           
                                ),
                                Padding(
                        padding: const EdgeInsets.only(top: 25.0),
                        child: Container(
                          height: 50,
                          width: 250,
                          child: MaterialButton(
                            color: Colors.black,
                            textColor: AppColors.colorWhite,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(35)),
                            onPressed: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => EditPatient(
                                        patientIndex: widget.patientIndex,
                                      )));
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                Icon(Icons.edit_outlined),
                                Text("Edit Client Info"),
                              ],
                            ),
                          ),
                        ),
                      ),
                    
                
                       
                  
                    ],
                  ),
                )
                  )
              ],
            );
          }
        } else {
          return AppLoader();
        }
      },
    );
  }
}
