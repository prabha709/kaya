import 'package:flutter/material.dart';
import 'package:kaya_skincare/Provider/PatientImageProvider.dart';
import 'package:kaya_skincare/utils/BaseUrl.dart';
import 'package:kaya_skincare/utils/Loader.dart';
import 'package:provider/provider.dart';

class PatientImagesScreen extends StatefulWidget {
  final int patientindexId;
  PatientImagesScreen({this.patientindexId});
  @override
  _PatientImagesScreenState createState() => _PatientImagesScreenState();
}

class _PatientImagesScreenState extends State<PatientImagesScreen> {
  @override
  Widget build(BuildContext context) {
    PatientImageProvider patientImageProvider =
        Provider.of<PatientImageProvider>(context, listen: false);
    Size screenSize = MediaQuery.of(context).size;
    final double itemHeight = (screenSize.height - kToolbarHeight - 10);
    final double itemWidth = screenSize.width / 2;
    return Container(
        height: screenSize.height,
        width: screenSize.width,
        child: FutureBuilder(
          future: patientImageProvider.getDetails(widget.patientindexId),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              if (snapshot.data == false) {
                return Center(
                  child: Text("No data Found"),
                );
              } else {
                return GridView.builder(
                    itemCount:
                        patientImageProvider.patientImageModel.all.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3,
                        childAspectRatio: itemWidth / itemHeight),
                    itemBuilder: (context, index) {
                      return Card(
                        elevation: 2.0,
                        child: Container(
                          height: screenSize.height / 2.6,
                          width: screenSize.width / 4,
                          decoration: BoxDecoration(
                              color: Colors.green.withOpacity(0.5),
                              image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: NetworkImage(BaseUrl.imageurl +
                                      "${patientImageProvider.patientImageModel.all.elementAt(index).image}"))),
                        ),
                      );
                    });
              }
            } else {
              return AppLoader();
            }
          },
        ));
  }
}
