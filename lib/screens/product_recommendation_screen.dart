import 'package:flutter/material.dart';
import 'package:kaya_skincare/screens/DoctorService.dart';
import 'package:kaya_skincare/screens/Products.dart';
import 'package:kaya_skincare/screens/Services.dart';

class ProductRecommendationScreen extends StatefulWidget {
  final int patientId;

  ProductRecommendationScreen({this.patientId});

  @override
  _ProductRecommendationScreenState createState() =>
      _ProductRecommendationScreenState();
}

class _ProductRecommendationScreenState
    extends State<ProductRecommendationScreen>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  int _selectedIndex = 0;
  Color disabledTextColor = Color(0xffc5c5c5);
  Color disabledBoxColor = Color(0xffc5c5c5);
  Color enabledTextColor = Colors.black;

  Color enabledBoxColor = Color(0xffCD8B6D);
  Color defaultTextColor = Color(0xffc5c5c5);
  Color defaultBoxColor = Color(0xffc5c5c5);
  bool loading = false;
  @override
  void initState() {
    _tabController = TabController(length: 3, vsync: this);

    _tabController.addListener(() {
      setState(() {
        _selectedIndex = _tabController.index;
      });
      // print("Selected Index: " + _tabController.index.toString());
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        flexibleSpace: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/bckg_img.png"),
              fit: BoxFit.cover,
            ),
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0.0,
        automaticallyImplyLeading: false,
        title: Container(
          decoration: BoxDecoration(
            color: Color(0xffF0F0F0),
            borderRadius: BorderRadius.all(Radius.circular(35)),
          ),
          width: screenSize.width / 2.5,
          height: screenSize.height / 12,
          child: Center(
              child: Text(
            "Product Recommendation",
            style: TextStyle(color: Colors.black, fontSize: 25),
          )),
        ),
        toolbarHeight: 160.0,
        bottom: TabBar(
          controller: _tabController,
          unselectedLabelColor: Colors.black,
          labelColor: Colors.white,
          indicator: BoxDecoration(
            borderRadius: BorderRadius.circular(50),
            color: Colors.black,
          ),
          tabs: [
            Text(
              'Doctor Service',
              style: TextStyle(fontSize: 20),
            ),
            Container(
              height: screenSize.height / 17,
              child: Center(
                child: Text(
                  'Services',
                  style: TextStyle(fontSize: 20),
                ),
              ),
            ),
            Text(
              'Products',
              style: TextStyle(fontSize: 20),
            ),
          ],
        ),
      ),
      backgroundColor: Colors.white,
      body: TabBarView(
          physics: NeverScrollableScrollPhysics(),
          controller: _tabController,
          children: [
            DoctorService(
              patientId: widget.patientId,
            ),
            Services(
              patientId: widget.patientId,
            ),
            Products(patientId: widget.patientId),
          ]),
      // floatingActionButton: Container(
      //   height: 50,
      //   width: 200,
      //   child: ElevatedButton(
      //       style: ElevatedButton.styleFrom(
      //         primary: Colors.black,
      //       ),
      //       onPressed: () async {
      //         SharedPreferences prefs = await SharedPreferences.getInstance();
      //         var services = prefs.getString("check${widget.patientId}");
      //         var products = prefs.getString("product${widget.patientId}");
      //         var dservices = prefs.getString("dService${widget.patientId}");
      //         if (services == null) {
      //           return Fluttertoast.showToast(
      //               msg: "Kindly select the services",
      //               toastLength: Toast.LENGTH_SHORT,
      //               gravity: ToastGravity.BOTTOM,
      //               timeInSecForIosWeb: 1,
      //               backgroundColor: Colors.red,
      //               textColor: Colors.white,
      //               fontSize: 16.0);
      //         } else if (products == null) {
      //           return Fluttertoast.showToast(
      //               msg: "Kindly select the Products",
      //               toastLength: Toast.LENGTH_SHORT,
      //               gravity: ToastGravity.BOTTOM,
      //               timeInSecForIosWeb: 1,
      //               backgroundColor: Colors.red,
      //               textColor: Colors.white,
      //               fontSize: 16.0);
      //         } else if (dservices == null) {
      //           return Fluttertoast.showToast(
      //               msg: "Kindly select the Doctor Services",
      //               toastLength: Toast.LENGTH_SHORT,
      //               gravity: ToastGravity.BOTTOM,
      //               timeInSecForIosWeb: 1,
      //               backgroundColor: Colors.red,
      //               textColor: Colors.white,
      //               fontSize: 16.0);
      //         } else {
      //           setState(() {
      //             loading = true;
      //           });
      //           SharedPreferences prefs =
      //               await SharedPreferences.getInstance();
      //           var services = prefs.getString("check${widget.patientId}");
      //           var products = prefs.getString("product${widget.patientId}");
      //           var dservices =
      //               prefs.getString("dService${widget.patientId}");
      //           Map data = {
      //             "service": {
      //               "service1": services,
      //               "service2": ["none"]
      //             },
      //             "product": {
      //               "product1": products,
      //               "product2": ["none"]
      //             },
      //             "doctor_service": dservices
      //           };
      //           print(data);
      //           String token = prefs.getString("token");
      //           String url = BaseUrl.url + "servicelist/${widget.patientId}";
      //           var response = await http.post("$url",
      //               headers: {
      //                 "Authorization": "token $token",
      //                 "Content-Type": "application/json"
      //               },
      //               body: json.encode(data));
      //           if (response.statusCode == 200) {
      //             setState(() {
      //               loading = false;
      //             });
      //             return Fluttertoast.showToast(
      //                 msg: "Data Submitted Sucessfully",
      //                 toastLength: Toast.LENGTH_SHORT,
      //                 gravity: ToastGravity.BOTTOM,
      //                 timeInSecForIosWeb: 1,
      //                 backgroundColor: Colors.green,
      //                 textColor: Colors.white,
      //                 fontSize: 16.0);
      //           } else {
      //             setState(() {
      //               loading = false;
      //             });
      //             print(response.body);
      //             Fluttertoast.showToast(
      //                 msg: "Internal Server Error",
      //                 toastLength: Toast.LENGTH_SHORT,
      //                 gravity: ToastGravity.BOTTOM,
      //                 timeInSecForIosWeb: 1,
      //                 backgroundColor: Colors.red,
      //                 textColor: Colors.white,
      //                 fontSize: 16.0);
      //           }
      //         }
      //       },
      //       child: loading == false
      //           ? Text(
      //               "Submit",
      //               style: TextStyle(color: Colors.white),
      //             )
      //           : Center(
      //               child: CircularProgressIndicator(),
      //             )),
      // )
    );
  }
}
