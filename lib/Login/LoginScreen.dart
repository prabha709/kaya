import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kaya_skincare/screens/PatientLIst.dart';
import 'package:kaya_skincare/utils/BaseUrl.dart';
import 'package:kaya_skincare/utils/app_colors.dart';
import 'package:kaya_skincare/utils/raised_button.dart';
import 'package:http/http.dart' as http;
import 'package:email_validator/email_validator.dart';
import 'dart:convert';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:camera/camera.dart';

class LoginScreen extends StatefulWidget {
  final List<CameraDescription> cameras;

  LoginScreen({
    this.cameras,
  });
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();
  String email;
  String password;
  bool loading = false;

  login(context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String url = BaseUrl.url + "login";
    if (_formkey.currentState.validate()) {
      setState(() {
        loading = true;
      });

      _formkey.currentState.save();
      print(url);

      Map data = {"username": email, "password": password};
      var response = await http.post("$url", body: data);
      var jsonObject = json.decode(response.body);
      if (response.statusCode == 200) {
        setState(() {
          loading = false;
        });
        debugPrint(jsonObject['auth_token']);
        await prefs.setString("token", jsonObject["auth_token"]);

        Navigator.pop(context);
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => PatientList(
                    cameras: widget.cameras,
                  )),
        );
      } else {
        setState(() {
          loading = false;
        });
        Fluttertoast.showToast(
            msg: response.body.toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 3,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      }
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        height: screenSize.height,
        width: screenSize.width,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(BaseUrl.image), fit: BoxFit.fill)),
        child: Container(
           margin: EdgeInsets.only(top:18.0, right: 18.0,left: 18.0),
          child: Column(
          
            children: [
            
              Expanded(
                              child: Container(
                                 color: Colors.white.withOpacity(0.2),
                  child: Row(
                    children: [
                        Image.asset(
                        "assets/Kaya_Clinic_logo 2.png",
                        
                      ),
                      Spacer(),
                      Image.asset("assets/kaya_login_image_1.png"),
                      SizedBox(width: 25),
                      Text("18+ yrs of \nDermatological Expertise",style:TextStyle(fontFamily: "Montserrat",fontSize: 14.0,fontWeight: FontWeight.w500),),
                      Spacer(),
                      Image.asset("assets/login_image_2.png"),
                      SizedBox(width: 25),
                      Text("Largest dermatologist\nclinic chain in india",style:TextStyle(fontFamily: "Montserrat",fontSize: 14.0,fontWeight: FontWeight.w500),),
                      Spacer(),
                      Image.asset("assets/login_image_3.png"),
                      SizedBox(width: 25),
                      Text("Customized solutions for\nIndian skin & hair",style:TextStyle(fontFamily: "Montserrat",fontSize: 14.0,fontWeight: FontWeight.w500),),
                      Spacer(),
                    ],
                  ),
                ),
              ),
             
              Row(
                children: [
                  Expanded(
                                      child: Container(
                        
                        child: Image.asset("assets/login_primary_image.png")),
                  ),
                  Expanded(
                                      child: Container(
                      // color: Colors.green,
                      child: Form(
                        key: _formkey,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Your E-mail",style:TextStyle(fontFamily: "Montserrat",fontSize: 18.0,fontWeight: FontWeight.w400),),
                            SizedBox(height: 3.0),
                            SizedBox(
                              width: screenSize.width * 0.4,
                              
                              child: TextFormField(
                                style: TextStyle(fontFamily: "Montserrat",fontSize: 18.0,fontWeight: FontWeight.w400),
                                  initialValue: "test_old@gmail.com",
                                  validator: (val) =>
                                      !EmailValidator.validate(val, true)
                                          ? 'Not a Valid Email.'
                                          : null,
                                  onSaved: (val) => email = val,
                                  decoration: new InputDecoration(
                                      hintText: "Email",
                                      hintStyle: TextStyle(fontFamily: "Montserrat",fontSize: 18.0,fontWeight: FontWeight.w400),
                                      border: new OutlineInputBorder(
                                        // borderRadius: new BorderRadius.circular(25.0),
                                        borderSide: new BorderSide(
                                            color: AppColors.colorAccent),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        // borderRadius: BorderRadius.circular(25.0),
                                        borderSide: BorderSide(
                                          width: 5.0,
                                          color: AppColors.colorAccent,
                                        ),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        // borderRadius: BorderRadius.circular(25.0),
                                        borderSide: BorderSide(
                                          color: Colors.grey,
                                          width: 2.0,
                                        ),
                                      ))
                                  // focusNode: _focusNode,
                                  ),
                            ),
                            SizedBox(height: 20),
                             Text("Password",style:TextStyle(fontFamily: "Montserrat",fontSize: 18.0,fontWeight: FontWeight.w400),),
                            SizedBox(height: 3.0),
                            SizedBox(
                              width: screenSize.width * 0.4,
                              child: TextFormField(
                                style: TextStyle(fontFamily: "Montserrat",fontSize: 18.0,fontWeight: FontWeight.w400),
                                  obscureText: true,
                                  initialValue: "Kaya@123",
                                  validator: (val) => val.length < 4
                                      ? 'Password too Short..'
                                      : null,
                                  onSaved: (val) => password = val,
                                  decoration: new InputDecoration(
                                    hintStyle: TextStyle(fontFamily: "Montserrat",fontSize: 18.0,fontWeight: FontWeight.w400),
                                      hintText: "Password",
                                      border: new OutlineInputBorder(
                                        // borderRadius: new BorderRadius.circular(25.0),
                                        borderSide: new BorderSide(
                                            color: AppColors.colorAccent),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        // borderRadius: BorderRadius.circular(25.0),
                                        borderSide: BorderSide(
                                          width: 5.0,
                                          color: AppColors.colorAccent,
                                        ),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        // borderRadius: BorderRadius.circular(25.0),
                                        borderSide: BorderSide(
                                          color: Colors.grey,
                                          width: 2.0,
                                        ),
                                      ))
                                  // focusNode: _focusNode,
                                  ),
                            ),
                            SizedBox(height: 20),
                            Padding(
                              padding: const EdgeInsets.only(left:88.0),
                              child: MyRaisedButton(
                                loading: loading,
                                buttonColor: Colors.black,
                                textColor: AppColors.colorWhite,
                                onPressed: login,
                                title: "Sign In",
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
