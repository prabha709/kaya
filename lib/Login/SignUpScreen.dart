import 'package:flutter/material.dart';
import 'package:kaya_skincare/utils/BaseUrl.dart';
import 'package:kaya_skincare/utils/app_colors.dart';

import 'package:http/http.dart' as http;
import 'package:email_validator/email_validator.dart';
import 'package:kaya_skincare/utils/raised_button.dart';
import 'package:wc_form_validators/wc_form_validators.dart';
import 'package:fluttertoast/fluttertoast.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();
  String email;
  String password;
  String username;
  String fname;
  String lname;
  bool loading = false;
  bool passWordVisible = true;

  void togglePasswordVisiblity() {
    setState(() {
      passWordVisible = !passWordVisible;
    });
  }

  login(context) async {
    String url = BaseUrl.url + "registeruser";
    if (_formkey.currentState.validate()) {
      setState(() {
        loading = true;
      });

      _formkey.currentState.save();

      Map data = {
        "email": email,
        "password": password,
        "username": username,
        "fname": fname,
        "lname": lname
      };
      var response = await http.post("$url", body: data);

      if (response.statusCode == 201) {
        setState(() {
          loading = false;
        });
        Navigator.pop(context);
        // Navigator.pushNamed(context, '/patientlist');

      } else {
        setState(() {
          loading = false;
        });
        return Fluttertoast.showToast(
            msg: response.body.toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 3,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      }
    }
  }

  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        height: screenSize.height,
        width: screenSize.width,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(BaseUrl.image), fit: BoxFit.fill)),
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 60),
              Opacity(
                opacity: 0.8,
                child: Row(
                  children: [
                    SizedBox(
                      width: 55,
                    ),
                    Image.asset("assets/Kaya_Clinic_logo 2.png",
                        width: screenSize.width / 7),
                    Spacer(),
                    Image.asset("assets/kaya_login_image_1.png"),
                    SizedBox(width: 25),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("18+ yrs of "),
                        Text("Dermatological Expertise"),
                      ],
                    ),
                    Spacer(),
                    Image.asset("assets/login_image_2.png"),
                    SizedBox(width: 25),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("18+ yrs of "),
                        Text("Dermatological Expertise"),
                      ],
                    ),
                    Spacer(),
                    Image.asset("assets/login_image_3.png"),
                    SizedBox(width: 25),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("18+ yrs of "),
                        Text("Dermatological Expertise"),
                      ],
                    ),
                    Spacer(),
                  ],
                ),
              ),
              SizedBox(height: 50),
              Row(
                children: [
                  Container(
                      width: screenSize.width / 2,
                      child: Image.asset("assets/login_primary_image.png")),
                  Form(
                    key: _formkey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Your e-mail", style: TextStyle(fontSize: 18)),
                        SizedBox(height: 20),
                        SizedBox(
                            child: TextFormField(
                                // initialValue: "prabha709@gmail.com",
                                validator: (val) =>
                                    !EmailValidator.validate(val, true)
                                        ? 'Not a Valid Email.'
                                        : null,
                                onSaved: (val) => email = val,
                                decoration: new InputDecoration(
                                    // fillColor: Colors.white,
                                    border: new OutlineInputBorder(
                                      // borderRadius: new BorderRadius.circular(25.0),
                                      borderSide: new BorderSide(
                                          color: AppColors.colorAccent),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      // borderRadius: BorderRadius.circular(25.0),
                                      borderSide: BorderSide(
                                        width: 5.0,
                                        color: AppColors.colorAccent,
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      // borderRadius: BorderRadius.circular(25.0),
                                      borderSide: BorderSide(
                                        color: Colors.grey,
                                        width: 2.0,
                                      ),
                                    ))
                                // focusNode: _focusNode,
                                ),
                            width: screenSize.width * 0.4),
                        SizedBox(height: 20),

                        Text("Your Password", style: TextStyle(fontSize: 18)),
                        SizedBox(height: 20),
                        SizedBox(
                            child: TextFormField(
                                obscureText: true,
                                // initialValue: "prabha",
                                validator: Validators.compose([
                                  Validators.required('Password is required'),
                                  Validators.patternString(
                                      r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$',
                                      'Invalid Password, Example:Test@123')
                                ]),
                                onSaved: (val) => password = val,
                                decoration: new InputDecoration(
                                    // fillColor: Colors.white,
                                    border: new OutlineInputBorder(
                                      // borderRadius: new BorderRadius.circular(25.0),
                                      borderSide: new BorderSide(
                                          color: AppColors.colorAccent),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      // borderRadius: BorderRadius.circular(25.0),
                                      borderSide: BorderSide(
                                        width: 5.0,
                                        color: AppColors.colorAccent,
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      // borderRadius: BorderRadius.circular(25.0),
                                      borderSide: BorderSide(
                                        color: Colors.grey,
                                        width: 2.0,
                                      ),
                                    ))
                                // focusNode: _focusNode,
                                ),
                            width: screenSize.width * 0.4),
                        SizedBox(height: 20),
                        Text("UserName", style: TextStyle(fontSize: 18)),
                        SizedBox(height: 20),
                        SizedBox(
                            child: TextFormField(
                                // initialValue: "prabha",

                                validator: (val) => val.isEmpty
                                    ? 'this Field is required.'
                                    : null,
                                onSaved: (val) => username = val,
                                decoration: new InputDecoration(
                                    labelText: "username",
                                    labelStyle:
                                        TextStyle(color: AppColors.colorAccent),
                                    // fillColor: Colors.white,
                                    border: new OutlineInputBorder(
                                      // borderRadius: new BorderRadius.circular(25.0),
                                      borderSide: new BorderSide(
                                          color: AppColors.colorAccent),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      // borderRadius: BorderRadius.circular(25.0),
                                      borderSide: BorderSide(
                                        width: 5.0,
                                        color: AppColors.colorAccent,
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      // borderRadius: BorderRadius.circular(25.0),
                                      borderSide: BorderSide(
                                        color: Colors.grey,
                                        width: 2.0,
                                      ),
                                    ))
                                // focusNode: _focusNode,
                                ),
                            width: screenSize.width * 0.4),
                        SizedBox(height: 20),
                        Text("FirstName", style: TextStyle(fontSize: 18)),
                        SizedBox(height: 20),
                        SizedBox(
                            child: TextFormField(
                                // initialValue: "prabha",

                                validator: (val) => val.isEmpty
                                    ? 'this Field is required.'
                                    : null,
                                onSaved: (val) => fname = val,
                                decoration: new InputDecoration(
                                    labelText: "firstname",
                                    labelStyle:
                                        TextStyle(color: AppColors.colorAccent),
                                    // fillColor: Colors.white,
                                    border: new OutlineInputBorder(
                                      // borderRadius: new BorderRadius.circular(25.0),
                                      borderSide: new BorderSide(
                                          color: AppColors.colorAccent),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      // borderRadius: BorderRadius.circular(25.0),
                                      borderSide: BorderSide(
                                        width: 5.0,
                                        color: AppColors.colorAccent,
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      // borderRadius: BorderRadius.circular(25.0),
                                      borderSide: BorderSide(
                                        color: Colors.grey,
                                        width: 2.0,
                                      ),
                                    ))
                                // focusNode: _focusNode,
                                ),
                            width: screenSize.width * 0.4),
                        SizedBox(height: 20),
                        Text("LastName", style: TextStyle(fontSize: 18)),
                        SizedBox(height: 20),
                        SizedBox(
                            child: TextFormField(
                                // initialValue: "prabha",

                                validator: (val) => val.isEmpty
                                    ? 'this Field is required.'
                                    : null,
                                onSaved: (val) => lname = val,
                                decoration: new InputDecoration(
                                    labelText: "lastname",
                                    labelStyle:
                                        TextStyle(color: AppColors.colorAccent),
                                    // fillColor: Colors.white,
                                    border: new OutlineInputBorder(
                                      // borderRadius: new BorderRadius.circular(25.0),
                                      borderSide: new BorderSide(
                                          color: AppColors.colorAccent),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      // borderRadius: BorderRadius.circular(25.0),
                                      borderSide: BorderSide(
                                        width: 5.0,
                                        color: AppColors.colorAccent,
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      // borderRadius: BorderRadius.circular(25.0),
                                      borderSide: BorderSide(
                                        color: Colors.grey,
                                        width: 2.0,
                                      ),
                                    ))
                                // focusNode: _focusNode,
                                ),
                            width: screenSize.width * 0.4),
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: MyRaisedButton(
                            loading: loading,
                            buttonColor: AppColors.colorAccent,
                            textColor: AppColors.colorWhite,
                            onPressed: login,
                            title: "Sign Up",
                          ),
                        ),
                        // Padding(
                        //     padding:
                        //     const EdgeInsets.only(right: 90.0, top: 10.0),
                        //     child: FlatButton(
                        //       child: Text(
                        //         "Already Having Account Login ?",
                        //         style: TextStyle(
                        //             fontSize: 23.0,
                        //             color: AppColors.colorAccent),
                        //       ),
                        //       onPressed: () {
                        //         Navigator.pop(context);
                        //       },
                        //     ))
                      ],
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
