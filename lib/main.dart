import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kaya_skincare/Login/SignUpScreen.dart';
import 'package:kaya_skincare/Provider/BeforeAfterProvider.dart';
import 'package:kaya_skincare/Provider/DignosisImageProvider.dart';
import 'package:kaya_skincare/Provider/DoctorProfileProvider.dart';

import 'package:kaya_skincare/Provider/Form1Provider.dart';
import 'package:kaya_skincare/Provider/MeasureImproveProvider.dart';
import 'package:kaya_skincare/Provider/PatientDetailsProvider.dart';
import 'package:kaya_skincare/Provider/PatientImageProvider.dart';
import 'package:kaya_skincare/Provider/PatientListProvider.dart';
import 'package:kaya_skincare/Provider/ServiceCheckProvider.dart';
import 'package:kaya_skincare/Provider/doctor_service_provider.dart';
import 'package:kaya_skincare/Provider/form2_provider.dart';
import 'package:kaya_skincare/Provider/product_recommendation_provider.dart';
import 'package:kaya_skincare/screens/AddPatient.dart';
import 'package:kaya_skincare/screens/PatientLIst.dart';
import 'package:kaya_skincare/screens/Welcome.dart';
import 'package:kaya_skincare/screens/side_menu_bar.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  // try {
  //   cameras = await availableCameras();
  // } on CameraException catch (e) {
  //   print('Error: $e.code\nError Message: $e.message');
  // }

  SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);

  await SystemChrome.setPreferredOrientations(
    [DeviceOrientation.landscapeLeft],
  );

  // await SystemChrome.setPreferredOrientations([DeviceOrientation.landscapeRight]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<PatientListProvider>(
          create: (BuildContext context) => PatientListProvider(),
        ),
        ChangeNotifierProvider<ProductRecommendationProvider>(
          create: (BuildContext context) => ProductRecommendationProvider(),
        ),
        ChangeNotifierProvider<PatientDetailsProvider>(
          create: (BuildContext context) => PatientDetailsProvider(),
        ),
        ChangeNotifierProvider<Form1Provider>(
          create: (BuildContext context) => Form1Provider(),
        ),
        ChangeNotifierProvider<DignosisImageProvider>(
          create: (BuildContext context) => DignosisImageProvider(),
        ),
        ChangeNotifierProvider<Form2Provider>(
          create: (BuildContext context) => Form2Provider(),
        ),
        ChangeNotifierProvider<PatientImageProvider>(
            create: (BuildContext context) => PatientImageProvider()),
        ChangeNotifierProvider<BeforeAfterProvider>(
            create: (BuildContext context) => BeforeAfterProvider()),
        ChangeNotifierProvider<MeasureImprovementsProvider>(
            create: (BuildContext context) => MeasureImprovementsProvider()),
        ChangeNotifierProvider<DoctorProfileProvider>(
            create: (BuildContext context) => DoctorProfileProvider()),
        ChangeNotifierProvider<ServiceCheckProvider>(
            create: (BuildContext context) => ServiceCheckProvider()),
        ChangeNotifierProvider<DoctorSericeProvider>(
            create: (BuildContext context) => DoctorSericeProvider()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
          textTheme: GoogleFonts.montserratTextTheme(
            Theme.of(context).textTheme,
          ),
        ),
        routes: {
          '/welcome': (context) => WelcomePage(),
          '/patientlist': (context) => PatientList(),
          '/signUp': (context) => SignUpScreen(),
          '/addpatient': (context) => AddPatient(),
          '/sidemenubar': (context) => SideMenuBar(),
          // '/form2': (context) => Form2()
        },

        // home: SideMenuBar(),
        home: MyHomePage(title: 'Flutter Demo Home Page'),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: WelcomePage());
  }
}
