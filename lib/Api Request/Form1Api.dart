import 'package:http/http.dart' as http;
import 'package:kaya_skincare/utils/BaseUrl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FormApi {
  Future<http.Response> patientForm1Data() async {
    final url = BaseUrl.url + "form1_all_questions/";
    print(url);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");
    return http.get("$url", headers: {"Authorization": "token $token"});
  }

  Future<http.Response> patientForm2Data(int patientId) async {
    final url = BaseUrl.url + "form2_all_questions/$patientId";
    print(url);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");
    return http.get("$url", headers: {"Authorization": "token $token"});
  }

   Future<http.Response> checkForm1Answer(int patientId) async {
    final url = BaseUrl.url + "patient_form1/$patientId";
    print(url);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");
    return http.get("$url", headers: {"Authorization": "token $token"});
    
  }
  Future<http.Response> checkForm2Answer(int patientId) async {
    final url = BaseUrl.url + "patient_form2/$patientId";
    print(url);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");
    return http.get("$url", headers: {"Authorization": "token $token"});
  }
}
