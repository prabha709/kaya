import 'package:http/http.dart' as http;
import 'package:kaya_skincare/utils/BaseUrl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PatientDataApi{

  int patientId;
PatientDataApi({
  this.patientId
});  
  



Future<http.Response> patientListData() async{
  final url =BaseUrl.url +"patient/$patientId";
  print(url);
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString("token");
  print(token);
return http.get("$url",headers: {"Authorization":"token $token"});
 
}
}