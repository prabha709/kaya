import 'package:http/http.dart' as http;
import 'package:kaya_skincare/utils/BaseUrl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DiagonasisApi{

  int patientId;
  int dImage;
DiagonasisApi({
  this.patientId,
  this.dImage
});  
  



Future<http.Response> patientDiagnosisImage() async{
  final url =BaseUrl.url +"diagnose_image/$patientId";
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString("token");
return http.get("$url",headers: {"Authorization":"token $token"});
 
}
Future<http.Response> deleteImage() async{
  final url =BaseUrl.url +"diagnose_image/$dImage";
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString("token");

  return http.delete("$url",headers: {"Authorization":"token $token"});
}



}