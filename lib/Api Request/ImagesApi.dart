import 'package:http/http.dart' as http;
import 'package:kaya_skincare/utils/BaseUrl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ImagesApi{

  int patientId;
  int imageId;
ImagesApi({
  this.patientId,
  this.imageId
});  
  



Future<http.Response> patientImage() async{
  final url =BaseUrl.url +"patient_image/$patientId";
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString("token");
return http.get("$url",headers: {"Authorization":"token $token"});
 
}
Future<http.Response> deleteImage() async{
  final url =BaseUrl.url +"patient_image/$imageId";
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString("token");
return http.delete("$url",headers: {"Authorization":"token $token"});
 
}
}