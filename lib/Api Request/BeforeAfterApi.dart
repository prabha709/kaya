import 'package:http/http.dart' as http;
import 'package:kaya_skincare/utils/BaseUrl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BeforeAfterApi{

  int patientId;
  int imageId;
BeforeAfterApi({
  this.patientId,
  this.imageId
});  
  



Future<http.Response> beforeAfterImage() async{
  final url =BaseUrl.url +"patient_before_after/$patientId";
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString("token");
return http.get("$url",headers: {"Authorization":"token $token"});
 
}

Future<http.Response> deleteImage() async{
  final url =BaseUrl.url +"patient_before_after/$imageId";
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString("token");
return http.delete("$url",headers: {"Authorization":"token $token"});
 
}
}