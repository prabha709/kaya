import 'package:http/http.dart' as http;
import 'package:kaya_skincare/utils/BaseUrl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MeasureImprovementsApi{
int imageId;
  int patientId;
MeasureImprovementsApi({
  this.patientId,
  this.imageId
});  
  



Future<http.Response> patientData() async{
  final url =BaseUrl.url +"patient_measure_improvement/$patientId";
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString("token");
return http.get("$url",headers: {"Authorization":"token $token"});
 
}
Future<http.Response> deleteImage() async{
  final url =BaseUrl.url +"patient_measure_improvement/$imageId";
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString("token");
return http.delete("$url",headers: {"Authorization":"token $token"});
 
}
}