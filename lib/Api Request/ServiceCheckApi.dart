import 'package:http/http.dart' as http;
import 'package:kaya_skincare/utils/BaseUrl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ServiceCheckApi {
  int patientId;
  int dImage;
  ServiceCheckApi({this.patientId, this.dImage});

  Future<http.Response> serviceCheck() async {
    final url = BaseUrl.url + "check_service_product/$patientId";
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");
    return http.get("$url", headers: {"Authorization": "token $token"});
  }
}
