import 'package:http/http.dart' as http;
import 'package:kaya_skincare/utils/BaseUrl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DoctorProfileApi{


  



Future<http.Response> doctorProfile() async{
  final url =BaseUrl.url +"doctor_info/";
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString("token");
return http.get("$url",headers: {"Authorization":"token $token"});
 
}
}