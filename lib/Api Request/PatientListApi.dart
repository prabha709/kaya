import 'package:http/http.dart' as http;
import 'package:kaya_skincare/utils/BaseUrl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PatientListApi{
  

  int patientId;
PatientListApi({
  this.patientId
});  
  
  
  final url =BaseUrl.url +"patient";


  Future<http.Response> patientListData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String token = prefs.getString("token");
    print(url);
    return http.get("$url", headers: {"Authorization": "token $token"});
  }
   Future<http.Response> patientListDelete() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
String deleteUrl =url+"/"+"$patientId";
    String token = prefs.getString("token");
 
    return http.delete("$deleteUrl", headers: {"Authorization": "token $token"});
  }
}