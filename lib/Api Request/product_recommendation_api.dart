import 'package:kaya_skincare/utils/BaseUrl.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class ProductRecommendationApi {
  final String baseUrl = BaseUrl.url;

  Future<http.Response> getProductRecommendations(int patientId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");
    String url = BaseUrl.url + "servicelist/$patientId";
    print(url);
    return http.get("$url", headers: {"Authorization": "token $token"});
  }

  Future<http.Response> postProductRecommendations(
      String patientId, Map recommendationData) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");
    String url = baseUrl + "servicelist/" + patientId;
    String encodedRecommendationData = json.encode(recommendationData);
    print(encodedRecommendationData);
    print(url);
    return http.post("$url",
        headers: {
          "Authorization": "token $token",
          "Content-Type": "application/json"
        },
        body: encodedRecommendationData);
  }
}
