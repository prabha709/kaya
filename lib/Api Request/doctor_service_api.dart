import 'package:http/http.dart' as http;
import 'package:kaya_skincare/utils/BaseUrl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DoctorServiceApi {
  int patientId;

  DoctorServiceApi({
    this.patientId,
  });

  Future<http.Response> doctorServiceApi() async {
    final url = BaseUrl.url + "doctor-service";
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");
    return http.get("$url", headers: {"Authorization": "token $token"});
  }
}
