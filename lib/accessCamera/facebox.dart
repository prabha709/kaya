import 'package:flutter/material.dart';

class BndBox extends StatelessWidget {
  final List<dynamic> results;
  final int previewH;
  final int previewW;
  final double screenH;
  final double screenW;

  BndBox(
    this.results,
    this.previewH,
    this.previewW,
    this.screenH,
    this.screenW,
  );

  @override
  Widget build(BuildContext context) {
    List<Widget> _renderBox() {
      return results.map((re) {
        var _x = re["rect"]["x"];
        var _w = re["rect"]["w"];
        var _y = re["rect"]["y"];
        var _h = re["rect"]["h"];
        var scaleW, scaleH, x, y, w, h;

        if (screenH / screenW > previewH / previewW) {
          scaleW = screenH / previewH * previewW;
          scaleH = screenH;
          var difW = (scaleW - screenW) / scaleW;
          x = (_x - difW / 2) * scaleW;
          w = _w * scaleW;
          if (_x < difW / 2) w -= (difW / 2 - _x) * scaleW;
          y = _y * scaleH;
          h = _h * scaleH;
        } else {
          scaleH = screenW / previewW * previewH;
          scaleW = screenW;
          var difH = (scaleH - screenH) / scaleH;
          x = _x * scaleW;
          w = _w * scaleW;
          y = (_y - difH / 2) * scaleH;
          h = _h * scaleH;
          if (_y < difH / 2) h -= (difH / 2 - _y) * scaleH;
        }
        double perimeter = 2 * (_w + _h);
        print(perimeter);
        return Positioned(
          left: 300,
          top: 30,
          width: previewW + _w * previewW,
          height: _y * previewH + _h * previewH,
          child: Container(
            padding: EdgeInsets.only(left: 5.0),
            child: perimeter < 2.95
                ? Padding(
                    padding: const EdgeInsets.only(
                      right: 8.0,
                    ),
                    child: Text(
                      "Stand Closer",
                      style: TextStyle(
                        color: Colors.red,
                        fontSize: 35.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  )
                : perimeter > 3.55
                    ? Text(
                        "Stand back",
                        style: TextStyle(
                          color: Colors.red,
                          fontSize: 35.0,
                          fontWeight: FontWeight.bold,
                        ),
                      )
                    : Text(
                        "Take Photo",
                        style: TextStyle(
                          color: Color.fromRGBO(37, 213, 253, 1.0),
                          fontSize: 35.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
          ),
        );
      }).toList();
    }

    return Stack(
      children: _renderBox(),
    );
  }
}
