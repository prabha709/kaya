import 'dart:math' as math;
import 'dart:io';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:kaya_skincare/Provider/DignosisImageProvider.dart';
import 'package:kaya_skincare/accessCamera/facebox.dart';
import 'package:kaya_skincare/utils/Loader.dart';
import 'package:kaya_skincare/utils/app_colors.dart';
import 'package:provider/provider.dart';
import 'package:kaya_skincare/utils/BaseUrl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:tflite/tflite.dart';

XFile imageFile1;
CameraController controller;
List<CameraDescription> cameras;

class OpenCamers extends StatefulWidget {
  final int patientIndex;
  final bool patchImage;

  OpenCamers({this.patientIndex, this.patchImage});

  @override
  _OpenCamersState createState() => _OpenCamersState();
}

class _OpenCamersState extends State<OpenCamers> {
  DignosisImageProvider dignosisImageProvider;
  String message;
  bool appbar = false;
  bool openCamera1 = false;
  bool loading = false;
  bool apiloading = false;
  bool getImages = false;
  bool selectImages = false;
  String imageData;
  bool longPress = false;
  List<dynamic> _recognitions;
  int _imageHeight = 0;
  int _imageWidth = 0;
  bool isDetecting = false;
  double itemSize = 100.0;
  setRecognitions(recognitions, imageHeight, imageWidth) {
    setState(() {
      _recognitions = recognitions;
      _imageHeight = imageHeight;
      _imageWidth = imageWidth;
    });
  }

  openTheCamera() async {
    cameras = await availableCameras();

    controller = CameraController(cameras[0], ResolutionPreset.ultraHigh);

    controller.initialize().then((_) {
      if (!mounted) {
        return;
      }
      // controller.setFlashMode(FlashMode.off);
      controller.lockCaptureOrientation(DeviceOrientation.portraitUp);
      setState(() {});

      controller.startImageStream((CameraImage img) {
        if (!isDetecting) {
          isDetecting = true;
          Tflite.detectObjectOnFrame(
            bytesList: img.planes.map((plane) {
              return plane.bytes;
            }).toList(),
            model: "SSDMobileNet",
            imageHeight: img.height,
            imageWidth: img.width,
            imageMean: 127.5,
            imageStd: 127.5,
            numResultsPerClass: 1,
            threshold: 0.4,
          ).then((recognitions) {
            setRecognitions(recognitions, img.height, img.width);
            print("datarec $recognitions");

            isDetecting = false;
          });
        }
      });
    });
  }

  Future<XFile> takePicture() async {
    if (!controller.value.isInitialized) {
      // showInSnackBar('Error: select a camera first.');
      return null;
    }

    if (controller.value.isTakingPicture) {
      // A capture is already pending, do nothing.
      return null;
    }

    try {
      XFile file = await controller.takePicture();
      return file;
    } on CameraException catch (e) {
      print(e);
      // _showCameraException(e);
      return null;
    }
  }

  void onTakePictureButtonPressed() {
    controller.stopImageStream();
    takePicture().then((XFile file) {
      if (mounted) {
        setState(() {
          imageFile1 = file;
          openCamera1 = false;
        });

        // File data = file.toFile();

        // if (file != null) showInSnackBar('Picture saved to ${file.path}');
      }
    });
  }

  Widget _captureControlRowWidget() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 25),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          GestureDetector(
              onTap: controller != null && controller.value.isInitialized
                  ? onTakePictureButtonPressed
                  : null,
              child: Icon(
                Icons.circle,
                color: AppColors.colorWhite,
                size: 75,
              ))
        ],
      ),
    );
  }

  @override
  void initState() {
    openTheCamera();
    super.initState();
  }

  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _patchImage() async {
      DignosisImageProvider dignosisImageProvider =
          Provider.of<DignosisImageProvider>(context, listen: false);
      int id = dignosisImageProvider.diagnosisImageModel.resp[0].id;
      String url = BaseUrl.url + "diagnose_image/$id";
      SharedPreferences prefs = await SharedPreferences.getInstance();
      setState(() {
        loading = true;
      });
      String token = prefs.getString("token");
      Map<String, String> data = {"Authorization": "token $token"};

      var request = http.MultipartRequest("PATCH", Uri.parse("$url"));
      request.headers.addAll(data);
      // selectImages == false
      //     ?
      request.files.add(await http.MultipartFile.fromPath(
        "d_image",
        imageFile1.path,
      ));
      // : request.files.add(http.MultipartFile.fromString(
      //     "d_image",
      //     imageData,
      //   ));
      var res = await request.send();
      final respStr = res.stream.bytesToString();
      print(res.statusCode);
      if (res.statusCode == 200) {
        setState(() {
          getImages = true;
          imageFile1 = null;
          loading = false;
        });
        Fluttertoast.showToast(
            msg: "Your Result is Ready..",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.green,
            textColor: Colors.black,
            fontSize: 16.0);
        SystemChrome.setPreferredOrientations(
            [DeviceOrientation.landscapeLeft]);
        Navigator.pop(context);
        return respStr;
      } else if (res.statusCode == 204) {
        openTheCamera();
        setState(() {
          loading = false;
        });
        Fluttertoast.showToast(
            msg: "No Face Found in Image",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
        return null;
      } else {
        openTheCamera();
        setState(() {
          getImages = true;
          imageFile1 = null;
          loading = false;
        });
        Fluttertoast.showToast(
            msg: "Internal Server Error",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
        return null;
      }

      // setState(() {
      //   result= respStr;
      // });
    }

    _uploadImage() async {
      String url = BaseUrl.url + "diagnose_image/${widget.patientIndex}";
      SharedPreferences prefs = await SharedPreferences.getInstance();
      setState(() {
        loading = true;
      });
      String token = prefs.getString("token");
      Map<String, String> data = {"Authorization": "token $token"};

      var request = http.MultipartRequest("POST", Uri.parse("$url"));
      request.headers.addAll(data);
      // selectImages == false
      //     ?
      request.files.add(await http.MultipartFile.fromPath(
        "d_image",
        imageFile1.path,
      ));
      // : request.files.add(http.MultipartFile.fromString(
      //     "d_image",
      //     imageData,
      //   ));
      var res = await request.send();
      final respStr = res.stream.bytesToString();
      print(res.statusCode);
      if (res.statusCode == 200) {
        setState(() {
          getImages = true;
          imageFile1 = null;
          loading = false;
        });
        Fluttertoast.showToast(
            msg: "Your Result is Ready.",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.green,
            textColor: Colors.black,
            fontSize: 16.0);
        SystemChrome.setPreferredOrientations(
            [DeviceOrientation.landscapeLeft]);
        Navigator.pop(context);
        return respStr;
      } else if (res.statusCode == 204) {
        openTheCamera();
        setState(() {
          loading = false;
        });
        Fluttertoast.showToast(
            msg: "No Face Found in Image",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
        return null;
      } else {
        setState(() {
          loading = false;
        });
        openTheCamera();
        Fluttertoast.showToast(
            msg: "Internal Server Error",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
        return null;
      }
    }

    Future<bool> _onBackPressed() {
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Are you sure?'),
              content: Text('You are going to exit the application!!'),
              actions: <Widget>[
                TextButton(
                  child: Text('NO'),
                  onPressed: () {
                    Navigator.of(context).pop(false);
                  },
                ),
                TextButton(
                  child: Text('YES'),
                  onPressed: () {
                    controller.stopImageStream();
                    SystemChrome.setPreferredOrientations(
                        [DeviceOrientation.landscapeLeft]);
                    Navigator.of(context).pop(true);
                  },
                ),
              ],
            );
          });
    }

    dignosisImageProvider =
        Provider.of<DignosisImageProvider>(context, listen: false);

    /*24 is for notification bar on Android*/

    Size screenSize = MediaQuery.of(context).size;

    return Scaffold(
        appBar: AppBar(
          leading: InkWell(
            onTap: () {
              controller.stopImageStream();
              SystemChrome.setPreferredOrientations(
                  [DeviceOrientation.landscapeLeft]);
              Navigator.pop(context);
              // setState(() {
              //   getImages = true;
              // });
            },
            child: Icon(
              Icons.arrow_back_ios_outlined,
              color: Colors.black,
            ),
          ),
          flexibleSpace: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/bckg_img.png"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          elevation: 0.0,
          automaticallyImplyLeading: false,
          title: Container(
            decoration: BoxDecoration(
              color: Color(0xffF0F0F0),
              borderRadius: BorderRadius.all(Radius.circular(35)),
            ),
            width: screenSize.width / 2.5,
            height: screenSize.height / 12,
            child: Center(
                child: Text(
              "Capture Image",
              style: TextStyle(color: Colors.black, fontSize: 25),
            )),
          ),
          toolbarHeight: 160.0,
        ),
        body: loading
            ? AppLoader()
            : WillPopScope(
                onWillPop: _onBackPressed,
                child: Container(
                  height: screenSize.height,
                  width: screenSize.width,
                  color: AppColors.greyColor,
                  child: imageFile1 == null
                      ? Stack(children: [
                          AspectRatio(
                            aspectRatio:
                                screenSize.width * 1.142 / screenSize.height,
                            child: CameraPreview(controller),
                          ),
                          BndBox(
                            _recognitions == null ? [] : _recognitions,
                            math.max(_imageHeight, _imageWidth),
                            math.min(_imageHeight, _imageWidth),
                            screenSize.width,
                            screenSize.height - 160,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 14.0),
                            child: Image.asset("assets/jaimin_face_green.ico"),
                          ),
                          Align(
                              alignment: Alignment.bottomCenter,
                              child: _captureControlRowWidget())
                        ])
                      : RotatedBox(
                          quarterTurns: 4,
                          child: Container(
                              height: screenSize.height,
                              width: screenSize.width,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      fit: BoxFit.fill,
                                      image:
                                          FileImage(File(imageFile1.path))))),
                        ),
                ),
              ),
        floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
        floatingActionButton: imageFile1 != null
            ? Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 18.0, bottom: 0),
                    child: InkWell(
                      child: Icon(
                        Icons.close,
                        size: 75,
                        color: Colors.white,
                      ),
                      onTap: () {
                        openTheCamera();
                        setState(() {
                          imageFile1 = null;
                        });
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 18.0, bottom: 0),
                    child: InkWell(
                      child: Icon(
                        Icons.check,
                        size: 75,
                        color: Colors.white,
                      ),
                      onTap: () {
                        widget.patchImage == false
                            ? _uploadImage()
                            : _patchImage();
                      },
                    ),
                  ),
                ],
              )
            : Container());
  }
}
