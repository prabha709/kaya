class PatientImageModel {
  List<All> all;
  List<PostImages> postImages;

  PatientImageModel({this.all, this.postImages});

  PatientImageModel.fromJson(Map<String, dynamic> json) {
    if (json['all'] != null) {
      all = new List<All>();
      json['all'].forEach((v) {
        all.add(new All.fromJson(v));
      });
    }
    if (json['post_images'] != null) {
      postImages = new List<PostImages>();
      json['post_images'].forEach((v) {
        postImages.add(new PostImages.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.all != null) {
      data['all'] = this.all.map((v) => v.toJson()).toList();
    }
    if (this.postImages != null) {
      data['post_images'] = this.postImages.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class All {
  int id;
  String image;
  String createdDate;

  All({this.id, this.image, this.createdDate});

  All.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    image = json['image'];
    createdDate = json['created_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['image'] = this.image;
    data['created_date'] = this.createdDate;
    return data;
  }
}

class PostImages {
  int id;
  String image;
  String createdDate;

  PostImages({this.id, this.image, this.createdDate});

  PostImages.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    image = json['image'];
    createdDate = json['created_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['image'] = this.image;
    data['created_date'] = this.createdDate;
    return data;
  }
}
