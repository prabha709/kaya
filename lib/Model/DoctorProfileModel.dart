class DoctorProfileModel {
  Resp resp;

  DoctorProfileModel({this.resp});

  DoctorProfileModel.fromJson(Map<String, dynamic> json) {
    resp = json['resp'] != null ? new Resp.fromJson(json['resp']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.resp != null) {
      data['resp'] = this.resp.toJson();
    }
    return data;
  }
}

class Resp {
  int id;
  String email;
  String username;
  String fname;
  String lname;

  Resp({this.id, this.email, this.username, this.fname, this.lname});

  Resp.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    email = json['email'];
    username = json['username'];
    fname = json['fname'];
    lname = json['lname'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['email'] = this.email;
    data['username'] = this.username;
    data['fname'] = this.fname;
    data['lname'] = this.lname;
    return data;
  }
}