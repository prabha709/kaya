class ServiceCheckModel {
  int id;
  String profilePic;
  String patientName;
  String patientEmail;
  int age;
  String gender;
  Null dob;
  String phone;
  Null regiDate;
  Null heartbeat;
  Null bp;
  Null dustAllergy;
  Null medication;
  String guestCode;

  ServiceCheckModel(
      {this.id,
      this.profilePic,
      this.patientName,
      this.patientEmail,
      this.age,
      this.gender,
      this.dob,
      this.phone,
      this.regiDate,
      this.heartbeat,
      this.bp,
      this.dustAllergy,
      this.medication,
      this.guestCode});

  ServiceCheckModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    profilePic = json['profile_pic'];
    patientName = json['patient_name'];
    patientEmail = json['patient_email'];
    age = json['age'];
    gender = json['gender'];
    dob = json['dob'];
    phone = json['phone'];
    regiDate = json['regi_date'];
    heartbeat = json['heartbeat'];
    bp = json['bp'];
    dustAllergy = json['dust_allergy'];
    medication = json['medication'];
    guestCode = json['guest_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['profile_pic'] = this.profilePic;
    data['patient_name'] = this.patientName;
    data['patient_email'] = this.patientEmail;
    data['age'] = this.age;
    data['gender'] = this.gender;
    data['dob'] = this.dob;
    data['phone'] = this.phone;
    data['regi_date'] = this.regiDate;
    data['heartbeat'] = this.heartbeat;
    data['bp'] = this.bp;
    data['dust_allergy'] = this.dustAllergy;
    data['medication'] = this.medication;
    data['guest_code'] = this.guestCode;
    return data;
  }
}
