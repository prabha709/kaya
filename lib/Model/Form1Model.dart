class Form1Model {
  List<Textfield> textfield;
  List<Checkbox> checkbox;
  List<Resp> resp;

  Form1Model({this.textfield, this.checkbox, this.resp});

  Form1Model.fromJson(Map<String, dynamic> json) {
    if (json['textfield'] != null) {
      textfield = new List<Textfield>();
      json['textfield'].forEach((v) {
        textfield.add(new Textfield.fromJson(v));
      });
    }
    if (json['checkbox'] != null) {
      checkbox = new List<Checkbox>();
      json['checkbox'].forEach((v) {
        checkbox.add(new Checkbox.fromJson(v));
      });
    }
    if (json['resp'] != null) {
      resp = new List<Resp>();
      json['resp'].forEach((v) {
        resp.add(new Resp.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.textfield != null) {
      data['textfield'] = this.textfield.map((v) => v.toJson()).toList();
    }
    if (this.checkbox != null) {
      data['checkbox'] = this.checkbox.map((v) => v.toJson()).toList();
    }
    if (this.resp != null) {
      data['resp'] = this.resp.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Textfield {
  int id;
  QuestionType questionType;

  Textfield({this.id, this.questionType});

  Textfield.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    questionType = json['question_type'] != null
        ? new QuestionType.fromJson(json['question_type'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.questionType != null) {
      data['question_type'] = this.questionType.toJson();
    }
    return data;
  }
}

class Checkbox {
  int id;
  QuestionType questionType;

  Checkbox({this.id, this.questionType});

  Checkbox.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    questionType = json['question_type'] != null
        ? new QuestionType.fromJson(json['question_type'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.questionType != null) {
      data['question_type'] = this.questionType.toJson();
    }
    return data;
  }
}
class Resp {
  int id;
  QuestionType questionType;

  Resp({this.id, this.questionType});

  Resp.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    questionType = json['question_type'] != null
        ? new QuestionType.fromJson(json['question_type'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.questionType != null) {
      data['question_type'] = this.questionType.toJson();
    }
    return data;
  }
}

class QuestionType {
  String type;
  String question;
  QuestionOptions questionOptions;

  QuestionType({this.type, this.question, this.questionOptions});

  QuestionType.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    question = json['question'];
    questionOptions = json['question_options'] != null
        ? new QuestionOptions.fromJson(json['question_options'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = this.type;
    data['question'] = this.question;
    if (this.questionOptions != null) {
      data['question_options'] = this.questionOptions.toJson();
    }
    return data;
  }
}

class QuestionOptions {
  List<Option> option;

  QuestionOptions({this.option});

  QuestionOptions.fromJson(Map<String, dynamic> json) {
    if (json['option'] != null) {
      option = [];
      json['option'].forEach((v) {
        option.add(new Option.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.option != null) {
      data['option'] = this.option.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Option {
  Option1 option1;
  Option1 option2;
  Option1 option3;
  Option1 option4;
  Option1 option5;

  Option(
      {this.option1, this.option2, this.option3, this.option4, this.option5});

  Option.fromJson(Map<String, dynamic> json) {
    option1 =
        json['option1'] != null ? new Option1.fromJson(json['option1']) : null;
    option2 =
        json['option2'] != null ? new Option1.fromJson(json['option2']) : null;
    option3 =
        json['option3'] != null ? new Option1.fromJson(json['option3']) : null;
    option4 =
        json['option4'] != null ? new Option1.fromJson(json['option4']) : null;
    option5 =
        json['option5'] != null ? new Option1.fromJson(json['option5']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.option1 != null) {
      data['option1'] = this.option1.toJson();
    }
    if (this.option2 != null) {
      data['option2'] = this.option2.toJson();
    }
    if (this.option3 != null) {
      data['option3'] = this.option3.toJson();
    }
    if (this.option4 != null) {
      data['option4'] = this.option4.toJson();
    }
    if (this.option5 != null) {
      data['option5'] = this.option5.toJson();
    }
    return data;
  }
}

class Option1 {
  List<Text> text;

  Option1({this.text});

  Option1.fromJson(Map<String, dynamic> json) {
    if (json['text'] != null) {
      text = new List<Text>();
      json['text'].forEach((v) {
        text.add(new Text.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.text != null) {
      data['text'] = this.text.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Text {
  String data1;
  String data2;

  Text({this.data1, this.data2});

  Text.fromJson(Map<String, dynamic> json) {
    data1 = json['data1'];
    data2 = json['data2'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['data1'] = this.data1;
    data['data2'] = this.data2;
    return data;
  }
}