class Form1AnswerModel {
  Resp resp;

  Form1AnswerModel({this.resp});

  Form1AnswerModel.fromJson(Map<String, dynamic> json) {
    resp = json['resp'] != null ? new Resp.fromJson(json['resp']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.resp != null) {
      data['resp'] = this.resp.toJson();
    }
    return data;
  }
}

class Resp {
  int id;
  List<QuestionAnswers> questionAnswers;

  Resp({this.id, this.questionAnswers});

  Resp.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    if (json['question&answers'] != null) {
      questionAnswers = new List<QuestionAnswers>();
      json['question&answers'].forEach((v) {
        questionAnswers.add(new QuestionAnswers.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.questionAnswers != null) {
      data['question&answers'] =
          this.questionAnswers.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class QuestionAnswers {
  int serialNo;
  String question;
  String answer;

  QuestionAnswers({this.serialNo, this.question, this.answer});

  QuestionAnswers.fromJson(Map<String, dynamic> json) {
    serialNo = json['serial_no'];
    question = json['question'];
    answer = json['answer'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['serial_no'] = this.serialNo;
    data['question'] = this.question;
    data['answer'] = this.answer;
    return data;
  }
}