class ProductRecommendationPostResponse {
  int id;
  String service;
  String product;

  ProductRecommendationPostResponse({this.id, this.service, this.product});

  ProductRecommendationPostResponse.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    service = json['service'];
    product = json['product'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['service'] = this.service;
    data['product'] = this.product;
    return data;
  }
}
