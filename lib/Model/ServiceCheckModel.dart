class ServiceCheckModel {
  bool resp;

  ServiceCheckModel({this.resp});

  ServiceCheckModel.fromJson(Map<String, dynamic> json) {
    resp = json['resp'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['resp'] = this.resp;
    return data;
  }
}
