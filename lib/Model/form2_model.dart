class Form2Model {
  List<Resp> resp;
  String flag;

  Form2Model({this.resp, this.flag});

  Form2Model.fromJson(Map<String, dynamic> json) {
    if (json['resp'] != null) {
      resp = new List<Resp>();
      json['resp'].forEach((v) {
        resp.add(new Resp.fromJson(v));
      });
    }
    flag = json['flag'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.resp != null) {
      data['resp'] = this.resp.map((v) => v.toJson()).toList();
    }
    data['flag'] = this.flag;
    return data;
  }
}

class Resp {
  String id;
  int form1id;
  QuestionType questionType;

  Resp({this.id, this.questionType, this.form1id});

  Resp.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    form1id = json["form1_id"];
    questionType = json['question_type'] != null
        ? new QuestionType.fromJson(json['question_type'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.questionType != null) {
      data['question_type'] = this.questionType.toJson();
    }
    return data;
  }
}

class QuestionType {
  String question;
  List<Type> type;
  List<Option> option;

  QuestionType({this.question, this.type, this.option});

  QuestionType.fromJson(Map<String, dynamic> json) {
    question = json['question'];
    if (json['type'] != null) {
      type = new List<Type>();
      json['type'].forEach((v) {
        type.add(new Type.fromJson(v));
      });
    }
    if (json['option'] != null) {
      option = new List<Option>();
      json['option'].forEach((v) {
        option.add(new Option.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['question'] = this.question;
    if (this.type != null) {
      data['type'] = this.type.map((v) => v.toJson()).toList();
    }
    if (this.option != null) {
      data['option'] = this.option.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Type {
  String data;

  Type({this.data});

  Type.fromJson(Map<String, dynamic> json) {
    data = json['data'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['data'] = this.data;
    return data;
  }
}

class Option {
  String answerType;
  String options;

  Option({this.answerType, this.options});

  Option.fromJson(Map<String, dynamic> json) {
    answerType = json['answer_type'];
    options = json['options'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['answer_type'] = this.answerType;
    data['options'] = this.options;
    return data;
  }
}