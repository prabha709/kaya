class ProductRecommendationModel {
  List<Resp> resp;

  ProductRecommendationModel({this.resp});

  ProductRecommendationModel.fromJson(Map<String, dynamic> json) {
    if (json['resp'] != null) {
      resp = new List<Resp>();
      json['resp'].forEach((v) {
        resp.add(new Resp.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.resp != null) {
      data['resp'] = this.resp.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Resp {
  String concern;
  List<Service> service;
  List<Product> product;
  bool pregBfeedStatus;

  Resp({this.concern, this.service, this.product, this.pregBfeedStatus});

  Resp.fromJson(Map<String, dynamic> json) {
    concern = json['concern'];
    if (json['service'] != null) {
      service = new List<Service>();
      json['service'].forEach((v) {
        service.add(new Service.fromJson(v));
      });
    }
    if (json['product'] != null) {
      product = new List<Product>();
      json['product'].forEach((v) {
        product.add(new Product.fromJson(v));
      });
    }
    pregBfeedStatus = json['preg_bfeed_status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['concern'] = this.concern;
    if (this.service != null) {
      data['service'] = this.service.map((v) => v.toJson()).toList();
    }
    if (this.product != null) {
      data['product'] = this.product.map((v) => v.toJson()).toList();
    }
    data['preg_bfeed_status'] = this.pregBfeedStatus;
    return data;
  }
}

class Service {
  String serviceName;

  Service({this.serviceName});

  Service.fromJson(Map<String, dynamic> json) {
    serviceName = json['service_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['service_name'] = this.serviceName;
    return data;
  }
}

class Product {
  String productName;
  String productImage;

  Product({this.productName, this.productImage});

  Product.fromJson(Map<String, dynamic> json) {
    productName = json['product_name'];
    productImage = json['product_image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['product_name'] = this.productName;
    data['product_image'] = this.productImage;
    return data;
  }
}