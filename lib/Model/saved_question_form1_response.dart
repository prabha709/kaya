class SavedAnswersForm1Response {
  List<String> question;
  List<String> answer;
  bool flag;

  SavedAnswersForm1Response({this.question, this.answer, this.flag});

  SavedAnswersForm1Response.fromJson(Map<String, dynamic> json) {
    question = json['question'].cast<String>();
    answer = json['answer'].cast<String>();
    flag = json['flag'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['question'] = this.question;
    data['answer'] = this.answer;
    data['flag'] = this.flag;
    return data;
  }
}
