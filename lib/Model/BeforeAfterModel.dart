class BeforeAfterModel {
  List<Resp> resp;

  BeforeAfterModel({this.resp});

  BeforeAfterModel.fromJson(Map<String, dynamic> json) {
    if (json['resp'] != null) {
      resp = new List<Resp>();
      json['resp'].forEach((v) {
        resp.add(new Resp.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.resp != null) {
      data['resp'] = this.resp.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Resp {
  int id;
  String image;
  String processImage;
  String createdDate;
  String updatedDate;

  Resp(
      {this.id,
      this.image,
      this.processImage,
      this.createdDate,
      this.updatedDate});

  Resp.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    image = json['image'];
    processImage = json['process_image'];
    createdDate = json['created_date'];
    updatedDate = json['updated_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['image'] = this.image;
    data['process_image'] = this.processImage;
    data['created_date'] = this.createdDate;
    data['updated_date'] = this.updatedDate;
    return data;
  }
}