class Form2Response {
  Resp resp;

  Form2Response({this.resp});

  Form2Response.fromJson(Map<String, dynamic> json) {
    resp = json['resp'] != null ? new Resp.fromJson(json['resp']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.resp != null) {
      data['resp'] = this.resp.toJson();
    }
    return data;
  }
}

class Resp {
  int id;
  String question;
  String answer;
  List<SkinType> skinType;

  Resp({this.id, this.question, this.answer, this.skinType});

  Resp.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    question = json['question'];
    answer = json['answer'];
    if (json['skin_type'] != null) {
      skinType = new List<SkinType>();
      json['skin_type'].forEach((v) {
        skinType.add(new SkinType.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['question'] = this.question;
    data['answer'] = this.answer;
    if (this.skinType != null) {
      data['skin_type'] = this.skinType.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class SkinType {
  String data;

  SkinType({this.data});

  SkinType.fromJson(Map<String, dynamic> json) {
    data = json['data'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['data'] = this.data;
    return data;
  }
}