class FormDetailResponse {
  int id;
  String answerType;
  String question;
  String answer;
  String createdDate;
  String updatedDate;

  FormDetailResponse(
      {this.id,
        this.answerType,
        this.question,
        this.answer,
        this.createdDate,
        this.updatedDate});

  FormDetailResponse.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    answerType = json['answer_type'];
    question = json['question'];
    answer = json['answer'];
    createdDate = json['created_date'];
    updatedDate = json['updated_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['answer_type'] = this.answerType;
    data['question'] = this.question;
    data['answer'] = this.answer;
    data['created_date'] = this.createdDate;
    data['updated_date'] = this.updatedDate;
    return data;
  }
}
