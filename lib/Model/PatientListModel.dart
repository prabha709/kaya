class PatientListModel {
  List<Resp> resp;

  PatientListModel({this.resp});

  PatientListModel.fromJson(Map<String, dynamic> json) {
    if (json['resp'] != null) {
      resp = new List<Resp>();
      json['resp'].forEach((v) {
        resp.add(new Resp.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.resp != null) {
      data['resp'] = this.resp.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Resp {
  int id;
  String patientName;
  int age;
  String gender;
  String dob;
  String phone;
  String regiDate;
  int heartbeat;
  String bp;
  String dustAllergy;
  String medication;

  Resp(
      {this.id,
      this.patientName,
      this.age,
      this.gender,
      this.dob,
      this.phone,
      this.regiDate,
      this.heartbeat,
      this.bp,
      this.dustAllergy,
      this.medication});

  Resp.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    patientName = json['patient_name'];
    age = json['age'];
    gender = json['gender'];
    dob = json['dob'];
    phone = json['phone'];
    regiDate = json['regi_date'];
    heartbeat = json['heartbeat'];
    bp = json['bp'];
    dustAllergy = json['dust_allergy'];
    medication = json['medication'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['patient_name'] = this.patientName;
    data['age'] = this.age;
    data['gender'] = this.gender;
    data['dob'] = this.dob;
    data['phone'] = this.phone;
    data['regi_date'] = this.regiDate;
    data['heartbeat'] = this.heartbeat;
    data['bp'] = this.bp;
    data['dust_allergy'] = this.dustAllergy;
    data['medication'] = this.medication;
    return data;
  }
}