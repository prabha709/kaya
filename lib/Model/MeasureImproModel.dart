class MeasureImproveModel {
  List<Resp> resp;

  MeasureImproveModel({this.resp});

  MeasureImproveModel.fromJson(Map<String, dynamic> json) {
    if (json['resp'] != null) {
      resp = new List<Resp>();
      json['resp'].forEach((v) {
        resp.add(new Resp.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.resp != null) {
      data['resp'] = this.resp.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Resp {
  int id;
  bool remark;
  String image;
  String processImage;
  double skinScore;
  String createdDate;
  String updatedDate;

  Resp(
      {this.id,
      this.remark,
      this.image,
      this.processImage,
      this.skinScore,
      this.createdDate,
      this.updatedDate});

  Resp.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    remark = json['remark'];
    image = json['image'];
    processImage = json['process_image'];
    skinScore = json['skin_score'];
    createdDate = json['created_date'];
    updatedDate = json['updated_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['remark'] = this.remark;
    data['image'] = this.image;
    data['process_image'] = this.processImage;
    data['skin_score'] = this.skinScore;
    data['created_date'] = this.createdDate;
    data['updated_date'] = this.updatedDate;
    return data;
  }
}