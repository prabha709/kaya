class DiagnosisImageModel {
  List<Resp> resp;

  DiagnosisImageModel({this.resp});

  DiagnosisImageModel.fromJson(Map<String, dynamic> json) {
    if (json['resp'] != null) {
      resp = new List<Resp>();
      json['resp'].forEach((v) {
        resp.add(new Resp.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.resp != null) {
      data['resp'] = this.resp.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Resp {
  int id;
  String dImage;
  String processImage;
  String dPlot;
  String createdDate;
  String updatedDate;

  Resp(
      {this.id,
      this.dImage,
      this.processImage,
      this.dPlot,
      this.createdDate,
      this.updatedDate});

  Resp.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    dImage = json['d_image'];
    processImage = json['process_image'];
    dPlot = json['d_plot'];
    createdDate = json['created_date'];
    updatedDate = json['updated_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['d_image'] = this.dImage;
    data['process_image'] = this.processImage;
    data['d_plot'] = this.dPlot;
    data['created_date'] = this.createdDate;
    data['updated_date'] = this.updatedDate;
    return data;
  }
}
