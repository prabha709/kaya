class DoctorServiceModel {
  List<DoctorServices> doctorServices;

  DoctorServiceModel({this.doctorServices});

  DoctorServiceModel.fromJson(Map<String, dynamic> json) {
    if (json['Doctor_services'] != null) {
      doctorServices = new List<DoctorServices>();
      json['Doctor_services'].forEach((v) {
        doctorServices.add(new DoctorServices.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.doctorServices != null) {
      data['Doctor_services'] =
          this.doctorServices.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DoctorServices {
  String rowLabels;

  DoctorServices({this.rowLabels});

  DoctorServices.fromJson(Map<String, dynamic> json) {
    rowLabels = json['Row Labels'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Row Labels'] = this.rowLabels;
    return data;
  }
}
